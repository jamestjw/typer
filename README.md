
Typer
=====

# How to build Typer

Status [![Build Status](https://travis-ci.org/Delaunay/typer.svg?branch=master)](https://travis-ci.org/Delaunay/typer)

## Requirement

* Ocaml 4.11
* Dune 2.x
* Zarith

## Build

By default, Dune creates a '_build' folder which holds all the compiled files.

* `make typer`: build Typer interpreter (`./typer_cli.bc`).
* `COMPILE_MODE=exe make typer`: build Typer interpreter (`./typer_cli.exe`).
* `make debug`: build typer with debug info (`./debug_util.bc`).
* `make tests`: run interpreter's tests.

# Directory layout

* `src/` interpreter's source
* `doc/` interpreter's doc files
* `sample/` typer code sample for testing purposes
* `tests/`  ocaml test files
* `btl/`    builtin typer library

# Emacs files

    /typer-mode.el
