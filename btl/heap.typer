%%% heap --- Manipulate partially initialized objects on the heap

%%      Copyright (C) 2011-2020  Free Software Foundation, Inc.
%%
%% Author: Simon Génier <simon.genier@umontreal.ca>
%% Keywords: languages, lisp, dependent types.
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

datacons-label = macro (lambda args ->
  let
    label = Sexp_dispatch (List_nth 0 args Sexp_error)
      (lambda _ _ -> Sexp_error)
      (lambda label-name -> Sexp_string label-name)
      (lambda _ -> Sexp_error)
      (lambda _ -> Sexp_error)
      (lambda _ -> Sexp_error)
      (lambda _ -> Sexp_error)
  in
    IO_return (quote (datacons-label<-string (uquote label)))
)
