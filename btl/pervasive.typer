%%% pervasive --- Always available definitions

%%      Copyright (C) 2011-2023  Free Software Foundation, Inc.
%%
%% Author: Pierre Delaunay <pierre.delaunay@hec.ca>
%% Keywords: languages, lisp, dependent types.
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Commentary:

%% This file includes all kinds of predefined types and functions that are
%% generally useful.  It plays a similar role to `builtins.typer` and is
%% read right after that one.  The main reason for the separation into
%% 2 different files, is that for technical reasons, one cannot use macros
%% in `builtins.typer`.

%%% Code:

%%%% `Option` type

Option = typecons (Option (ℓ ::: TypeLevel) (a : Type_ ℓ)) (none) (some a);
some = datacons Option some;
none = datacons Option none;

optionally : (?a -> ?b) -> ?b -> Option ?a -> ?b;
optionally func =
  lambda def ->
  lambda opt ->
  case opt
  | some a => func a
  | none => def;

%%%% List functions

List_length : List ?a -> Int;   % Recursive defs aren't generalized :-(
List_length xs = case xs
  | nil => (Integer->Int 0)
  | cons hd tl =>
      (Int_+ (Integer->Int 1) (List_length tl));

%% ML's typical `head` function is not total, so can't be defined
%% as-is in Typer.  There are several workarounds:
%% - Provide a default value : `a -> List a -> a`;
%% - Disallow problem case   : `(l : List a) -> (l != nil) -> a`;
%% - Return an Option/Error
List_head1 xs = case xs
  | nil => none
  | cons hd tl => some hd;

List_head x = lambda xs -> case xs
  | cons x _ => x
  | nil => x;

List_tail xs = case xs
  | nil => nil
  | cons hd tl => tl;

List_map : (?a -> ?b) -> List ?a -> List ?b;
List_map f = lambda xs -> case xs
  | nil => nil
  | cons x xs => cons (f x) (List_map f xs);

List_foldr : (?b -> ?a -> ?a) -> List ?b -> ?a -> ?a;
List_foldr f = lambda xs -> lambda i -> case xs
  | nil => i
  | cons x xs => f x (List_foldr f xs i);

List_find : (?a -> Bool) -> List ?a -> Option ?a;
List_find f = lambda xs -> case xs
  | nil => none
  | cons x xs => case f x | true => some x | false => List_find f xs;

List_nth : Int -> List ?a -> ?a -> ?a;
List_nth = lambda n -> lambda xs -> lambda d -> case xs
  | nil => d
  | cons x xs
    => case Int_<= n (Integer->Int 0)
       | true => x
       | false => List_nth (Int_- n (Integer->Int 1)) xs d;

%%%% A more flexible `lambda`

%% An `Sexp` which we use to represents an *error*.
Sexp_error = Sexp_symbol "<error>";

%% Sexp_to_list : Sexp -> List Sexp -> List Sexp;
Sexp_to_list s = lambda exceptions ->
  let singleton (_ : ?) = cons s nil in %FIXME: Get rid of ": ?"!
  Sexp_dispatch
      s
      (node := lambda head -> lambda tail
        -> case List_find (Sexp_eq head) exceptions
        | some _ => singleton ()
        | none => cons head tail)
      (symbol := lambda s
        -> case String_eq "" s
        | true => nil
        | false => singleton ())
      singleton singleton singleton singleton;

multiarg_lambda =
  %% This macro lets `lambda_->_` (and siblings) accept multiple arguments.
  %% TODO: We could additionally turn
  %%     lambda (x :: t) y -> e
  %% into
  %%     lambda (x : t) => lambda y -> e`
  %% thus providing an alternate syntax for lambdas which don't use
  %% => and ≡>.
  let exceptions = List_map Sexp_symbol
                            (cons "_:_" (cons "_::_" (cons "_:::_" nil))) in
  lambda name ->
  let sym = (Sexp_symbol name);
      mklambda = lambda arg -> lambda body ->
        Sexp_node sym (cons arg (cons body nil)) in
  lambda (margs : List Sexp)
  -> IO_return
         case margs
         | nil => Sexp_error
         | cons fargs margstail
           => case margstail
              | nil => Sexp_error
              | cons body bodytail
                => case bodytail
                   | cons _ _ => Sexp_error
                   | nil => List_foldr mklambda
                                       (Sexp_to_list fargs exceptions)
                                       body;

lambda_->_ = macro (multiarg_lambda "##lambda_->_");
lambda_=>_ = macro (multiarg_lambda "##lambda_=>_");
lambda_≡>_ = macro (multiarg_lambda "##lambda_≡>_");

%%%% More list functions

List_reverse : List ?a -> List ?a -> List ?a;
List_reverse l t = case l
  | nil => t
  | cons hd tl => List_reverse tl (cons hd t);

List_concat l t = List_reverse (List_reverse l nil) t;

List_foldl : (?a -> ?b -> ?a) -> ?a -> List ?b -> ?a;
List_foldl f i xs = case xs
  | nil => i
  | cons x xs => List_foldl f (f i x) xs;

List_mapi : (a : Type) ≡> (b : Type) ≡> (a -> Int -> b) -> List a -> List b;
List_mapi f xs = let
  helper : (a -> Int -> b) -> Int -> List a -> List b;
  helper f i xs = case xs
    | nil => nil
    | cons x xs => cons (f x i) (helper f (Int_+ i (Integer->Int 1)) xs);
in helper f (Integer->Int 0) xs;

List_map2 : (?a -> ?b -> ?c) -> List ?a -> List ?b -> List ?c;
List_map2 f xs ys = case xs
  | nil => nil
  | cons x xs => case ys
    | nil => nil % error
    | cons y ys => cons (f x y) (List_map2 f xs ys);

%% Fold 2 List as long as both List are non-empty
List_fold2 : (?a -> ?b -> ?c -> ?a) -> ?a -> List ?b -> List ?c -> ?a;
List_fold2 f o xs ys = case xs
  | cons x xs => ( case ys
    | cons y ys => List_fold2 f (f o x y) xs ys
    | nil => o ) % may be an error
  | nil => o; % may or may not be an error

%% Is argument List empty?
List_empty : List ?a -> Bool;
List_empty xs = Int_eq (List_length xs) (Integer->Int 0);

%%% Good 'ol combinators

id x = x;

%% Use explicit erasable annotations since with an annotation like
%%     K : ?a -> ?b -> ?a;
%% Typer generalizes to
%%     K : (a : Type) ≡> (b : Type) ≡> a -> b -> a;
%% Which is less general.
%% FIXME: Change the generalizer to insert the `(b : Type) ≡>`
%% more lazily (i.e. after the `a` argument).
K : ?a -> (b : Type) ≡> b -> ?a;
K x y = x;

%%%% Quasi-Quote macro

%%   f = (quote (uquote x) * x) (node _*_ [(Sexp_node unquote "x") "x"])
%%
%%   f = Sexp_node "*" cons (x, cons (Sexp_symbol "x") nil))
%%
%%     =>       x

%% Takes an Sexp `x` as argument and return an Sexp which represents code
%% which will construct an Sexp equivalent to `x` at run-time.
%% This is basically *cross stage persistence* for Sexp.
quote1 : Sexp -> Sexp;
quote1 x = let
               qlist : List Sexp -> Sexp;
               qlist xs = case xs
                 | nil => Sexp_symbol "nil"
                 | cons x xs => Sexp_node (Sexp_symbol "cons")
                                          (cons (quote1 x)
                                                (cons (qlist xs) nil));
               make_app str sexp =
                 Sexp_node (Sexp_symbol str) (cons sexp nil);

               node op y = case (Sexp_eq op (Sexp_symbol "uquote"))
                 | true  => List_head Sexp_error y
                 | false => Sexp_node (Sexp_symbol "##Sexp.node")
                                      (cons (quote1 op)
                                            (cons (qlist y) nil));
               symbol s = make_app "##Sexp.symbol" (Sexp_string s);
               string s = make_app "##Sexp.string" (Sexp_string s);
               integer i = make_app "##Sexp.integer"
                                    (make_app "##Int->Integer" (Sexp_integer i));
               float f = make_app "##Sexp.float" (Sexp_float f);
               block sxp =
                  %% FIXME ##Sexp.block takes a string (and prelexes
                  %% it) but we have a sexp, what should we do?
                  Sexp_symbol "<error FIXME block quoting>";
           in Sexp_dispatch x node symbol string integer float block;

%% quote definition
quote = macro (lambda x -> IO_return (quote1 (List_head Sexp_error x)));

%%%% The `type` declaration macro

%% Build a declaration:
%%     var-name = value-expr;
make-decl : Sexp -> Sexp -> Sexp;
make-decl var-name value-expr =
  Sexp_node (Sexp_symbol "_=_")
            (cons var-name
                  (cons value-expr nil));

chain-decl : Sexp -> Sexp -> Sexp;
chain-decl a b =
  Sexp_node (Sexp_symbol "_;_") (cons a (cons b nil));

chain-decl3 : Sexp -> Sexp -> Sexp -> Sexp;
chain-decl3 a b c =
  Sexp_node (Sexp_symbol "_;_") (cons a (cons b (cons c nil)));

%% Build datacons:
%%     ctor-name = datacons type-name ctor-name;
make-cons : Sexp -> Sexp -> Sexp;
make-cons ctor-name type-name =
  make-decl ctor-name
            (Sexp_node (Sexp_symbol "datacons")
                       (cons type-name
                             (cons ctor-name nil)));

%% Build type annotation:
%%     var-name : type-expr;
make-ann : Sexp -> Sexp -> Sexp;
make-ann var-name type-expr =
  Sexp_node (Sexp_symbol "_:_")
            (cons var-name
                  (cons type-expr nil));

type-impl = lambda (x : List Sexp) ->
  %% `x` follow the mask ->
  %%                  (_|_ Nat zero (succ Nat))
  %%       Type name  --^    ^------^ constructors

  %% Return a list contained inside a node sexp.
  let knil = K nil;
      kerr = K Sexp_error;

      get-list : Sexp -> List Sexp;
      get-list node = Sexp_dispatch node
                                     (lambda op lst -> lst) % Nodes
                                     knil      % Symbol
                                     knil      % String
                                     knil      % Integer
                                     knil      % Float
                                     knil;     % List of Sexp

      %% Get a name (symbol) from a sexp
      %%   - (name t) -> name
      %%   - name -> name
      get-name : Sexp -> Sexp;
      get-name sxp =
        Sexp_dispatch sxp
                       (lambda op _ -> get-name op) % Nodes
                       (lambda _    -> sxp) % Symbol
                       kerr  % String
                       kerr  % Integer
                       kerr  % Float
                       kerr; % List of Sexp

      get-args : Sexp -> List Sexp;
      get-args sxp =
        Sexp_dispatch sxp
                       (lambda _ args -> args)            % Nodes
                       (lambda _    -> (nil : List Sexp)) % Symbol
                       (lambda _ -> (nil : List Sexp))    % String
                       (lambda _ -> (nil : List Sexp))    % Integer
                       (lambda _ -> (nil : List Sexp))    % Float
                       (lambda _ -> (nil : List Sexp));   % List of Sexp

      %% Takes an arrow type and a parameter (possibly named using
      %% `:`/`::`/`:::`), and prepends the corresponding arrow
      %% (`->`/`=>`/`≡>`) to the arrow type given as argument.
      add-arg-arrow : Sexp -> Sexp -> Sexp;
      add-arg-arrow arrow-type arg =
        let default_arr =
              Sexp_node (Sexp_symbol "_->_") (cons arg (cons arrow-type nil)) in
        Sexp_dispatch arg
            (lambda s ss ->
             let single-colon-arg =
                   (Sexp_node (Sexp_symbol "_:_")
                              (cons (List_head Sexp_error ss)
                                    (cons (List_nth (Integer->Int 1) ss Sexp_error) nil)));
                 arrow-args = (cons single-colon-arg (cons arrow-type nil)) in
             case (Sexp_eq s (Sexp_symbol "_:_"))
             | true  => Sexp_node (Sexp_symbol "_->_") arrow-args
             | false =>
                 (case (Sexp_eq s (Sexp_symbol "_::_"))
                  | true  => Sexp_node (Sexp_symbol "_=>_") arrow-args
                  | false =>
                      (case (Sexp_eq s (Sexp_symbol "_:::_"))
                       | true  => Sexp_node (Sexp_symbol "_≡>_") arrow-args
                       | false => default_arr)))
            (lambda _ -> default_arr)
            (lambda _ -> Sexp_error)
            (lambda _ -> Sexp_error)
            (lambda _ -> Sexp_error)
            (lambda _ -> Sexp_error);

      get-head = List_head Sexp_error;

      %% Get expression.
      expr = get-head x;

      %% Expression is  Sexp_node (Sexp_symbol "|") (list)
      %% we only care about the list bit
      lst = get-list expr;
      ctor = List_tail lst;

      %% head+ is (Sexp_node type-name (arg list)), optionally wrapped
      %% with a type annotation (<head> : Type_ l)
      head+ = get-head lst;

      %% head-pair is a 2-list, which adds a default type annotation to head+
      head-pair = case Sexp_eq (get-name head+) (Sexp_symbol "_:_")
        | true => get-list head+
        | false => cons head+ (cons (Sexp_symbol "Type") nil);

      head      = List_nth (Integer->Int 0) head-pair Sexp_error;
      type-type = List_nth (Integer->Int 1) head-pair Sexp_error;
      type-name = get-name head;
      type-args = get-list head;

      %% Create the inductive type definition.
      inductive = Sexp_node (Sexp_symbol "typecons")
                            (cons head ctor);

      %% Declare the inductive type as a function
      %% Useful for recursive type
      type-decl = quote ((uquote type-name) : (uquote (
        (List_foldl add-arg-arrow type-type (List_reverse type-args nil))
      )));

      decl  = make-decl type-name inductive;

      %% Add constructors.
      ctors = let for-each : List Sexp -> Sexp -> Sexp;
                  for-each ctr acc = case ctr
                    | cons hd tl =>
                        let acc2 = chain-decl acc (make-cons (get-name hd)
                                                             type-name)
                        in for-each tl acc2
                    | nil => acc
              in for-each ctor (Sexp_node (Sexp_symbol "_;_") nil)

                            %% return decls
  in IO_return (chain-decl3 type-decl % type as a function declaration
                            decl      % inductive type declaration
                            ctors);   % constructor declarations

type_ = macro type-impl;

%%%% An inductive type to wrap Sexp

type Sexp_wrapper
  | node Sexp (List Sexp)
  | symbol String
  | string String
  | integer Integer
  | float Float
  | block Sexp;

Sexp_wrap s = Sexp_dispatch s node symbol string integer float block;

%%%% Tuples

%% Sample tuple: a module holding Bool and its constructors.
BoolMod = (##datacons
             %% We need the `?` metavars to be lexically outside of the
             %% `typecons` expression, otherwise they end up generalized, so
             %% we end up with a type constructor like
             %%
             %%     typecons _ (cons (τ₁ : Type) (t : τ₁)
             %%                      (τ₂ : Type) (true : τ₂)
             %%                      (τ₃ : Type) (false : τ₃))
             %%
             %% And it's actually even worse because it tries to generalize
             %% over the level of those `Type`s, so we end up with an invalid
             %% inductive type.
             ((lambda t1 t2 t3
               -> typecons _ (cons (t :: t1) (true :: t2) (false :: t3)))
                  ? ? ?)
             cons)
              (_ := Bool) (_ := true) (_ := false);

Pair = typecons (Pair (a : Type) (b : Type)) (pair (fst : a) (snd : b));
pair = datacons Pair pair;

%% Triplet (tuple with 3 values)
type Triplet (a : Type) (b : Type) (c : Type)
  | triplet (x : a) (y : b) (z : c);

%%%% List with tuple

%% Merge two List to a List of Pair
%% Both List must be of same length
List_merge : List ?a -> List ?b -> List (Pair ?a ?b);
List_merge xs ys = case xs
  | cons x xs => ( case ys
    | cons y ys => cons (pair x y) (List_merge xs ys)
    | nil => nil ) % error
  | nil => nil;

%% `Unmerge` a List of Pair
%% The two functions name said it all
List_map-fst xs = let
  mf p = case p | pair x _ => x;
in List_map mf xs;

List_map-snd xs = let
  mf p = case p | pair _ y => y;
in List_map mf xs;

%%%% Sigma
Sigma = typecons
          (Sigma (l1 ::: TypeLevel)
                 (l2 ::: TypeLevel)
                 (A : Type_ l1) (B : A -> Type_ l2))
          (sigma (fst : A) (snd : B fst));
sigma = datacons Sigma sigma;

Sigma_η : (A : Type_ ?) ≡> (P : (a : A) -> Type_ ?)
          ≡> (p : Sigma A P) -> Eq p (sigma (B := P) p.fst p.snd);
Sigma_η p = case p
    | sigma x y =>
        let
          sigmaxy = sigma (B := P) x y;
          branch≡p : Eq sigmaxy p;
          branch≡p = ##DeBruijn 0;
          sigmaxy_η : Eq sigmaxy (sigma (B := P) sigmaxy.fst sigmaxy.snd);
          sigmaxy_η = Eq_refl;
        in
          Eq_cast (x := sigmaxy) (y := p)
                  (p := branch≡p)
                  (f := lambda x -> Eq x (sigma (B := P) x.fst x.snd))
                  sigmaxy_η;

%%%% Logic

%% `False` should be one of the many empty types.
%% Other popular choices are False = ∀a.a and True = ∃a.a.
False = Void;
True = Unit;

Not prop = prop -> False;

%% The Unit argument is a little hack to prevent premature evaluation
%% of the body.
exfalso : (C : Type_ ?) ≡> (v : Void) ≡> Unit -> C;
exfalso = lambda l ≡> lambda C ≡> lambda v ≡> lambda u ->
  ##case_ (Eq_cast (p := (##case_ v) : Eq Unit Void) (f := id) u);

%% Testing generalization in inductive type constructors.
LList : Type -> Int -> Type; %FIXME: `LList : ?` should be sufficient!
type LList (a : Type) (n : Int)
  | vnil (p ::: Eq n (Integer->Int 0))
  | vcons a (LList a ?n1) (p ::: Eq n (Int_+ ?n1 (Integer->Int 1)));

%%%% If

define-operator "if" () 2;
define-operator "then" 2 1;
define-operator "else" 1 66;

if_then_else_
  = macro (lambda args ->
           let e1 = List_nth (Integer->Int 0) args Sexp_error;
               e2 = List_nth (Integer->Int 1) args Sexp_error;
               e3 = List_nth (Integer->Int 2) args Sexp_error;
           in IO_return (quote (case uquote e1
                                | true => uquote e2
                                | false => uquote e3)));

%%%% More boolean

%% FIXME: Type annotations should not be needed below.
not : Bool -> Bool;
%% FIXME: We use braces here to delay parsing, otherwise the if/then/else
%% part of the grammar declared above is not yet taken into account.
not x = { if x then false else true };

or : Bool -> Bool -> Bool;
or x y = { if x then x else y };

and : Bool -> Bool -> Bool;
and x y = { if x then y else x };

xor : Bool -> Bool -> Bool;
xor x y = { if x then (not y) else y };

%% FIXME: Can't use ?a because that is generalized to be universe-polymorphic,
%% which we don't support in `load` yet.
fold : Bool -> (a : Type) ≡> a -> a -> a;
fold x t e = { if x then t else e};

%%%% Test
test1 : ?;
test2 : Option Int;
test3 : Option Int;
test1 = test2;
%% In earlier versions of Typer, we could use `?` in declarations
%% to mean "fill this for me by unifying with later type information",
%% but this is incompatible with the use of generalization to introduce
%% implicit arguments.
%%     test4 : Option ?;
test2 = none;
test4 : Option Int;
test4 = test1 : Option ?;
test3 = test4;

%%%% Rewrite of some builtins to use `Option`

%%
%% If `Elab_nth-arg'` returns "_" it means:
%%   A- The constructor isn't defined, or
%%   B- The constructor has no n'th argument
%%
%% So in those case this function returns `none`
%%
Elab_nth-arg a b c = let
  r = Elab_nth-arg' a b c;
in case (String_eq r "_")
     | true  => (none)
     | false => (some r);

%%
%% If `Elab_arg-pos'` returns (-1) it means:
%%   A- The constructor isn't defined, or
%%   B- The constructor has no argument named like this
%%
%% So in those case this function returns `none`
%%
Elab_arg-pos a b c = let
  r = Elab_arg-pos' a b c;
in case (Int_eq r (Integer->Int -1))
     | true  => (none)
     | false => (some r);

%%%%
%%%% Common library
%%%%

%%
%% Polymorphic literals
%%

%% FIXME: It would be useful to have a way to "open" modules.
poly-lits = load "btl/poly-lits.typer";
typer-immediate = poly-lits.typer-immediate;
FromInteger = poly-lits.FromInteger;
mkFromInteger = poly-lits.mkFromInteger;
fromInteger = poly-lits.fromInteger;
IntFromInteger = poly-lits.IntFromInteger;
IntegerFromInteger = poly-lits.IntegerFromInteger;
instance IntFromInteger IntegerFromInteger;
typeclass FromInteger;

%%
%% Monads
%%

monads = load "btl/monads.typer";
Monad = monads.Monad;
bind = monads.bind;
pure = monads.pure;
IO_monad = monads.IO_monad;
Option_monad = monads.Option_monad;
instance IO_monad Option_monad;
typeclass Monad;

%%
%% `<-` operator used in macro `do` and for tuple assignment
%%

define-operator "<-" 80 96;

%%
%% `List` is the type and `list` is the module
%%
list = load "btl/list.typer";

%%
%% Macro `do` for easier series of IO operation
%%
%% e.g.:
%%     do { IO_return true; };
%%
do-lib = load "btl/do.typer";
do-impl = do-lib.do-impl;
do = do-lib.do;
do* = do-lib.do*;

%%
%% Module containing various macros for tuple
%% Used by `case_`
%%
tuple-lib = load "btl/tuple.typer";

%%
%% Get the nth element of a tuple
%%
%% e.g.:
%%     tuple-nth tup 0;
%%
tuple-nth = tuple-lib.tuple-nth;

%%
%% Affectation of tuple
%%
%% e.g.:
%%     (x,y,z) <- tup;
%%
_<-_ = tuple-lib.assign-tuple;

%%
%% Instantiate a tuple from expressions
%%
%% e.g.:
%%     tup = (x,y,z);
%%
_\,_ = tuple-lib.make-tuple;

Tuple = tuple-lib.tuple-type;

%%
%% Macro `case` for a some more complex pattern matching
%%
case_ = let lib = load "btl/case.typer" in lib.case-macro;

%%
%% plain-let
%% Not recursive and not sequential
%%
%% e.g.:
%%     plain-let x = 1; in x;
%%

define-operator "plain-let" () 3;

%%
%% Already defined:
%%     define-operator "in" 3 67;
%%

plain-let_in_ = let lib = load "btl/plain-let.typer" in lib.plain-let-macro;

%%
%% `case` at function level
%%
%% e.g.:
%%     my-fun (x : Bool)
%%       | true  => false
%%       | false => true;
%%
_|_ = let lib = load "btl/polyfun.typer" in lib._|_;

%%
%% case_as_return_, case_return_
%% A `case` for dependent elimination
%%

define-operator "as" 42 42;
define-operator "return" 42 42;
depelim = load "btl/depelim.typer";
case_as_return_ = depelim.case_as_return_;
case_return_ = depelim.case_return_;

define-operator "qcase" () 42;
qcase_ = let lib = load "btl/qcase.typer" in lib.qcase_macro;

%%%% Equational reasoning
%% Function to do one step of equational reasoning
step-≡ : (x : ?A) -> (y : ?A) ≡> (z : ?A) ≡> Eq x y -> Eq y z -> Eq x z;
step-≡ _ p q = Eq_trans p q;

%% Function to conclude equational reasoning
qed : (x : ?A) -> Eq x x;
qed x = Eq_refl;

%% FIXME: Chosen somewhat arbitrarily for now
define-operator "==<" 45 40;
define-operator ">==" 40 42;
define-operator "∎" 43 ();

_==<_>==_ = step-≡;
_∎ = qed;

%%%% Unit tests function for doing file

%% It's hard to do a primitive which execute test file
%% because of circular dependency...

%%
%% A macro using `load` for unit testing purpose
%%
%% Takes a file name (String) as argument
%% Returns variable named `exec-test` from the loaded file
%%
%% `exec-test` should be a command doing unit tests
%% for other purpose than that just use `load` directly!
%%
Test_file = macro (lambda args -> let

  %% return an empty command on error
  ret = Sexp_node
    (Sexp_symbol "IO_return")
    (cons (Sexp_symbol "unit") nil);

%% Expecting a String and nothing else
in IO_return (Sexp_dispatch (List_nth 0 args Sexp_error)
    (lambda _ _ -> ret)
    (lambda _ -> ret)
    %% `load` is a special form and takes a Sexp rather than a real `String`
    (lambda s -> quote (let lib = load (uquote (Sexp_string s)); in lib.exec-test))
    (lambda _ -> ret)
    (lambda _ -> ret)
    (lambda _ -> ret))
);

%%%% Integer theorems

%%
%% Zero-product property of integers
%% (¬ x ≡ 0) → (¬ y ≡ 0) -> (¬ x · y ≡ 0)
%%
Integer_0-product : (x : Integer) -> (y : Integer) -> (Eq x 0 -> Void) ->
                   (Eq y 0 -> Void) -> (Eq (Integer_* x y) 0 -> Void);
Integer_0-product = lambda x -> lambda y ->
                    lambda x≠0 -> lambda y≠0 -> lambda xy≠0 ->
                     y≠0 (Integer_isIntegral x y xy≠0 x≠0);

%%
%% x · (y + z) ≡ x · y + x · z
%%
%% FIXME: It'd be much better to have some sort of operator to facilitate the
%% concatenation of equality proofs.
%% Also, the last 2 commutativity proofs could be combined if we had cong₂,
%% whose definition requires dependent paths.
Integer_*DistR+ : (x : Integer) -> (y : Integer) -> (z : Integer) ->
                  Eq (Integer_* x (Integer_+ y z))
                     (Integer_+ (Integer_* x y) (Integer_* x z));
Integer_*DistR+ x y z =
  Integer_* x (Integer_+ y z)
  ==< Integer_*-comm x (Integer_+ y z) >==
  Integer_* (Integer_+ y z) x
  ==< Integer_*DistL+ y z x >==
  Integer_+ (Integer_* y x) (Integer_* z x)
  ==< Eq_cong (lambda e -> Integer_+ e (Integer_* z x))
              (Integer_*-comm y x) >==
  Integer_+ (Integer_* x y) (Integer_* z x)
  ==< Eq_cong (lambda e -> Integer_+ (Integer_* x y) e)
              (Integer_*-comm z x) >==
  Integer_+ (Integer_* x y) (Integer_* x z) ∎;

%% FIXME: The fromInteger typeclass seems to make this difficult
z0 = 0 : Integer;
z1 = 1 : Integer;

Integer_+Rid : (x : Integer) -> Eq (Integer_+ x z0) x;
Integer_+Rid x = Eq_trans (Integer_+-comm x z0) (Integer_+Lid x);

Integer_+Rinv : (x : Integer) -> Eq (Integer_+ x (Integer_- z0 x)) z0;
Integer_+Rinv x = Eq_trans (Integer_+-comm x (Integer_- z0 x)) (Integer_+Linv x);

Integer_*Rid : (x : Integer) -> Eq (Integer_* x z1) x;
Integer_*Rid x = Eq_trans (Integer_*-comm x z1) (Integer_*Lid x);

Integer_*Rzero : (x : Integer) -> Eq (Integer_* x z0) z0;
Integer_*Rzero x = Eq_trans (Integer_*-comm x z0) (Integer_*Lzero x);

Integer_*DistR- : (x : Integer) -> (y : Integer) -> (z : Integer) ->
                  Eq (Integer_* x (Integer_- y z))
                     (Integer_- (Integer_* x y) (Integer_* x z));
Integer_*DistR- x y z =
  Integer_* x (Integer_- y z)
  ==< Integer_*-comm x (Integer_- y z) >==
  Integer_* (Integer_- y z) x
  ==< Integer_*DistL- y z x >==
  Integer_- (Integer_* y x) (Integer_* z x)
  ==< Eq_cong (lambda e -> Integer_- e (Integer_* z x))
              (Integer_*-comm y x) >==
  Integer_- (Integer_* x y) (Integer_* z x)
  ==< Eq_cong (lambda e -> Integer_- (Integer_* x y) e)
              (Integer_*-comm z x) >==
  Integer_- (Integer_* x y) (Integer_* x z) ∎;

Integer_NegateDistL* : (x : Integer) -> (y : Integer)
                        -> Eq (Integer_- z0 (Integer_* x y))
                              (Integer_* (Integer_- z0 x) y);
Integer_NegateDistL* x y =
  Integer_- z0 (Integer_* x y)
  ==< Eq_cong (lambda e -> Integer_- e (Integer_* x y))
              (Eq_comm (Integer_*Lzero y)) >== 
  Integer_- (Integer_* z0 y) (Integer_* x y)
  ==< Eq_comm (Integer_*DistL- z0 x y) >==
  Integer_* (Integer_- z0 x) y ∎;

Integer_NegateDistR* : (x : Integer) -> (y : Integer)
                        -> Eq (Integer_- z0 (Integer_* x y))
                              (Integer_* x (Integer_- z0 y));
Integer_NegateDistR* x y =
  Integer_- z0 (Integer_* x y)
  ==< Eq_cong (lambda e -> Integer_- e (Integer_* x y))
              (Eq_comm (Integer_*Rzero x)) >==
  Integer_- (Integer_* x z0) (Integer_* x y)
  ==< Eq_comm (Integer_*DistR- x z0 y) >==
  Integer_* x (Integer_- z0 y) ∎;

%%% pervasive.typer ends here.
