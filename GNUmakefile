DUNE = dune
OCAMLCP := ocamlcp

BUILDDIR := _build

SRC_FILES := $(wildcard ./src/*.ml)
DEPSORT_FILES := $(shell ocamldep -sort -I src $(SRC_NO_DEBUG))

DUNE_PROFILE = dev
# DUNE_PROFILE = release

# Set COMPILE_MODE to `exe` for native builds.
COMPILE_MODE ?= bc

# DEBUG           ?= 1
# VERBOSE         ?= 1

all: typer debug tests

help: ## Prints help for targets with comments
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST)   \
		| { echo -e 'typer:  ## Build Typer'; sort; } \
		| awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

typer: typer.$(COMPILE_MODE)

debug: debug_util.$(COMPILE_MODE)

# interactive typer
typer.$(COMPILE_MODE):
	$(DUNE) build ./typer.$(COMPILE_MODE) --profile $(DUNE_PROFILE)

debug_util.$(COMPILE_MODE):
	$(DUNE) build ./debug_util.$(COMPILE_MODE)

# By default, Dune only runs tests that failed since the last changes. This can
# be confusing if you don't know Dune, so we pass the --force flag to rerun all
# the tests no matter what.
tests:
	$(DUNE) test --force

# There is nothing here. I use this to test if opam integration works
install: tests

# Make language doc
doc-tex:
	texi2pdf ./doc/manual.texi --pdf --build=clean

# Make implementation doc
doc-ocaml:
	ocamldoc -html -d $(BUILDDIR)/doc $(SRC_FILES)

# everything is expected to be compiled in the "./$(BUILDDIR)/" folder
clean:
	$(DUNE) clean

.PHONY: typer debug tests

run/typecheck:
	@./$(BUILDDIR)/debug_util ./samples/test__.typer -typecheck

run/debug_util:
	@./$(BUILDDIR)/debug_util ./samples/test__.typer \
	                          -fmt-type=on -fmt-index=off -fmt-pretty=on

run/typer:
	@./$(BUILDDIR)/typer

run/tests:
	@./$(BUILDDIR)/tests/utests --verbose 1

run/typer-file:
	@./$(BUILDDIR)/typer ./samples/test__.typer

# Compile into bytecode using ocamlc in profiling mode.
# Generate a ocamlprof.dump file.
profiling-cp:
	# ============================
	#     profiling bytecode
	# ============================
	ocamlfind $(OCAMLCP) -o profiling -linkpkg -package zarith \
	-I src str.cma -P f $(DEPSORT_FILES)

# Compile into native code using ocamlopt in profiling mode.
# Generate a gmon.out file.
profiling-optp:
	# ============================
	#     profiling native code
	# ============================
	ocamlfind $(OCAMLOPTP) -o profiling -linkpkg -package zarith \
	-I src str.cmxa -P f $(DEPSORT_FILES)

# Clean profiling
# FIXME: We prefer generate files in the "./$(BUILDDIR)/" folder but how ?
# No -build-dir option found for ocamlc and ocamlopt.
clean-profiling:
	-rm -rf profiling
	-rm -rf src/*.cm[iox] src/*.o
	-rm -rf ocamlprof.dump
	-rm -rf gmon.out
