%%% HoTT -- Homotopy Type-Theory

%%      Copyright (C) 2020  Free Software Foundation, Inc.
%%
%% Author: Stefan Monnier <monnier@iro.umontreal.ca>
%%
%% This file is part of Typer.
%%
%% Typer is free software; you can redistribute it and/or modify it under the
%% terms of the GNU General Public License as published by the Free Software
%% Foundation, either version 3 of the License, or (at your option) any
%% later version.
%%
%% Typer is distributed in the hope that it will be useful, but WITHOUT ANY
%% WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
%% FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
%% more details.
%%
%% You should have received a copy of the GNU General Public License along
%% with this program.  If not, see <http://www.gnu.org/licenses/>.

%%% Commentary:

%% Various definitions inspired from the HoTT book.

%%% Code:

%%%% Paths

%% Cubical interval type `I`
%% Path type:
%%
%%     I : Type0?
%%     i₀ : I;
%%     i₁ : I;
%%
%% Note: while one cannot eliminate (no `if`) on `I`, one can use
%% `∨`, `∧`, and `¬` on values of this type, and maybe one could do
%% `type I | i₀ | i₁` and allow elimination with the simple restriction
%% that you can only eliminate an `I` to another `I` (so one could define
%% `∨` by hand).  Better yet, maybe instead of a special `I` we could simply
%% use a normal `Bool`, since the `≡>` already prevents elimination?
%%
%%     Eq : (t : Type) ≡> t → t → Type;
%%     path : (p : I ≡> t) → Eq (p(_ := i₀)) (p(_ := i₁));
%%
%% We additionally need some kind of elimination on paths like:
%%
%%     Eq_call : Eq(_ := ?t) ?x ?y ≡> I ≡> ?t;
%%       Eq_call (_ : Eq x y) i₀ ↝ x
%%       Eq_call (_ : Eq x y) i₁ ↝ y
%%
%% Heterogenous equality could look like:
%%
%%     HEq (t₁ : Type) ≡> (t₂ : Type) ≡> t₁ → t₂ → Type;
%%     type HEq x₁ x₂
%%       | Hrefl (p : Eq t₁ t₂) (Eq (coe p x₁) x₂);
%%     type HEq (t : (i : I) ≡> Type)
%%                 (x₁ : t(i := i₀)) (x₂ : t(i := i₁))
%%       | Hrefl (p : Eq t₁ t₂) (Eq (coe p x₁) x₂);

%% Example usage of functions based on the Interval
%% type to define equalities
constantString : I ≡> Type;
constantString = lambda _  ≡> String;

reflString : Eq String String;
reflString = Eq_eq (f := constantString);

hello : String;
hello = Eq_cast (p := reflString) (f := lambda x -> x) "hello";

%% This doesn't work as `helloOrWorld`'s first argument is not erasable,
%% hence we can't prove obviously false equalities.
%% helloOrWorld : I -> String;
%% helloOrWorld s = case s
%%                    | i0 => "hello"
%%                    | i1 => "world";
%%
%% hello=world : Eq "hello" "world";
%% hello=world = Eq_eq helloOrWorld;

constantString' : I ≡> Type;
constantString' = Eq_uneq (p := reflString);

hello2 : Eq_uneq (p := reflString) (i := i0);
hello2 = "hello";

hello3 : constantString' (_ := i0);
hello3 = "hello";

hello4 : constantString' (_ := i1);
hello4 = "hello";

helloRefl : Eq "hello" hello;
helloRefl = Eq_refl;

%% Functional extensionality
Eq_funext : (f : ? -> ?) => (g : ? -> ?) =>
            ((x : ?) -> Eq (f x) (g x)) ->
            Eq f g;
Eq_funext p = Eq_eq (f := lambda i ≡> lambda x -> Eq_uneq (p := p x) (i := i));

Eq_cast_refl : (A : ?) ≡> (x : A) -> Eq (Eq_cast (p := Eq_refl (x := x))
                                                 (f := lambda _ -> A) x)
                                        x;
Eq_cast_refl = lambda _ A ≡> lambda x ->
    Eq_eq (f := lambda i ≡> I_transp (A := lambda _ ≡> A) x (r := i));

Eq_cong2 : (x1 : ?A) => (y1 : ?A) => (x2 : ?B) => (y2 : ?B)
           => (f : ?A -> ?B -> ?) -> (p : Eq x1 y1) -> (q : Eq x2 y2)
           -> Eq (f x1 x2) (f y1 y2);
Eq_cong2 f p q =
  Eq_eq (f := lambda i ≡> f (Eq_uneq (p := p) (i := i))
                            (Eq_uneq (p := q) (i := i)));

%% This is necessary to prove Eq_comm_inv
notnot=id : (i : I) -> Eq i (I_not (I_not i));
notnot=id i = case i return (Eq i (I_not (I_not i)))
                | i0 => Eq_refl
                | i1 => Eq_refl;

%% FIXME: This should be provable, need to reduce `(Eq_comm (Eq_comm p))` to `p`.
%% It should be sufficient to add the following reduction:
%%      Eq_uneq (Eq_eq f) i = f (_ := i)
%% and apply `notnot=id`.
%% Eq_comm_inv : (x : ?t) => (y : ?t) => (p : Eq x y) -> Eq (Eq_comm (Eq_comm p)) p;
%% Eq_comm_inv p = ?;

%% This equality holds definitionally by appealing to the following η-equivalence
%% Heq_eq (f := λi ⇛ Heq_uneq (p := p) (i := i)) ≃ p
Eq_cong_Id : (x : ?t) => (y : ?t) => (p : Eq x y) -> Eq (Eq_cong id p) p;
Eq_cong_Id p = Eq_refl;

%% Eq_cong properties

%% TODO: Do we want to make this work? It would require η for Eq types.
%%
%% (Eq l (Heq l t t a b) (Eq_cong l l t t a b (id l t) p) p)
%% but expression has type:
%%     (Eq l (Heq l t t a b) p p)
%% can't unify:
%%     p
%% with:
%%     (Heq_eq l (lambda (<anon> : I) ≡>
%%            t)
%%  (lambda (i : I) ≡>
%%   ((lambda (x : t) ->
%%     x) (Eq_uneq l t a b p i))))
%%
%% Eq_cong_id : (p : Eq ?a ?b) -> Eq (Eq_cong id p) p;
%% Eq_cong_id p = Eq_refl (x := p);

%%%% Univalence

%% type Equiv_function (f : ?A -> ?B) (g : ?A -> ?B)
%%   | equiv_function ((x : ?) -> Eq (f x) (g x));
Equiv_function f (g : (x : ?A) -> ?B) = ((x : ?A) -> Eq (f x) (g x));
%% Equiv_function_axiom : Equiv_function ?f ?g -> Eq ?f ?g;

%% type HoTT_IsEquiv (f : ?A -> ?B)
%%   | hott_isequiv (Equiv_function (compose f ?g) identity)
%%                  (Equiv_function (compose ?h f) identity);

%% type Equiv_type (A : Type) (B : Type)
%%   | equiv_type (f : A -> B) (HoTT_Isequiv f);
%% univalence_axiom : Equiv_type A B -> Eq A B;

%% FIXME: Univalence is incompatible with Typer's `Eq_cast` because
%% it allows casting between, say, `Nat` and `BinNat` which is not
%% a no-op.  Cubical Agda supports it by making its "Eq elimination" into a
%% non-trivial operation whose `Eq` proof is very much non-erasable.
%%
%% It'd be great to support univalence without losing `Eq_cast`, but
%% it's not at all clear how:
%% - One way is to make it a non-axiom and implement it as a system that
%%   proves it directly on a case-by-case basis, as in the paper
%%   "Equivalence for Free!".
%% - Another is to make `Eq_cast` take a proof of `isSet T`,
%%   but that prevents use of `Eq_cast` between `Nat` and `α`
%%   since `isSet Type` is not true.
%%   It would also prevent use of `Eq_cast` on HIT.
%% - Ideally another would be to require `Eq_cast` to take an additional
%%   proof that the equality proof is equal to `eq_refl` or more generally:
%%
%%       Eq_cast: (eq: Eq ?x ?y) ≡> Eq_erasable eq ≡> ...
%%
%%   but that begs the question: how could we prevent "promoting"
%%   a non-erasable proof to an erasable one?
%% - Another is to distinguish univalent universes, as done in the paper
%%   "Extending Homotopy Type Theory with Strict Equality".
%%   E.g. Our sorts `Type ℓ` would be refined to `Type k ℓ` where `k`
%%   would be a boolean indicating if the universe admits univalence or not,
%%   and then the J (aka "cast") rule would be restricted so that a proof
%%   about equality between univalent types can only be used to coerce
%%   between univalent types.
%%   This is reminiscent of the confinement of Prop in Coq, so we could
%%   similarly restrict the univalent universes so they're always erased
%%   before runtime (so we don't need to have the non-nop form of "cast"
%%   at run-time).
%%   This can be thought of as adding a new equality to some universes,
%%   aka making a "quotient universe", which, like HIT, requires proving
%%   that every time we use types from this universe we obey those equalities.

%%%% Propositions, resizing, etc...

HoTT_isSet A = (x : A) -> (y : A) -> (p : Eq x y) -> (q : Eq x y) -> Eq p q;
%% `isProp` basically implies proof irrelevance.
%% So it also implies erasability.  Note that if we use the double-negation
%% encoding of classical `or` in type-theory, then it preserves `isProp`!
HoTT_isProp P = (x : P) -> (y : P) -> Eq x y;

HoTT_isContr = typecons (HoTT_isContr (l ::: TypeLevel)
                                      (A : Type_ l))
                        (isContr (a : A) ((a' : A) -> Eq a a'));
isContr = datacons HoTT_isContr isContr;

isPropΠ : (A : Type_ ?) ≡> (B : A -> Type_ ?) ≡>
          (h : (x : A) -> HoTT_isProp (B x)) ->
          HoTT_isProp ((x : A) -> B x);
isPropΠ h f g = Eq_eq (f := lambda i ≡>
                            lambda (x : A) ->
                                Eq_uneq (p := h x (f x) (g x))
                                        (i := i));

whiskerRight : (A : I ≡> Type_ ?) ≡> (a₀ : A (_ := i0)) -> (a₁ : A (_ := i1))
               -> (a₁' : A (_ := i1)) -> Heq (t := A) a₀ a₁ -> Eq a₁ a₁'
               -> Heq (t := A) a₀ a₁';
whiskerRight = lambda l A ≡> lambda a₀ a₁ a₁' p₁ p₂ ->
  %% FIXME: Somehow type inference doesn't work well if we make
  %% a₀ a₁ a₁' implicit
  Eq_cast (p := p₂)
          (f := lambda x -> Heq (t := A) a₀ x) p₁;

Σ≡Prop : (A : Type_ ?l1) ≡> (B : (a : A) -> Type_ ?l2)
         ≡> ((x : A) -> HoTT_isProp (B x))
         -> (u : Sigma A B) -> (v : Sigma A B)
         -> (p : Eq u.fst v.fst) -> Eq u v;
Σ≡Prop = lambda l1 l2 A B ≡> lambda propB u v p ->
  let
    h : Heq (t := lambda i ≡> B (Eq_uneq (p := p) (i := i)))
            u.snd (Eq_cast (p := p) (f := B) u.snd);
    h = Heq_eq (t := lambda i ≡> B (Eq_uneq (p := p) (i := i)))
               (f := lambda j ≡>
                      I_transp (A := lambda i ≡>
                                       B (Eq_uneq (p := p) (i := I_meet j i)))
                               u.snd (r := j));
    sndv_eq : Eq (Eq_cast (p := p) (f := B) u.snd) v.snd;
    sndv_eq = propB v.fst (Eq_cast (p := p) (f := B) u.snd) v.snd;
    pathOver : Heq (t := lambda i ≡> B (Eq_uneq (p := p) (i := i)))
                   u.snd v.snd;
    pathOver = whiskerRight (A := lambda i ≡> B (Eq_uneq (p := p) (i := i)))
                            u.snd (Eq_cast (p := p) (f := B) u.snd)
                            v.snd h sndv_eq;
    r : Eq (sigma (B := B) u.fst u.snd) (sigma (B := B) v.fst v.snd);
    r = Eq_eq (f := lambda i ≡>
                    sigma (B := B) (Eq_uneq (p := p) (i := i))
                            (Heq_uneq (t := lambda i ≡>
                                            B (Eq_uneq (p := p) (i := i)))
                            (p := pathOver) (i := i)));
  in
    Eq_trans (Sigma_η u) (Eq_trans r (Eq_comm (Sigma_η v)));

Eq_η : (A : I ≡> Type_ ?) ≡> (i : I)
       -> Eq (A (_ := i)) (Eq_uneq (p := Eq_eq (f := A)) (i := i));
Eq_η = lambda l A ≡> lambda i ->
  case i return Eq (A (_ := i))
                   (Eq_uneq (p := Eq_eq (f := A)) (i := i))
  | i0 => Eq_refl
  | i1 => Eq_refl;

castIsEq : (A : I ≡> Type_ ?) ≡> (x : A (_ := i0))
           -> Heq (t := A) x (Eq_cast (p := Eq_eq (f := A)) (f := id) x);
castIsEq = lambda _  A ≡> lambda x ->
  let
    p : Eq (I_transp (A := A) x (r := i1))
           (Eq_cast (p := Eq_eq (f := A)) (f := id) x);
    p = Eq_eq (f := lambda j ≡>
                    I_transp
                        (A := lambda i ≡> Eq_uneq (p := Eq_η (A := A) i)
                                                  (i := j))
                        x (r := i1));

   h = Heq_eq (t := A)
              (f := lambda i ≡>
                    I_transp (A := A) x (r := i));
                    %% I_transp (A := lambda j ≡> A (_ := I_meet i j))
  in
    Eq_cast (p := p) (f := lambda e -> Heq (t := A) x e) h ;

%% Provable without axioms:
%%
%%     ¬¬¬A -> ¬A

Inverse_double_negation : ?A -> Not (Not ?A);
Inverse_double_negation a na = na a;

Weak_double_negation : Not (Not (Not ?A)) -> Not ?A;
Weak_double_negation nnna a = nnna (lambda na -> na a);

%% BEWARE: univalence_axiom incompatible with general LEM!
%% HoTT_LEM_axiom : HoTT_isProp A -> ¬¬A -> A;

%% "mere propositions" can be treated impredicatively!!
%% HoTT_propositional_resizing_axiom :
%%    Equiv_type { A : Type l | isProp A} { A : Type (l + 1) | isProp A}

%% Propositional truncation: ||A|| is equivalent to A but is a mere proposition.
%% One way to approximate could be:
%% FIXME: The below definition makes the importation of this module fail!
%% Field type (ℓ : ##TypeLevel
%%             => (ℓ : ##TypeLevel
%%                  ≡> (A : (##Type_ ℓ)
%%                         -> (##Type_
%%                             (##TypeLevel.∪ (##TypeLevel.succ ℓ)
%%                              (##TypeLevel.∪ ℓ ℓ)))))) is not a Type! (##Type_ω)
%% Propositional_truncation A = (P : ?) ≡> HoTT_isProp P ≡> (A -> P) -> P;
%% propositional_truncation : ?A -> Propositional_truncation ?A;
%% propositional_truncation a f = f a;



%%% hott.typer ends here.
