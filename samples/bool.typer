%%% bool.typer --- Library of plain old booleans

%% Call it `t` so when we do `Bool = load "bool.typer"`, we
%% can use `Bool.t` to refer to the type.
%% FIXME: In a type context, `Bool` should then automatically be "coerced"
%% to `Bool.t`.
type t
  | false
  | true;

%% FIXME: These grammar settings currently only apply to this file,
%% whereas they should be import(ed|able) via something like `load`.
define-operator "if" () 2;
define-operator "then" 2 1;
define-operator "else" 1 66;

if_then_else_
  = macro (lambda args ->
           let e1 = List_nth 0 args Sexp_error;
               e2 = List_nth 1 args Sexp_error;
               e3 = List_nth 2 args Sexp_error;
           in IO_return (quote (case uquote e1
                     | true => uquote e2
                     | false => uquote e3)));

%% FIXME: Type annotations should not be needed below.
not : t -> t;
%% FIXME: We use braces here to delay parsing, otherwise the if/then/else
%% part of the grammar declared above is not yet taken into account.
not x = { if x then false else true };

or : t -> t -> t;
or x y = { if x then x else y };

and : t -> t -> t;
and x y = { if x then y else x };

xor : t -> t -> t;
xor x y = { if x then (not y) else y };

%% FIXME: Can't use ?a because that is generalized to be universe-polymorphic,
%% which we don't support in `load` yet.
fold : t -> (a : Type) ≡> a -> a -> a;
fold x t e = { if x then t else e};


