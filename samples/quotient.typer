%%%% Definitions of `Nat`
nat-lib = load "samples/nat.typer";
Nat = nat-lib.Nat;
zero = nat-lib.zero;
succ = nat-lib.succ;
Nat_- = nat-lib.minus;
NatToInt = nat-lib.to-num;
HoTT_lib = load "samples/hott.typer";
quot_lib = load "samples/quotient_lib.typer";

NatPair = Pair Nat Nat;

normaliseZ : NatPair -> NatPair;
normaliseZ np = case np
  | pair m n => pair (Nat_- m n) (Nat_- n m);

%% FIXME: Ideally, we shouldn't need to define this separately as this should
%% be taken care of by `NormToQuotient`. However, for now we still to
%% explicitly pass this as a value of R to most `Quotient` functions as it
%% cannot be inferred.
equalZ : NatPair -> NatPair -> Type;
equalZ x1 x2 = Eq (normaliseZ x1) (normaliseZ x2);

Z = NormToQuotient normaliseZ;

%%
%% Quotient.eq
%%
%% Proof that quotiented elements are equal when the base elements themselves
%% are related in the underlying type.

q1-0 : Quotient NatPair equalZ;
q1-0 = Quotient_in (pair (succ zero) zero);

q2-1 : Quotient NatPair equalZ;
q2-1 = Quotient_in (pair (succ (succ zero)) (succ zero));

q1=1 : Eq q1-0 q2-1;
q1=1 = Quotient_eq (R := equalZ)
                   (a := pair (succ zero) zero)
                   (a' := pair (succ (succ zero)) (succ zero))
                   Eq_refl;

%%
%% Quotient.elim
%%
%% Elimination of quotients requires a proof that the equality between
%% quotients is respected.

NatPairToInt' : NatPair -> Int;
NatPairToInt' np = case np
  | pair x y =>
    (case x
      | zero => (NatToInt y) * -1
      | succ _ => NatToInt x);

NatPairToInt : NatPair -> Int;
NatPairToInt np = NatPairToInt' (normaliseZ np);

%% Proof that NatPairToInt respects the quotient Z
NatPairToIntCompat : (a : NatPair) -> (a' : NatPair) ->
                     (p : equalZ a a') ->
                     Eq (NatPairToInt a) (NatPairToInt a');
NatPairToIntCompat _ _ p = Eq_eq (f := lambda i ≡>
                                    NatPairToInt' (Eq_uneq (p := p) (i := i)));

%% FIXME: Explicitly providing a value for R should unnecessary,
%% this should be inferred based on the type of `q`. This is
%% because we do not handle residuals during unification for now.
Z_To_Int : Quotient NatPair equalZ -> Int;
Z_To_Int q = Quotient_rec (R := equalZ) NatPairToInt (p := NatPairToIntCompat) q;

neg2_Z : Quotient NatPair equalZ;
neg2_Z = Quotient_in (pair (succ zero) (succ (succ (succ zero))));

neg2_Int : Int;
neg2_Int = Z_To_Int neg2_Z;

%% FIXME: This could work if we add a reduction rule
%% neg2_refl : Eq neg2_Int (-2 : Int);
%% neg2_refl = Eq_refl;

%% `qcase` macro to facilitate elimination
Z_To_Int' : Quotient NatPair equalZ -> Int;
Z_To_Int' q =
  %% The annotation is optional, but is necessary in
  %% this case, since the propagation of type
  %% information is insufficient the way things are now.
  qcase (q : NatPair / equalZ)
   | Quotient_in a => NatPairToInt a
   | Quotient_eq a a' r i => NatPairToInt' (Eq_uneq (p := r) (i := i));

%% Omitting the `i` parameter by providing an equality proof on the RHS
Z_To_Int'' : Quotient NatPair equalZ -> Int;
Z_To_Int'' q =
  qcase (q : NatPair / equalZ)
   | Quotient_in a => NatPairToInt a
   | Quotient_eq a a' r => NatPairToIntCompat a a' r;

%% TODO: Define Quotient NatPair equalZ ≃ Int

elim_β : (A : Type_ ?) ≡> (B : Type_ ?)
         ≡> (R : A -> A -> Type_ ?)
         ≡> (f : A -> B)
         -> (p : (a : A) -> (a' : A) -> R a a' -> Eq (f a) (f a'))
         -> (a : A) -> Eq (Quotient_rec (R := R) f
                                         (p := p) (Quotient_in (R := R) a))
                          (f a);
elim_β f p a = Eq_refl;

%%
%% Proof that the existence of Quotient types implies functional extensionality
%%

funext : (A : Type_ ?) -> (B : A -> Type_ ?)
         -> (f1 : (a : A) -> B a) -> (f2 : (a : A) -> B a)
         -> (h : (a : A) -> Eq (f1 a) (f2 a)) -> Eq f1 f2;
funext A B f1 f2 h =
  let
    fn_type = (a : A) -> B a;
    %% This relation can be easily shown to be an equivalence relation
    %% by leveraging the properties of the `Eq` type.
    equiv_rel : (f1 : fn_type) -> (f1 : fn_type) -> Type_ ?;
    equiv_rel f1 f2 = ((a : A) -> Eq (f1 a) (f2 a));
    extfun = Quotient fn_type equiv_rel;
    extfun_app : (f : extfun) -> fn_type;
    extfun_app f a = Quotient_rec (R := equiv_rel)
                                  (f := lambda f -> f a)
                                  (p := lambda f1 f2 r -> r a) f;
    sound : Eq (Quotient_in (R := equiv_rel) f1)
               (Quotient_in (R := equiv_rel) f2);
    sound = Quotient_eq (R := equiv_rel) (a := f1) (a' := f2) h;
    %% The two terms reduce to f1 and f2 respectively by
    %%  1. Leveraging the fact that elim_β holds definitionally
    %%  2. The η-equivalence of functions
    res : Eq (extfun_app (Quotient_in (R := equiv_rel) f1))
             (extfun_app (Quotient_in (R := equiv_rel) f2));
    %% FIXME: Loading `samples/hott.typer` causes the following error
    %%  [X] Fatal    :(internal) lub of two SLsucc
    %% Hence, the definition of `Eq_cong` is inlined for now
    res = Eq_eq (f := lambda i ≡> extfun_app (Eq_uneq (p := sound) (i := i)));
  in
    res;

%% Define ℤ using a `Prop`-version of the relation
%% FIXME: Something fishy is going on here, if instead of `a` and `b` we use
%% `x1` and `x2`, we get a Debruijn index error when we prove the effectiveness
%% of this Quotient.
equalZProp : NatPair -> NatPair -> Type;
equalZProp a b = Erased (equalZ a b);

ℤ' = Quotient NatPair equalZProp;

NatPairToIntCompat' : (a : NatPair) -> (a' : NatPair) ->
                     (p : equalZProp a a') ->
                     Eq (NatPairToInt a) (NatPairToInt a');
NatPairToIntCompat' _ _ p =
  case p
  | erased (_ := p) => Eq_eq (f := lambda i ≡>
                                     NatPairToInt' (Eq_uneq (p := p) (i := i)));

Z'_To_Int : ℤ' -> Int;
Z'_To_Int q = Quotient_rec (R := equalZProp)
                           NatPairToInt
                           (p := NatPairToIntCompat') q;

equalZPropisEquiv : quot_lib.isEquivRel NatPair equalZProp;
equalZPropisEquiv =
  let
    isRefl : (x : NatPair) -> equalZProp x x;
    isRefl x = erased (t := Eq_refl);
    isSym : (x : NatPair) -> (y : NatPair) -> equalZProp x y -> equalZProp y x;
    isSym x y p = case p
      | erased (_ := t) => erased (t := Eq_comm t);
    isTrans : (x : NatPair) -> (y : NatPair) -> (z : NatPair) -> equalZProp x y
              -> equalZProp y z -> equalZProp x z;
    isTrans x y z p q = case (p , q)
      | (erased (_ := t1) , erased (_ := t2)) => erased (t := Eq_trans t1 t2);
  in
    quot_lib.equivRel (R := equalZProp) isRefl isSym isTrans;

ℤ'≡→equalZProp : (a : NatPair) -> (b : NatPair) -> Eq (t := ℤ') (Quotient_in a)
                                                                (Quotient_in b)
                 -> equalZProp a b;
ℤ'≡→equalZProp a b p = quot_lib.effective equalZ equalZPropisEquiv a b p;

%% TODO: Define ℤ ≃ Int

%% Proof that `Quotient` has the UMP of a coequaliser
%% WTS:
%%   (A/R → B) ≃ Σ (A → B) (λ g → (x y : A) → R x y → g x ≡ g y)

e : (A : Type) ≡> (B : Type) ≡> (R : A -> A -> Type)
    ≡> (Quotient A R -> B)
    -> Sigma (A -> B)
             (lambda g -> (a : A) -> (a' : A) -> R a a' -> Eq (g a) (g a'));
e = lambda A B R ≡> lambda f ->
      sigma (B := lambda g -> (a : A) -> (a' : A) -> R a a' -> Eq (g a) (g a'))
            (lambda x -> f (Quotient_in (R := R) x))
            (lambda x y r -> Eq_cong f (Quotient_eq (R := R) r));

%% The function we are returning simply strips away the `Quotient_in` and
%% passes the underlying element to the function `f : A → B`, i.e. this
%% function is a trivial right inverse to the pre-composition with `Quotient_in`
e' : (A : Type) ≡> (B : Type) ≡> (R : A -> A -> Type)
     ≡> Sigma (A -> B)
              (lambda g -> (a : A) -> (a' : A) -> R a a' -> Eq (g a) (g a'))
     -> (Quotient A R -> B);
e' = lambda A B R ≡> lambda s ->
  Quotient_rec (R := R) s.fst (p := s.snd);

e'e≡id : (A : Type) ≡> (B : Type) ≡> (R : A -> A -> Type)
         ≡> HoTT_lib.HoTT_isSet B
         -> (f : Quotient A R -> B)
         -> Eq (e' (R := R) (e f)) f;
e'e≡id = lambda A B R ≡> lambda isSetB f ->
  let
    p : (x : Quotient A R) -> (f : Quotient A R -> B)
        -> Eq (e' (R := R) (e f) x) (f x);
    p x f =
      quot_lib.elimProp
          (R := R)
          (P := lambda x -> Eq (e' (R := R) (e f) x) (f x))
          (lambda x -> isSetB (e' (R := R) (e f) x) (f x))
          (lambda x -> Eq_refl (x := f (Quotient_in (R := R) x)))
          x;
  in
    HoTT_lib.Eq_funext (f := e' (R := R) (e f)) (g := f) (lambda x -> p x f);

Sigma_η : (ℓ : TypeLevel) ≡> (A : Type_ ℓ) ≡> (P : (a : A) -> Type_ ℓ)
          ≡> (p : Sigma A P) -> Eq p (sigma (B := P) p.fst p.snd);
Sigma_η p = case p
    | sigma x y =>
        let
          sigmaxy = sigma (B := P) x y;
          branch≡p : Eq sigmaxy p;
          branch≡p = ##DeBruijn 0;
          sigmaxy_η : Eq sigmaxy (sigma (B := P) sigmaxy.fst sigmaxy.snd);
          sigmaxy_η = Eq_refl;
        in
          Eq_cast (x := sigmaxy) (y := p)
                  (p := branch≡p)
                  (f := lambda x -> Eq x (sigma (B := P) x.fst x.snd))
                  sigmaxy_η;

ee'≡id : (A : Type) ≡> (B : Type) ≡> (R : A -> A -> Type)
         ≡> HoTT_lib.HoTT_isSet B
         -> (p : Sigma (A -> B)
                       (lambda g -> (x : A) -> (y : A) -> R x y -> Eq (g x) (g y)))
         -> Eq (e (e' (R := R) p)) p;
ee'≡id = lambda A B R ≡> lambda isSetB p ->
  let
    propSnd g =
      HoTT_lib.isPropΠ
        (B := lambda x -> (y : A) -> R x y -> Eq (g x) (g y))
        (lambda x -> HoTT_lib.isPropΠ
                       (B := lambda y -> R x y -> Eq (g x) (g y))
                       (lambda y -> HoTT_lib.isPropΠ
                                      (B := lambda (r : R x y) -> Eq (g x) (g y))
                                      (lambda r -> isSetB (g x) (g y))));
  in
    HoTT_lib.Σ≡Prop
      (B := lambda g -> (x : A) -> (y : A) -> R x y -> Eq (g x) (g y))
      propSnd
      (e (e' (R := R) p)) p Eq_refl;
