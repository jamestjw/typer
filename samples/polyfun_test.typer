
my-and (x : Bool) (y : Bool)
  | (true,true) => true
  | (_,_) => false;

my-or : Bool -> Bool -> Bool;
my-or x y
  | (true,_) => true
  | (_,true) => true
  | (_,_) => false;

my-not (x : Option Bool)
  | some true => none
  | some false => some true
  | none => some false;

my-merge : List Int -> List Int -> List (Pair Int Int);
my-merge xs ys
  | (nil,nil) => nil
  | (cons x xs, cons y ys) => cons (pair x y) (my-merge xs ys)
  | (_,_) => nil;

test-and = do {
  Test_info "POLYFUN" "my-and, basic test with type on arguments";

  r0 <- Test_true "my-and true true" (my-and true true);
  r1 <- Test_false "my-and false false" (my-and false false);
  r2 <- Test_false "my-and true false" (my-and true false);
  r3 <- Test_false "my-and false true" (my-and false true);

  success <- IO_return (and r0 (and r1 (and r2 r3)));

  if success then
    (Test_info "POLYFUN" "test on my-and succeeded")
  else
    (Test_warning "POLYFUN" "test on my-and failed");

  IO_return success;
};

test-or = do {
  Test_info "POLYFUN" "my-or, basic test with function declaration";

  r0 <- Test_true "my-or true true" (my-or true true);
  r1 <- Test_false "my-or false false" (my-or false false);
  r2 <- Test_true "my-or true false" (my-or true false);
  r3 <- Test_true "my-or false true" (my-or false true);

  success <- IO_return (and r0 (and r1 (and r2 r3)));

  if success then
    (Test_info "POLYFUN" "test on my-or succeeded")
  else
    (Test_warning "POLYFUN" "test on my-or failed");

  IO_return success;
};

test-not = do {
  Test_info "POLYFUN" "my-not, test with one variable, sub pattern and type on argument";

  r0 <- Test_eq "my-not (some true)" (my-not (some true)) none;
  r1 <- Test_eq "my-not (some false)" (my-not (some false)) (some true);
  r2 <- Test_eq "my-not none" (my-not none) (some false);

  success <- IO_return (and r0 (and r1 r2));

  if success then
    (Test_info "POLYFUN" "test on my-not succeeded")
  else
    (Test_info "POLYFUN" "test on my-not failed");

  IO_return success;
};

test-rec = do {
  Test_info "POLYFUN" "my-merge, function declaraction and recursion";

  r0 <- Test_eq "my-merge nil nil" (my-merge nil nil) nil;
  r1 <- Test_eq "my-merge nil (cons 1 nil)" (my-merge nil (cons 1 nil)) nil;
  r2 <- Test_eq "my-merge (cons 777 nil) (cons 999 nil)"
    (my-merge (cons 777 nil) (cons 999 nil)) (cons (pair 777 999) nil);
  r3 <- Test_eq "my-merge (cons 0 (cons 1 nil)) (cons 1 (cons 0 nil))"
    (my-merge (cons 0 (cons 1 nil)) (cons 1 (cons 0 nil)))
    (cons (pair 0 1) (cons (pair 1 0) nil));

  success <- IO_return (and r0 (and r1 (and r2 r3)));

  if success then
    (Test_info "POLYFUN" "test on my-merge succeeded")
  else
    (Test_info "POLYFUN" "test on my-merge failed");

  IO_return success;
};

exec-test = do {
  b1 <- test-and;
  b2 <- test-or;
  b3 <- test-not;
  b4 <- test-rec;

  IO_return (and b1 (and b2 (and b3 b4)));
};
