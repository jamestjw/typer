%% Alternative definition of Integers

Eq_cong2 = let lib = load "samples/hott.typer" in lib.Eq_cong2;

natlib = load "samples/nat.typer";
Nat = natlib.Nat;
zero = natlib.zero;
succ = natlib.succ;
_+_ = natlib.plus;
Nat+_comm = natlib.Nat+_comm;

equalZ : Pair Nat Nat -> Pair Nat Nat -> Type;
equalZ p1 p2
  | (pair x1 y1, pair x2 y2) => Eq (x1 + y2) (x2 + y1);

Z = Quotient (Pair Nat Nat) equalZ;

Zeq_2 : Eq (Quotient_in (R := equalZ) (pair (succ (succ zero)) zero))
           (Quotient_in (R := equalZ) (pair (succ (succ (succ zero)))
                                            (succ zero)));
Zeq_2 = Quotient_eq (R := equalZ)
                    (a := pair (succ (succ zero)) zero)
                    (a' := pair (succ (succ (succ zero))) (succ zero))
                    Eq_refl;

Z_negate : Z -> Z;
Z_negate z =
  let
    flip : Pair Nat Nat -> Pair Nat Nat;
    flip p
      | pair x y => pair y x;

    compat : (a : Pair Nat Nat) -> (b : Pair Nat Nat)
            -> equalZ a b
            -> equalZ (flip a) (flip b);
    compat a b r =
      case a return (equalZ (flip a) (flip b))
        | pair x1 y1 =>
            let
              pairx1y1≡a = ##DeBruijn 0;
            in
              case b return (equalZ (flip (pair x1 y1)) (flip b))
              | pair x2 y2 =>
                (let
                  pairx2y2≡b = ##DeBruijn 0;
                in
                  %% WTS: equalZ (flip (pair x1 y1)) (flip (pair x2 y2))
                  %% i.e. equalZ (pair y1 x1) (pair y2 x2)
                  %% i.e. Eq (y1 + x2) (y2 + x1)
                    (y1 + x2
                  ==< Nat+_comm y1 x2 >==
                    x2 + y1
                  ==< Eq_comm (Eq_cast (f := lambda e -> equalZ (pair x1 y1) e)
                                       (p := Eq_comm pairx2y2≡b)
                                       (Eq_cast (f := lambda e -> equalZ e b)
                                                (p := Eq_comm pairx1y1≡a) r)) >==
                    x1 + y2
                  ==< Nat+_comm x1 y2 >==
                    y2 + x1 ∎));
  in
    qcase (z : (Pair Nat Nat) / equalZ)
    | Quotient_in a => Quotient_in (R := equalZ) (flip a)
    | Quotient_eq a b r => Quotient_eq (R := equalZ)
                                       (a := flip a) (a' := flip b)
                                       (compat a b r);
