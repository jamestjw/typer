%% This test only works with conv_erased as we need Erased (_ := e1) and 
%% Erased (_ := e2) to be judgementally equal.

erasedIsProp : (T : Type_ ?) ≡> (t1 : Erased T) -> (t2 : Erased T) -> Eq t1 t2;
erasedIsProp t1 t2 =
  case t1
    | erased (_ := e1) =>
        let
          p1 : Eq (erased (t := e1)) t1;
          p1 = ##DeBruijn 0;
        in
          case t2
            | erased (_ := e2) =>
                let
                  p2 : Eq (erased (t := e2)) t2;
                  p2 = ##DeBruijn 0;
                in
                  %% We take advantage of conv_erase to make this work,
                  %% i.e. erased (t := e1) ≡ erased (t := e2)
                  Eq_trans (Eq_comm p1) p2;
