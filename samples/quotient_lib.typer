%%%%% Prelude %%%%%%

HoTT_lib = load "samples/hott.typer";

Eq_funext = HoTT_lib.Eq_funext;

HoTT_isProp = HoTT_lib.HoTT_isProp;

HoTT_isSet = HoTT_lib.HoTT_isSet;

HoTT_isContr = HoTT_lib.HoTT_isContr;
isContr = HoTT_lib.isContr;

isPropΠ = HoTT_lib.isPropΠ;

%%%%% Prelude END %%%%%%

%%  This function constructs the lid of this square:
%%
%%   x              y
%%    --------------
%%   |              |
%%   |              |
%%   |              | p
%%   |              |
%%   |              |
%%    --------------
%%  x     l      transp_x
%%
%% We first produce a path that corresponds to the base of this square (named `l`
%% in the code). Then by doing what we would normally do to 'concatenate' `l` and
%% `p`, i.e. the same way we traditionally prove transitivity, we obtain the lid
%% of this square.
toPathOver : (A : I ≡> Type_ ?) ≡> (x : A (_ := i0)) => (y : A (_ := i1))
             %% Proof that transporting `x` from `A i0` to `A i1`
             %% is equal to `y`
             => Eq (Eq_cast (p := Eq_eq (f := A))
                            (f := id) x) y
             -> Heq (t := A) x y;
toPathOver = lambda _  A ≡> lambda x y => lambda p ->
  %% Create `l` using `castIsEq` and concatenate with `p`
  HoTT_lib.whiskerRight (A := A) x (Eq_cast (p := Eq_eq (f := A)) (f := id) x) y
                        (HoTT_lib.castIsEq (A := A) x) p;
  %% %% We want a function that will return `x` when passed `i0`
  %% %% and `y` when passed `i1`.
  %% let
  %%   l : Heq (t := A) x (Eq_cast (p := Eq_eq (f := A)) (f := id) x);
  %%   %% Making use of the funny definition of `transp`!
  %%   l = Heq_eq (t := A)
  %%              (f := lambda i ≡>
  %%                    I_transp (A := (lambda j ≡> A (_ := I_meet i j)))
  %%                             x (r := I_not i));
  %% in
  %%   Eq_cast (x := (Eq_cast (p := Eq_eq (f := A)) (f := id) x))
  %%           (y := y)
  %%           (p := p) (f := lambda y' -> Heq (t := A) x y') l;

elimProp : (A : Type_ ?) ≡> (R : A -> A -> Type_ ?)
           ≡> (P : Quotient A R -> Type_ ?)
           ≡> (prop : (x : Quotient A R) -> HoTT_isProp (P x))
           -> (f : (x : A) -> P (Quotient_in x))
           -> (x : Quotient A R) -> P x;
elimProp = lambda _ _ _ A R P ≡> lambda prop f ->
  Quotient_elim
    (R := R) (P := P) f
    (p := lambda a a' r ->
      %% Want to return f a = f a'
      let
        a=a' : Eq (t := Quotient A R) (Quotient_in a) (Quotient_in a');
        a=a' = Quotient_eq (R := R) (a := a) (a' := a') r;
        fa=fa' : Heq (t := lambda i ≡> P (Eq_uneq (p := a=a') (i := i)))
                     (f a) (f a');
        fa=fa' = toPathOver (A := lambda i ≡> P (Eq_uneq (p := a=a') (i := i)))
                            (prop (Quotient_in a')
                                  (Eq_cast (p := Eq_cong P a=a') (f := id) (f a))
                                  (f a'));
      in
        fa=fa');

elimProp2 : (A : Type_ ?) ≡> (B : Type_ ?) ≡>
            (R : A -> A -> Type_ ?) ≡> (S : B -> B -> Type_ ?) ≡>
            (P : Quotient A R -> Quotient B S -> Type_ ?) ≡>
            (prop : (x : Quotient A R) -> (y : Quotient B S)
                    -> HoTT_isProp (P x y)) ->
            (f : (x : A) -> (y : B) -> P (Quotient_in x) (Quotient_in y)) ->
            (x : Quotient A R) -> (y : Quotient B S) -> P x y;
elimProp2 = lambda _ _ _ _ _ A B R S P ≡> lambda prop f ->
  elimProp (R := R) (P := lambda x -> (y : Quotient B S) -> P x y)
           (lambda x -> isPropΠ (B := P x) (prop x))
           (lambda a -> elimProp (R := S) (P := P (Quotient_in a))
                                 (prop (Quotient_in a)) (f a));

elimProp3 : (A1 : Type_ ?) ≡> (A2 : Type_ ?) ≡> (A3 : Type_ ?)
            ≡> (R1 : A1 -> A1 -> Type_ ?) ≡> (R2 : A2 -> A2 -> Type_ ?)
            ≡> (R3 : A3 -> A3 -> Type_ ?)
            ≡> (P : Quotient A1 R1 -> Quotient A2 R2 -> Quotient A3 R3 -> Type_ ?)
            ≡> (prop : (x : Quotient A1 R1) -> (y : Quotient A2 R2)
                       -> (z : Quotient A3 R3) -> HoTT_isProp (P x y z))
            -> (f : (x : A1) -> (y : A2) -> (z : A3)
                    -> P (Quotient_in x) (Quotient_in y) (Quotient_in z))
            -> (x : Quotient A1 R1) -> (y : Quotient A2 R2)
            -> (z : Quotient A3 R3) -> P x y z;
elimProp3 = lambda _ _ _ _ _ _ _ A1 A2 A3 R1 R2 R3 P ≡> lambda prop f ->
  elimProp2 (R := R1) (S := R2)
            (P := lambda x y -> (z : Quotient A3 R3) -> P x y z)
            (lambda x y -> isPropΠ (B := P x y) (prop x y))
            (lambda a b -> elimProp (R := R3) (P := P (Quotient_in a) (Quotient_in b))
                                    (prop (Quotient_in a) (Quotient_in b))
                                    (f a b));

recProp : (A : Type_ ?) ≡> (B : Type_ ?)
          ≡> (R : A -> A -> Type_ ?)
          ≡> (p : HoTT_isProp B)
          -> (f : A -> B)
          -> (x : Quotient A R) -> B;
recProp = lambda _ _ _ _ _ R ≡>
           lambda p f x ->
             Quotient_rec (R := R) f (p := lambda a a' r -> p (f a) (f a')) x;

%% %% Again, this is not very interesting, unlike its dependent
%% %% counterpart.
%% recContr : (A : Type_ ?) ≡> (B : Type_ ?)
%%            ≡> (R : A -> A -> Type_ ?)
%%            ≡> (p : HoTT_isContr B)
%%            -> Quotient A R -> B;
%% recContr = lambda _ _ _ _ _ R ≡>
%%             lambda p x -> case p
%%                 | isContr a f => Quotient_rec (R := R)
%%                                               (lambda _ -> a)
%%                                               (p := lambda a a' r -> Eq_refl)
%%                                               x;

rec2 : (A : Type_ ?) ≡>
       (B : Type_ ?) ≡>
       (C : Type_ ?) ≡>
       (R : A -> A -> Type_ ?) ≡>
       (S : B -> B -> Type_ ?) ≡>
       (C_isSet : HoTT_isSet C) ≡>
       (f : A -> B -> C) ->
       ((a : A) -> (b : A) -> (c : B) -> R a b -> Eq (f a c) (f b c)) ≡>
       ((a : A) -> (b : B) -> (c : B) -> S b c -> Eq (f a b) (f a c)) ≡>
       Quotient A R -> Quotient B S -> C;
rec2 =
  lambda _ _ _ _ _ A B C R S C_isSet ≡>
    lambda f -> lambda feql feqr ≡>
      Quotient_rec (R := R)
        (lambda a -> lambda b ->
          Quotient_rec (R := S) (f a) (p := feqr a) b)
        (p := lambda a a' r ->
          let
            eqf : (b : B) -> Eq (f a b) (f a' b);
            eqf b = feql a a' b r;
            p : (x : Quotient B S) ->
                HoTT_isProp (Eq (Quotient_rec (R := S) (f a) (p := feqr a) x)
                                (Quotient_rec (R := S) (f a') (p := feqr a') x));
            p x = C_isSet (Quotient_rec (R := S) (f a) (p := feqr a) x)
                          (Quotient_rec (R := S) (f a') (p := feqr a') x);
            compat : (x : Quotient B S) ->
                     (Eq (Quotient_rec (R := S) (f a) (p := feqr a) x)
                         (Quotient_rec (R := S) (f a') (p := feqr a') x));
            compat x = elimProp (R := S)
                                (P := lambda x ->
                                  (Eq (Quotient_rec (R := S) (f a) (p := feqr a) x)
                                      (Quotient_rec (R := S) (f a') (p := feqr a') x)))
                                p eqf x;
          in
            Eq_funext (f := Quotient_rec (R := S) (f a) (p := feqr a))
                      (g := Quotient_rec (R := S) (f a') (p := feqr a'))
                      compat);

%% Model propositional truncation using a quotient type!
%% We are essentially defining a relation where every element
%% of P is related to every other element of P.
%% FIXME: Unification errors occur when we make this universe polymorphic!?
PropTrunc : (P : Type) -> Type;
PropTrunc P = Quotient P (lambda p1 p2 -> Unit);

inPropTrunc : (P : Type) ≡> P -> PropTrunc P;
inPropTrunc p = Quotient_in (R := lambda p1 p2 -> Unit) p;

propTruncRec : (A : Type) ≡> (P : Type_ ?) ≡> HoTT_isProp P -> (A -> P) ->
               PropTrunc A -> P;
propTruncRec prop f a = elimProp (R := lambda p1 p2 -> Unit)
                                 (P := lambda _ -> P)
                                 (lambda _ -> prop)
                                 f a;

propTruncElim : (A : Type)
                ≡> (P : PropTrunc A -> Type_ ?)
                ≡> (prop : (x : PropTrunc A) -> HoTT_isProp (P x))
                -> ((x : A) -> P (inPropTrunc x))
                -> (a : PropTrunc A) -> P a;
propTruncElim prop f a = elimProp (R := lambda p1 p2 -> Unit)
                                  (P := P)
                                  prop
                                  f a;

squash : (P : Type) ≡> (x : PropTrunc P) -> (y : PropTrunc P) -> Eq x y;
squash = lambda P ≡> lambda x y ->
  let
    rel : P -> P -> Type;
    rel _ _ = Unit;
  in
    elimProp2 (R := rel) (S := rel)
              (P := lambda x y -> Eq x y)
              (lambda x y -> Quotient_trunc (R := rel) (x := x) (y := y))
              (lambda a a' -> Quotient_eq (R := rel)
                                          (a := a) (a' := a') unit)
              x y;


%% Lemma 6.10.2 in HoTT book, to prove this we need to
%% apply propositional truncation on SurjectiveQuotientProof.
SurjectiveQuotientProof = typecons (SurjectiveQuotientProof (l1 ::: TypeLevel)
                                                            (l2 ::: TypeLevel)
                                                            (A : Type_ l1)
                                                            (R : A -> A -> Type_ l2)
                                                            (x : Quotient A R))
                          (surjectiveQuotientProof (a : A) (p : Eq (Quotient_in (R := R) a) x));
surjectiveQuotientProof = datacons SurjectiveQuotientProof surjectiveQuotientProof;

%% FIXME: This should be made universe polymorphic, but this is contingent upon the
%% truncation type itself being polymorphic.
Quotient_in_surjective : (A : Type) ≡> (R : A -> A -> Type) ≡> (x : Quotient A R) ->
                         PropTrunc (SurjectiveQuotientProof A R x);
Quotient_in_surjective = lambda A R ≡>
  elimProp (R := R)
           (P := lambda x -> PropTrunc (SurjectiveQuotientProof A R x))
           (lambda x -> squash (P := SurjectiveQuotientProof A R x))
           (lambda a ->
             inPropTrunc (P := SurjectiveQuotientProof A R
                                                       (Quotient_in (R := R) a))
                         (surjectiveQuotientProof (R := R) a Eq_refl));

%% Given a proof that a unary operation preserves the underlying
%% relation, we can apply the operation to the quotiented type.
quotUnaryOp : (A : Type_ ?)
              ≡> (R : A -> A -> Type_ ?)
              ≡> (op : A -> A)
              -> ((a : A) -> (a' : A) -> R a a' -> R (op a) (op a'))
              -> Quotient A R -> Quotient A R;
quotUnaryOp = lambda _ _ A R ≡>
              lambda op h x ->
                 let
                   opPreservesQuotient : (a : A) -> (a' : A) -> R a a' ->
                                         Eq (t := Quotient A R)
                                            (Quotient_in (op a))
                                            (Quotient_in (op a'));
                   opPreservesQuotient a a' r = Quotient_eq (R := R) (h a a' r);
                 in
                    Quotient_rec (R := R)
                                  (lambda a -> Quotient_in (op a))
                                  (p := opPreservesQuotient)
                                  x;

%%
%% Quotient effectiveness
%%

inR : (?A -> ?A -> Type_ ?) -> ?A -> ?A -> Type_ ?;
inR R a b = Erased (R a b);

%% FIXME: Move this to a different module
type isEquivRel (l1 ::: TypeLevel) (l2 ::: TypeLevel)
                (A : Type_ l1) (R : A -> A -> Type_ l2) : Type_ (_∪_ l1 l2)
  | equivRel (isRefl : (a : A) -> R a a)
             (isSym : (a : A) -> (b : A) -> R a b -> R b a)
             (isTrans : (a : A) -> (b : A) -> (c : A)
                        -> R a b -> R b c -> R a c);

%% FIXME: Simplify the type signature after metavar elaboration bug is fixed
effective : (A : Type_ ?) ≡> (R : A -> A -> Type_ ?) -> isEquivRel A (inR R)
            -> (a : A) -> (b : A)
            -> Eq (Quotient_in (R := inR R) a) (Quotient_in (R := inR R) b)
            -> Erased (R a b);
effective = lambda l1 l2 A ≡> lambda R equiv a b eq ->
  let
    helper : Quotient A (inR R) -> Type_ l2; %% We want to return R a -
    helper x =
      %% FIXME: We are cheating here! Type_ l2 is not a `Set`, we shouldn't
      %% be allowed to do this !? We should return it in some kind of container
      %% that is indeed a `Set`
      Quotient_rec (R := inR R)
                   (lambda c -> inR R a c)
                   (p := lambda c d cd ->
                     let
                       ac->ad : Erased (R a c) -> Erased (R a d);
                       ac->ad ac = equiv.isTrans a c d ac cd;
                       ad->ac : Erased (R a d) -> Erased (R a c);
                       ad->ac ad = equiv.isTrans a d c ad (equiv.isSym c d cd);
                     in
                       propExt ac->ad ad->ac)
                   x;
    aa=ab : Eq (Erased (R a a)) (Erased (R a b));
    aa=ab = Eq_cong helper eq;
  in
    Eq_cast (p := aa=ab) (f := id) (equiv.isRefl a);
