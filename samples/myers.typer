%%% myers.typer --- Myers's lists

%% Adaptation of Myers list from ocaml file `myers.ml`

%% We could replace mnil and mcons by nil and cons
%% when load and list.typer are ready
%%
t : Type -> Type;
type t (a : Type)
  | mnil
  | mcons (data : a) (link1 : (t a)) (i : Int) (link2 : (t a));

%%
%% Contrary to Myers's presentation, we index from the top of the stack,
%% and we don't store the total length but the "skip distance" instead.
%% This makes `cons' slightly faster, and better matches our use for
%% debruijn environments.
%%

%%
%% Prepend an element `x` to a Myers list `l`
%%
cons : (a : Type) ≡> a -> t a -> t a;
cons x l = case l
  | mcons _ _ s1 l1 => ( case l1
    | mcons _ _ s2 l2 => ( case (Int_>= s1 s2)
      | true => mcons x l (s1 + s2 + 1) l2
      | false => mcons x l 1 l )
    | _ => mcons x l 1 l )
  | _ => mcons x l 1 l;

%%
%% Get the head of a Myers list `l`
%%   (Get the first element)
%% Returns `some x` if the list is non-empty or `none` if it is empty
%%
car : (a : Type) ≡> t a -> Option a;
car l = case l
  | mnil => none
  | mcons x _ _ _ => some x;

%%
%% Get the tail of a Myers list `l`
%%   (Get the same list without its first element)
%% Always returns `mnil` if the list is empty
%%
cdr : (a : Type) ≡> t a -> t a;
cdr l = case l
  | mnil => mnil
  | mcons _ l _ _ => l;

%%
%% Pass the first element to a user function and return the result
%%   or return a default value passed as argument
%%
match : (a : Type) ≡> (b : Type) ≡> t a -> b -> (a -> t a -> b) -> b;
match l n c = case l
  | mnil => n
  | mcons x l _ _ => c x l;

%%
%% Is the Myers list empty?
%%
empty? : (a : Type) ≡> t a -> Bool;
empty? l = case l
  | mnil => true
  | _ => false;

%%
%% Remove the n'th first element from the Myers list
%%   (It is the same as `(cdr (cdr ...))` with `cdr` called `n` times
%%    but may be faster for greater `n`)
%%
nthcdr : (a : Type) ≡> Int -> t a -> t a;
nthcdr n l = if_then_else_ (Int_eq n 0) l
  ( case l
    | mnil => mnil
    | mcons _ l1 s l2 => ( case (Int_>= n s)
      | true => nthcdr (n - s) l2
      | false => nthcdr (n - 1) l1
    )
  );

%%
%% Get the n'th element of a Myers list
%%
nth : (a : Type) ≡> Int -> t a -> Option a;
nth n l = car (nthcdr n l);

%%
%% Update the n'th element of a Myers list
%% Takes the index, the new value and the list
%% Returns the updated list
%%
%% (While `nth` is O(log N), `set-nth` is O(N)! :-( )
%%
set-nth : (a : Type) ≡> Int -> a -> t a -> t a;
set-nth n v l = let

  %% I should use the new case_ macro here!

  type Helper (a : Type)
    | pat1 (idx : Int) (data : a) (link1 : t a) (i : Int) (link2 : t a)
    | pat2 (idx : Int);

  helper : (a : Type) ≡> Int -> t a -> Helper a;
  helper n l = case l
    | mnil => pat2 n
    | mcons a l1 i l2 => pat1 n a l1 i l2;

  in case (helper n l)

  | pat1 n vp cdr s tail => if_then_else_ (Int_eq n 0)
    ( mcons v cdr s tail )
    ( cons vp (set-nth (n - 1) v cdr) )

  %%
  %% We can't set-nth past the end in general because we'd need to
  %% magically fill the intermediate entries with something of the right type.
  %% But we *can* set-nth just past the end.
  %%

  | pat2 n => if_then_else_ (Int_eq n 0)
    ( mcons v mnil 1 mnil )
    ( l ); % should throw an error here!

%%
%% Get the element count of a Myers list
%%
%% (This operation would be more efficient using Myers's choice of keeping
%% the length (instead of the skip-distance) in each node.)
%%
length : (a : Type) ≡> t a -> Int;
length l = let

  lengthp : t a -> Int -> Int;
  lengthp l n = case l
    | mnil => n
    | mcons _ _ s l => lengthp l (s + n);

in lengthp l 0;

%%
%% Find the first element for which the predicate `p' is true.
%% "Binary" search, assuming the list is "sorted" (i.e. all elements after
%% this one also return true).
%%
find : (a : Type) ≡> (a -> Bool) -> t a -> Option a;
find p l = let

  find1 : t a -> Option a;
  find2 : t a -> t a -> Option a;

  find2 l1 l2 = case l2
    | mcons x l1 _ l2 => ( case (p x)
      | false => find2 l1 l2
      | true => find1 l
    )
    | _ => find1 l;

  find1 l = case l
    | mnil => none
    | mcons x l1 _ l2 => ( case (p x)
      | true => some x
      | false => find2 l1 l2
    );

in find1 l;

%%
%% Find the last node for which the predicate `p' is false.
%% "Binary" search, assuming the list is "sorted" (i.e. all elements after
%% this one also return true).
%%
findcdr : (a : Type) ≡> (a -> Bool) -> t a -> t a;
findcdr p l = let

  maybev : Option (t a) -> t a;
  maybev ov = case ov
    | none => mnil
    | some l => l;

  findcdr1 : Option (t a) -> t a -> t a;
  findcdr2 : Option (t a) -> t a -> t a -> t a;

  findcdr2 last l1 l2 = case l2
    | mcons x l1p _ l2p => ( case (p x)
      | false => findcdr2 (some l2) l1p l2p
      | true => findcdr1 last l1
    )
    | _ => findcdr1 last l1;

  findcdr1 last l = case l
    | mnil => maybev last
    | mcons x l1 _ l2 => ( case (p x)
      | true => maybev last
      | false => findcdr2 (some l) l1 l2
    );

in findcdr1 none l;

%%
%% As `fold_left` of any functional language but on Myers list
%%
foldl : (a : Type) ≡> (b : Type) ≡> (b -> a -> b) -> b -> t a -> b;
foldl f i l = case l
  | mnil => i
  | mcons x l _ _ => foldl f (f i x) l;

%%
%% As 'fold_right` of any functional language but on Myers list
%%
foldr : (a : Type) ≡> (b : Type) ≡> (a -> b -> b) -> t a -> b -> b;
foldr f l i = case l
  | mnil => i
  | mcons x l _ _ => f x (foldr f l i);

%%
%% As `map` of any functional language but on Myers list
%% (Apply a function to all element of the list)
%%
map : (a : Type) ≡> (b : Type) ≡> (a -> b) -> t a -> t b;
map f l = let

  fp : a -> t b -> t b;
  fp x lp = cons (f x) lp;

in foldr fp l mnil;

%%
%% Apply all element of a Myers list to a user function (one by one)
%% Takes a function and a list
%% Returns the length of the list
%%
iteri : (a : Type) ≡> (Int -> a -> Unit) -> t a -> Int;
iteri f l = let

  fp : (Int -> a -> Int);
  fp i x = let _ = (f i x); in i + 1;

in foldl fp 0 l;
