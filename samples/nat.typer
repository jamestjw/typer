Nat : Type;
Nat = typecons (dNat) (zero) (succ Nat);

zero = datacons Nat zero;
succ = datacons Nat succ;

to-num : Nat -> Int;
to-num = lambda (x : Nat) -> case x
  | (succ y) => (1 + (to-num y))
  | zero => 0;

plus : Nat -> Nat -> Nat;
plus = lambda (x : Nat) -> lambda (y : Nat) -> case x
  | zero => y
  | succ z => succ (plus z y);

minus : Nat -> Nat -> Nat;
minus = lambda (x : Nat) -> lambda (y : Nat) -> case x
  | zero => zero
  | succ m => case y
                | zero => x
                | succ n => minus m n;

one = succ zero;
two = succ one;
three = succ two;

%% `even1` is added just to have a more complex mutual-recursion,
%% where the order of declarations and definitions don't match at all.
even1 : Nat -> Int;
even : Nat -> Int;
odd : Nat -> Int;
even1 = lambda (n : Nat) -> case n
  | zero => 1
  | succ y => (odd y);
odd = lambda (n : Nat) -> case n
  | zero => 0
  | succ y => (even y);

even = lambda (n : Nat) -> case n
  | zero => 1
  | succ y => (odd y);



a = odd one;
b = even one;
c = odd two;
d = even two;

%% Properties

_+_ = plus;

Nat+_zero : (m : Nat) -> Eq (m + zero) m;
Nat+_zero m =
  case m return (Eq (m + zero) m)
         | zero => Eq_refl
         | succ n => Eq_cong succ (Nat+_zero n);

Nat+_succ : (m : Nat) -> (n : Nat) -> Eq (m + succ n) (succ (m + n));
Nat+_succ m n =
  case m return (Eq (m + succ n) (succ (m + n)))
         | zero => Eq_refl
         | succ m' => Eq_cong succ (Nat+_succ m' n);

Nat+_comm : (m : Nat) -> (n : Nat) -> Eq (m + n) (n + m);
Nat+_comm m n =
  case n return (Eq (m + n) (n + m))
         | zero => Nat+_zero m
         | succ n' => Eq_trans (Nat+_succ m n')
                               (Eq_cong succ (Nat+_comm m n'));

%% Canonicity with funext and cast
%% Example from: https://www.cs.nott.ac.uk/~psztxa/publ/obseqnow.pdf

Eq_funext = let l = load "samples/hott.typer" in l.Eq_funext;

plus0 : (x : Nat) -> Eq (zero + x) (x + zero);
plus0 x = Nat+_comm zero x;

zero+x≡x+zero : Eq (t := Nat -> Nat)
                   (lambda x -> zero + x) (lambda x -> x + zero);
zero+x≡x+zero = Eq_funext (f := lambda x -> zero + x)
                          (g := lambda x -> x + zero)
                          plus0;

%% This computes to zero
canon0 : Eq (Eq_cast (p := zero+x≡x+zero) (f := lambda _ -> Nat) zero)
            zero;
canon0 = Eq_refl;


canon1 : Eq ((Eq_cast (p := zero+x≡x+zero) (f := lambda _ -> Nat -> Nat)
                      (lambda x -> zero + x)) (succ zero))
            (succ zero);
canon1 = Eq_refl;

succ≠zero : (n : Nat) -> Not (Eq (succ n) zero);
succ≠zero n p =
  Eq_cast (f := lambda n -> case n | zero => Void | succ _ => Unit)
          (p := p) unit;

zero≠succ : (n : Nat) -> Not (Eq zero (succ n));
zero≠succ n p =
  Eq_cast (f := lambda n -> case n | zero => Unit | succ _ => Void)
          (p := p) unit;

injSuc : (m : Nat) => (n : Nat) => Eq (succ m) (succ n) -> Eq m n;
injSuc p = Eq_cong (lambda x -> case x | zero => x | succ x' => x') p;
