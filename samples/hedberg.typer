%% Proof of Hedberg's theorem.
%% Source: A coherence theorem for Martin-Lof's type theory - MICHAEL HEDBERG

%% WTS: If a type has decidable equality, then it is a set, i.e. its equalities
%% are trivial.

%% A proof that a type A is either inhabited or not.
type Dec (l ::: TypeLevel) (A : Type_ l) : Type_ l
  | yes A
  | no (A -> False);

%% J-rule
%%
%% We can define the J-rule based on Eq_cast. This definition also makes use
%% of the η-equivalence of Eq.
J : (x : ?A) => (y : ?A) => (P : (y' : ?A) -> Eq x y' -> ?) ≡> P x Eq_refl
    -> (p : Eq x y) -> P y p;
J = lambda lA lP A ≡> lambda x y => lambda P ≡> lambda pxr p ->
    Eq_cast (p := Eq_eq (f := lambda i ≡>
                          P (Eq_uneq (p := p) (i := i))
                            (Eq_eq (f := lambda j ≡>
                                         Eq_uneq (p := p) (i := I_meet i j)))))
            (f := id) pxr;

%% Identity coercion
%%
%% `Bₛₜ*` is defined as `Eq_cast` in Typer. When passed reflexivity, it acts
%% as an identity function on `B(s)`, hence satisfying the required definitional
%% equality.

%% Identity mapping

%% `f̂ₛₜ` is defined as `Eq_cong` in Typer. And it also satisfies the necessary
%% definitional equality.

%% A groupoid law
%% We want to prove that: u ⁻¹ ∙ u ≡ refl. First, we need some lemmas.

%% refl ≡ refl ⁻¹
%% This makes use of the β-rule of Eq_uneq which says that if we have
%% `refl`, then we can return the endpoint of the equality proof regardless
%% of what `i` is.
symRefl : (x : ?A) => Eq (Eq_refl (x := x)) (Eq_comm Eq_refl);
symRefl = lambda l t ≡> lambda x => Eq_refl;

%% refl ≡ refl ∙ refl
compPathRefl : (x : ?A) => Eq (Eq_refl (x := x)) (Eq_trans Eq_refl Eq_refl);
compPathRefl = lambda l t ≡> lambda x => Eq_refl;

%% u ⁻¹ ∙ u ≡ refl
invrI : (a : ?A) -> (b : ?A) -> (u : Eq a b)
        -> Eq (Eq_trans (Eq_comm u) u) Eq_refl;
invrI a b u =
  let
    %% refl ⁻¹ ∙ refl ≡ refl
    p : Eq (Eq_trans (Eq_comm Eq_refl) Eq_refl)
           (Eq_refl (x := a));
    p = Eq_trans (Eq_comm Eq_refl) Eq_refl
          ==< Eq_cong (lambda x -> Eq_trans x Eq_refl) symRefl >==
        Eq_trans Eq_refl Eq_refl
          ==< Eq_comm compPathRefl >==
        Eq_refl ∎;
  in
    J (P := lambda y w -> Eq (Eq_trans (Eq_comm w) w) Eq_refl)
      p u;

%% Constancy lemma

con : (z : Dec ?A) -> ?A -> ?A;
con z a = case z
    | yes a' => a'
    | no _   => a;

isCon : (z : Dec ?A) -> (x : ?A) -> (y : ?A) -> Eq (con z x) (con z y);
isCon z x y = case z return Eq (con z x) (con z y)
      | yes a => Eq_refl
      | no contra => ##case_ (contra x); %% ⊥-elim

%% Collapse lemma
%%
%% If there is defined, on some type a constant function with a left inverse,
%% then the type is collapsed.

%% is_c is a proof that f is constant
%% is_li is a proof that g is a left-inverse of f
collaps : (f : ?A -> ?A) -> (g : ?A -> ?A)
          -> (is_c : (x : ?A) -> (x' : ?A) -> Eq (f x) (f x'))
          -> (is_li : (x : ?A) -> Eq (g (f x)) x)
          -> (a : ?A) -> (b : ?A) -> Eq a b;
collaps f g is_c is_li a b =
  let
    gfb≡b : Eq (g (f b)) b;
    gfb≡b = is_li b;

    gfa≡gfb : Eq (g (f a)) (g (f b));
    gfa≡gfb = Eq_cong g (is_c a b);

    a≡gfa : Eq a (g (f a));
    a≡gfa = Eq_comm (is_li a);
  in
    Eq_trans a≡gfa (Eq_trans gfa≡gfb gfb≡b);

%% Left inverse lemma
%%
%% For any family of functions ν_xy : I(x, y) → I(x, y) [x, y ∈ A] there is a
%% corresponding family of left inverses, ν'_xy : I(x,y) → I(x,y) [x,y ∈ A].

%% nt is the v_xy mentioned above
leftinv : (nt : (x : ?A) -> (y : ?A) -> Eq x y -> Eq x y)
          -> (a : ?A) -> (b : ?A) -> Eq a b -> Eq a b;
leftinv nt a b v = Eq_trans (Eq_comm (nt a a Eq_refl)) v;

%% Proof that `leftinv` is really a left-inverse
isleftinv : (nt : (x : ?A) -> (y : ?A) -> Eq x y -> Eq x y)
            -> (a : ?A) -> (b : ?A) -> (u : Eq a b)
            -> Eq (leftinv nt a b (nt a b u)) u;
isleftinv nt a b u =
  let
    h : Eq (leftinv nt a a (nt a a Eq_refl)) Eq_refl;
    h = invrI a a (nt a a Eq_refl);
  in
    J (P := lambda y w -> Eq (leftinv nt a y (nt a y w)) w) h u;


%% DI⊆CI-theorem
%%
%% Any type with decidable identity types has unique identity proofs.

%% Use the constancy lemma to define a family of constant functions
%% v_xy : I(x, y) → I(x, y)
condi : (di : (x : ?A) -> (y : ?A) -> Dec (Eq x y))
        -> (x : ?A) -> (y : ?A) -> Eq x y -> Eq x y;
condi di x y u = con (di x y) u;

%% The left inverse lemma gives us a left inverse for the above family of
%% functions. This implies that we can use the collapse lemma to show that
%% I(x, y) is collapsed for arbitrary elements x y of type A.
dici : (di : (x : ?A) -> (y : ?A) -> Dec (Eq x y)) -> (a : ?A) -> (b : ?A)
       -> (u : Eq a b) -> (v : Eq a b) -> Eq u v;
dici di a b u v = collaps (condi di a b)
                          (leftinv (condi di) a b)
                          (isCon (di a b))
                          (isleftinv (condi di) a b)
                          u v;

%% Sample:
%% Proof of `isSet Nat`
nat-lib = load "samples/nat.typer";
Nat = nat-lib.Nat;
zero = nat-lib.zero;
succ = nat-lib.succ;

Nat-isSet : (n₁ : Nat) -> (n₂ : Nat) -> (p₁ : Eq n₁ n₂) -> (p₂ : Eq n₁ n₂)
            -> Eq p₁ p₂;
Nat-isSet =
  let
    Nat-decidable : (n₁ : Nat) -> (n₂ : Nat) -> Dec (Eq n₁ n₂);
    Nat-decidable n₁ n₂ =
      case n₁ return Dec (Eq n₁ n₂)
        | zero =>
            (case n₂ return Dec (Eq zero n₂)
               | zero => yes Eq_refl
               | succ n₂' => no (nat-lib.zero≠succ n₂'))
        | succ n₁' =>
            (case n₂ return Dec (Eq (succ n₁') n₂)
               | zero => no (nat-lib.succ≠zero n₁')
               | succ n₂' => (case (Nat-decidable n₁' n₂')
                                | yes p => yes (Eq_cong succ p)
                                | no contra =>
                                    no (lambda sucn₁≡sucn₂ ->
                                         contra (nat-lib.injSuc sucn₁≡sucn₂))));
  in
    dici Nat-decidable;
