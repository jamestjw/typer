(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Typerlib
open Utest_lib

open Positivity

exception WrongPolarity of Sexp.vname

type polarity =
  | Positive
  | Negative

let print_lexp lexp = ut_string 3 (Lexp.to_string lexp ^ "\n")

let assert_polarity polarity lexp vname =
  match (polarity, positive 0 lexp) with
  | (Positive, false) | (Negative, true) -> raise (WrongPolarity vname)
  | _ -> ()

let lexp_decl_str (source : string) : (Sexp.vname * Lexp.lexp) list =
  let (decls, _) = Elab.lexp_decl_str source Elab.default_ectx in
  assert (List.length decls > 0);
  let decls = List.flatten decls in
  List.map (fun (vname, lexp, _) -> print_lexp lexp; (vname, lexp)) decls

(**
 * Check that all the values declared in the source have the given polarity.
 *
 * Only declarations are checked, you may introduce variables with any polarity
 * in the bindings of let or lambda expressions.
 *)
let check_polarity (polarity : polarity) (source : string) : int =
  let decls = lexp_decl_str source in
  try
    List.iter (fun (vname, lexp) -> assert_polarity polarity lexp vname) decls;
    success
  with
  | WrongPolarity _ -> failure

let add_polarity_test polarity name sample =
  add_test "POSITIVITY" name (fun () -> check_polarity polarity sample)

let add_positivity_test = add_polarity_test Positive

let add_negativity_test = add_polarity_test Negative

let () = add_positivity_test
  "a variable is positive in itself"
  {|
    x : Type;
    x = x;
  |}

let () = add_positivity_test
  "a variable is positive in some other variable"
  {|
    y : Type;
    y = Int;
    x : Type;
    x = y;
  |}

let () = add_positivity_test
  "an arrow is positive in a variable that appears in its right hand side"
  {|
    x : Type;
    x = Int -> x;
  |}

let () = add_negativity_test
  "an arrow is negative in a variable that appears in its left hand side"
  {|
    x : Type;
    x = x -> Int;
  |}

let () = add_positivity_test
  "an application of a variable is positive if it does not occur in its arguments"
  {|
    GoodList : Type -> Type;
    GoodList = typecons
      (GoodList (a : Type))
      (good_nil)
      (good_cons (GoodList a));
  |}

let () = add_negativity_test
  "an application of a variable is negative if it occurs in its arguments"
  {|
    EvilList : Type -> Type;
    EvilList = typecons
      (EvilList (a : Type))
      (evil_nil)
      (evil_cons (EvilList (EvilList a)));
  |}

let () = run_all ()
