(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>. *)

(** Tests for the compilation of Lexps to Gambit Scheme code.

   There is more support code in this file than I would have liked. This is due
   to the fact that there is a lot of α-renaming and gensymming so we can't
   directly compare s-expressions. Instead, I defined predicates for lists,
   strings, known symbols, unknown gensymmed symbols, etc. *)

open Typerlib

open Utest_lib

open Gambit

let fail_on_exp exp =
  Printf.printf "in %s\n" (Scm.to_string exp);
  false

let type_error expected actual =
  Printf.printf
    "%sExpected %s, but got %s%s\n"
    Fmt.red
    expected
    (Scm.describe actual)
    Fmt.reset;
  fail_on_exp actual

let string expected actual =
  match actual with
  | Scm.String value when String.equal expected value -> true
  | Scm.String _
    -> Printf.printf
         "%sExpected a string equal to \"%s\"%s\n"
         Fmt.red
         expected
         Fmt.reset;
       fail_on_exp actual
  | _ -> type_error "a string" actual

let sym expected actual =
  match actual with
  | Scm.Symbol value when String.equal expected value -> true
  | Scm.Symbol _
    -> Printf.printf
         "%sExpected a symbol equal to |%s|%s\n"
         Fmt.red
         expected
         Fmt.reset;
       fail_on_exp actual
  | _ -> type_error "a symbol" actual

let has_prefix prefix value =
  String.length prefix < String.length value
  && String.equal prefix (String.sub value 0 (String.length prefix))

type var = Var of string | UniqueVar of string

let make_var prefix = ref (Var prefix)

let gensym var actual =
    match actual with
    | Scm.Symbol value
      -> (match !var with
          | UniqueVar prev when String.equal value prev -> true
          | UniqueVar prev
            -> Printf.printf
                 "%sExpected a symbol equal to |%s|%s\n"
                 Fmt.red
                 prev
                 Fmt.reset;
               fail_on_exp actual
          | Var prefix when has_prefix (prefix ^ "%") value
            -> var := UniqueVar value;
               true
          | Var prefix
            -> Printf.printf
                 "%sExpected a symbol starting with |%s|%s\n"
                 Fmt.red
                 prefix
                 Fmt.reset;
               fail_on_exp actual)
    | _ -> type_error "a symbol" actual

(* Returns whether the s-expression is list and that that the element at an
   index satifies the predicate at the same index. *)
let list (expected : (Scm.t -> bool) list) (actual : Scm.t) : bool =
  match actual with
  | Scm.List value
    -> let rec loop i es xs =
         match es, xs with
         | e :: es, x :: xs
           -> if e x
              then loop (i + 1) es xs
              else fail_on_exp actual
         | _ :: _, [] | [], _ :: _
           -> Printf.printf
                "%sExpected a list of %d elements, but got %d elements%s\n"
                Fmt.red
                (List.length expected)
                (List.length value)
                Fmt.reset;
              fail_on_exp actual
         | [], [] -> true
       in
       loop 0 expected value
  | _ -> type_error "a list" actual

let test_compile name source predicate =
  let compile () =
    let ectx = Elab.default_ectx in
    let lctx = Debruijn.ectx_to_lctx ectx in
    let lexps = Elab.lexp_expr_str source ectx in
    let elexps = List.map (Opslexp.erase_type lctx) lexps in
    let sctx = ScmContext.of_lctx lctx in
    let actual = List.map (scheme_of_expr sctx) elexps in
    match actual with
    | head :: []
      -> if predicate head
         then success
         else failure
    | _
      -> Printf.printf "expected 1 expression, but got %d" (List.length actual);
         failure
  in
  Utest_lib.add_test "GAMBIT" name compile

let () =
  test_compile
    "Typer string to Scheme string"
    {|"Hello"|}
    (string "Hello")

let () =
  let var = make_var "x" in
  test_compile
    "Typer lambda to Scheme lambda"
    {|lambda (x : Int) -> x|}
    (list [sym "lambda"; list [gensym var]; gensym var])

let () =
  test_compile
    "Typer data cons of 0 arguments to Scheme vector"
    {|datacons (typecons T V) V|}
    (list [sym "##vector"; list [sym "quote"; sym "V"]])

let () =
  let var = make_var "" in
  test_compile
    "Typer data cons of 1 argument to Scheme function"
    {|datacons (typecons T (V Int)) V|}
    (list [sym "lambda";
           list [gensym var];
           list [sym "##vector";
                 list [sym "quote"; sym "V"];
                 gensym var]])

let () =
  let var_0 = make_var "" in
  let var_1 = make_var "" in
  test_compile
    "Typer data cons of 2 arguments to Scheme function"
    {|datacons (typecons T (V Int Int)) V|}
    (list [sym "lambda";
           list [gensym var_0];
           list [sym "lambda";
                 list [gensym var_1];
                 list [sym "##vector";
                       list [sym "quote"; sym "V"];
                       gensym var_0;
                       gensym var_1]]])

let () = Utest_lib.run_all ()
