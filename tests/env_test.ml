(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * --------------------------------------------------------------------------- *)

open Typerlib

open Utest_lib
open Env

let dsinfo = DB.dsinfo

let rctx = Elab.default_rctx

let make_val value = Vstring(value)


let _ = (add_test "ENV" "Set Variables" (fun () ->

    if 10 <= (!global_verbose_lvl) then (

        let var = [
            ((dsinfo, Some "a"), DB.type_integer, (make_val "a"));
            ((dsinfo, Some "b"), DB.type_integer, (make_val "b"));
            ((dsinfo, Some "c"), DB.type_integer, (make_val "c"));
            ((dsinfo, Some "d"), DB.type_integer, (make_val "d"));
            ((dsinfo, Some "e"), DB.type_integer, (make_val "e"));
        ] in

        let n = (List.length var) - 1 in

        let rctx = List.fold_left
                     (fun ctx (n, _, _) ->
                       add_rte_variable n Vundefined ctx)
                     rctx var in

        print_rte_ctx rctx;

        let rctx, _ = List.fold_left
                        (fun (ctx, idx) (n, _, _) ->
                          (set_rte_variable idx n Vundefined ctx);
                          (ctx, idx - 1))
                        (rctx, n) var in

        print_rte_ctx rctx);
    success))

let _ = (add_test "ENV" "Value Printer" (fun () ->
  let open Lexp in
  let exps = [((Vint 42),
               (mkArrow (dsinfo, Aerasable, (dsinfo, Some "l"),
                         DB.type_interval, DB.type_integer)),
               "(lambda _ ≡> 42)");
              ((Vbuiltin "Heq.eq"),
               (mkCall (dsinfo, DB.type_heq,
                        [Aerasable, mkVar ((dsinfo, None), 2);
                         Aerasable, mkLambda (Aerasable, (dsinfo, None),
                                              DB.type_interval,
                                              DB.type_integer);
                         Anormal, mkVar ((dsinfo, Some "x"), 0);
                         Anormal, mkVar ((dsinfo, Some "y"), 0)])),
               "x = y [ ##Integer ]")] in
    (* This test assumes that success = 0 and failure = -1 *)
    List.fold_left
      (fun res (v, ltype, expected)
        -> Int.min res
                   (expect_equal_str (value_string_with_type v ltype DB.empty_lctx)
                                     expected))
      0 exps))

(* run all tests *)
let _ = run_all ()
