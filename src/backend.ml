(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** A backend is a consumer of fully eleborated and checked lexps. This module
   provides interfaces for (batch) compilers and interactive backends. *)

open Lexp

type value = Env.value_type

class virtual t = object
  method virtual process_decls : ldecls -> unit
end

class virtual compiler = object
  inherit t
end

class virtual interactive = object
  inherit t

  method virtual eval_expr : lexp -> value

  method virtual print_rte_ctx : unit

  method virtual dump_rte_ctx : unit
end
