;;; Copyright (C) 2021-2022  Free Software Foundation, Inc.
;;;
;;; Author: Simon Génier <simon.genier@umontreal.ca>
;;; Keywords: languages, lisp, dependent types.
;;;
;;; This file is part of Typer.
;;;
;;; Typer is free software; you can redistribute it and/or modify it under the
;;; terms of the GNU General Public License as published by the Free Software
;;; Foundation, either version 3 of the License, or (at your option) any later
;;; version.
;;;
;;; Typer is distributed in the hope that it will be useful, but WITHOUT ANY
;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;; FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
;;; details.
;;;
;;; You should have received a copy of the GNU General Public License along with
;;; this program. If not, see <http://www.gnu.org/licenses/>.

;;; This file implments a simple protocol where Gambit will listen on stdin for
;;; s-expressions and echo the pretty-printed version of the same expression on
;;; stdout.

;; Read an 8-bit unsigned integer or call the given continuation.
(define (read-u8-or k)
  (let ((b (read-u8)))
    (if (eof-object? b) (k))
    b))

;; Read a 32-bit unsigned integer or call the given continuation.
(define (read-u32be-or k)
  (let ((b3 (read-u8-or k))
        (b2 (read-u8-or k))
        (b1 (read-u8-or k))
        (b0 (read-u8-or k)))
    (bitwise-ior (arithmetic-shift b3 24)
                 (arithmetic-shift b2 16)
                 (arithmetic-shift b1 8)
                 b0)))

(define (write-u32be n)
  (write-u8 (bitwise-and #xff (arithmetic-shift n -24)))
  (write-u8 (bitwise-and #xff (arithmetic-shift n -16)))
  (write-u8 (bitwise-and #xff (arithmetic-shift n -8)))
  (write-u8 (bitwise-and #xff n)))

;; Read a string prefixed by its length encoded as a 32-bit unsigned integer.
(define (read-sized-string k)
  (let* ((bytes-length (read-u32be-or k))
         (bytes (make-u8vector bytes-length)))
    (read-subu8vector bytes 0 bytes-length)
    (utf8->string bytes)))

;; Write a string prefixed by its length encoded as a 32-bit unsigned integer.
(define (write-sized-string string)
  (let* ((bytes (string->utf8 string))
         (bytes-length (u8vector-length bytes)))
    (write-u32be bytes-length)
    (write-subu8vector bytes 0 bytes-length)
    (force-output (current-output-port))))

(call-with-current-continuation
 (lambda (k)
   (let loop ()
     (write-sized-string
      (with-output-to-string
        (lambda ()
          (with-input-from-string (read-sized-string k)
            (lambda ()
              (pretty-print (read)))))))
     (loop))))
