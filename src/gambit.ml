(* Copyright (C) 2021  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

open Debruijn
open Elexp
open Lexp
open Printf
open Sexp

let gsc_path : string ref = ref "/usr/local/Gambit/bin/gsc"

module Scm = struct
  type t =
    | Symbol of string
    | List of t list
    | String of string
    | Integer of Z.t
    | Float of float

  let begin' : t = Symbol "begin"
  let case : t = Symbol "case"
  let define : t = Symbol "define"
  let double_right_arrow : t = Symbol "=>"
  let else' : t = Symbol "else"
  let lambda : t = Symbol "lambda"
  let let' : t = Symbol "let"
  let letrec : t = Symbol "letrec"
  let quote : t = Symbol "quote"
  let prim_vector : t = Symbol "##vector"
  let prim_vector_ref : t = Symbol "##vector-ref"

  let is_valid_bare_symbol (name : string) : bool =
    let length = String.length name in
    let rec loop i =
      if Int.equal length i
      then true
      else
        match name.[i] with
        | '|' | '\'' | ' ' | '(' | ')' -> false
        | _ -> loop (i + 1)
    in loop 0

  let escape_symbol (name : string) : string =
    if is_valid_bare_symbol name
    then name
    else
      let buffer = Buffer.create (String.length name) in
      Buffer.add_char buffer '|';
      let encode_char = function
        | '|' -> Buffer.add_string buffer {|\||}
        | c -> Buffer.add_char buffer c
      in
      String.iter encode_char name;
      Buffer.add_char buffer '|';
      Buffer.contents buffer

  let symbol (name : string) : t =
    Symbol (escape_symbol name)

  let rec to_string (exp : t) : string =
    match exp with
    | Symbol (name) -> name
    | List (elements)
      -> "(" ^ String.concat " " (List.map to_string elements) ^ ")"
    | String (value)
      -> let length = String.length value in
         let buffer = Buffer.create length in
         Buffer.add_char buffer '"';
         let encode_char = function
           | '\x00' .. '\x06' as c
             -> (Buffer.add_string buffer "\\x";
                 Buffer.add_string buffer (Printf.sprintf "%02d" (Char.code c)))
           | '\x07' -> Buffer.add_string buffer "\\a"
           | '\x08' -> Buffer.add_string buffer "\\b"
           | '\x09' -> Buffer.add_string buffer "\\t"
           | '\\' -> Buffer.add_string buffer "\\\\"
           | '"' -> Buffer.add_string buffer "\\\""
           | c -> Buffer.add_char buffer c
         in
         String.iter encode_char value;
         Buffer.add_char buffer '"';
         Buffer.contents buffer
    | Integer (value) -> Z.to_string value
    | Float (value) -> string_of_float value

  let print (output : out_channel) (exp : t) : unit =
    output_string output (to_string exp);
    output_char output '\n'

  let describe (exp : t) : string =
    match exp with
    | Symbol _ -> "a symbol"
    | List _ -> "a list"
    | String _ -> "a string"
    | Integer _ -> "an integer"
    | Float _ -> "a float"

  let rec equal (l : t) (r : t) : bool =
    match l, r with
    | Symbol l, Symbol r | String l, String r
      -> String.equal l r
    | List ls, List rs -> List.equal equal ls rs
    | Integer l, Integer r -> Z.equal l r
    | Float l, Float r -> Float.equal l r
    | _ -> false
end

let gensym : string -> string =
  let counter = ref 0 in
  fun name ->
  let i = !counter in
  counter := i + 1;
  let name = sprintf "%s%%%d" name i in
  Scm.escape_symbol name

module ScmContext = struct
  (* Maps Debruijn indices to Scheme variable names. *)
  type t = string Myers.myers

  let nil : t = Myers.nil

  let of_lctx (lctx : lexp_context) : t =
    let f ((_, name), _, _) = Option.get name in
    Myers.map f lctx

  let intro (sctx : t) (_, name : vname) : t * string =
    let name = gensym (Option.value ~default:"g" name) in
    (Myers.cons name sctx, name)

  let intro_binding (sctx : t) (name, elexp : vname * elexp) =
    let sctx, name = intro sctx name in
    (sctx, (name, elexp))

  let lookup (sctx : t) (index : int) : string option =
    Myers.nth_opt index sctx
end

let rec scheme_of_expr (sctx : ScmContext.t) (elexp : elexp) : Scm.t =
  match elexp with
  | Elexp.Imm (Sexp.String (_, s)) -> Scm.String s

  | Elexp.Imm (Sexp.Integer (_, i)) -> Scm.Integer i

  | Elexp.Imm (Sexp.Float (_, f)) -> Scm.Float f

  | Elexp.Builtin (_, "Sexp.node") -> Scm.Symbol "cons"

  | Elexp.Builtin (_, "Sexp.symbol") -> Scm.Symbol "string->symbol"

  | Elexp.Builtin (_, name) -> failwith ("TODO: Built-in \"" ^ name ^ "\"")

  | Elexp.Var (_, index)
    -> (match ScmContext.lookup sctx index with
        | None -> Scm.Symbol "TODO"
        | Some name -> Scm.Symbol name)

  | Elexp.Let (_, bindings, body)
    -> if List.length bindings = 0
       then scheme_of_expr sctx body
       else
         let sctx, bindings =
           List.fold_left_map ScmContext.intro_binding sctx bindings
         in
         let scheme_of_binding (name, elexp) =
           Scm.List [Scm.Symbol name; scheme_of_expr sctx elexp]
         in
         Scm.List [Scm.letrec;
                   Scm.List (List.map scheme_of_binding bindings);
                   scheme_of_expr sctx body]

  | Elexp.Lambda (name, body)
    -> let sctx', name = ScmContext.intro sctx name in
       Scm.List [Scm.lambda;
                 Scm.List [Scm.Symbol name];
                 scheme_of_expr sctx' body]

  | Elexp.Call (head, tail)
    -> let scm_head = scheme_of_expr sctx head in
       let scm_tail = List.map (scheme_of_expr sctx) tail in
       Scm.List (scm_head :: scm_tail)

  | Elexp.Cons (arity, (_, name))
    -> let rec make_vars i vars =
         match i with
         | 0 -> vars
         | _ -> make_vars (i - 1) (Scm.Symbol (gensym "") :: vars)
       in
       let vars = make_vars arity [] in
       let vector_expr =
         Scm.List (Scm.prim_vector
                   :: Scm.List [Scm.quote; Scm.symbol name]
                   :: List.rev vars)
       in
       let rec make_curried_lambda vars body =
         match vars with
         | [] -> body
         | var :: vars'
           -> let body' = Scm.List [Scm.lambda; Scm.List [var]; body] in
              make_curried_lambda vars' body'
       in
       make_curried_lambda vars vector_expr

  | Elexp.Case (_, target, branches, default_branch)
    -> let target_name =
         match target with
         | Elexp.Var _ -> scheme_of_expr sctx target
         | _ -> Scm.Symbol (gensym "target")
       in
       let scm_default_branch =
         match default_branch with
         | None -> []
         | Some (name, body)
           -> let sctx, name = ScmContext.intro sctx name in
              (* (else => (lambda (name) body)) *)
              [Scm.List [Scm.else';
                         Scm.double_right_arrow;
                         Scm.List [Scm.lambda;
                                   Scm.List [Scm.Symbol name];
                                   scheme_of_expr sctx body]]]
       in
       let add_branch cons_name (_, field_names, branch_body) scm_branches =
         let make_binding (sctx, bindings, i) (location, name) =
           match name with
           | Some "_" -> sctx, bindings, Z.succ i
           | _
             -> let sctx, name = ScmContext.intro sctx (location, name) in
                let binding = Scm.List [Scm.Symbol name;
                                        Scm.List [Scm.prim_vector_ref;
                                                  target_name;
                                                  Scm.Integer i]]
                in
                sctx, binding :: bindings, Z.succ i
         in
         let sctx, bindings, _ = List.fold_left make_binding (sctx, [], Z.one) field_names in
         let scm_branch =
           Scm.List [Scm.List [Scm.symbol cons_name];
                     if Int.equal (List.length bindings) 0
                     then scheme_of_expr sctx branch_body
                     else
                       Scm.List [Scm.let';
                                 Scm.List (List.rev bindings);
                                 scheme_of_expr sctx branch_body]]
         in
         scm_branch :: scm_branches
       in
       let scm_branches =
         List.rev_append (SMap.fold add_branch branches []) scm_default_branch
       in
       let scm_case =
         Scm.List (Scm.case
                   :: Scm.List [Scm.prim_vector_ref;
                                target_name;
                                Scm.Integer Z.zero]
                   :: scm_branches)
       in
       (match target with
        | Elexp.Var _ -> scm_case
        | _
          -> Scm.List [Scm.let';
                       Scm.List [Scm.List [target_name;
                                           scheme_of_expr sctx target]];
                       scm_case])

  | Elexp.Type _ -> Scm.Symbol "lets-hope-its-ok-if-i-erase-this"

  | _
    -> failwith
         (Format.asprintf "[gambit] unsupported elexp: %a" Elexp.pp_print elexp)

class inferior script_path =
  let down_reader, down_writer = Unix.pipe ~cloexec:true () in
  let up_reader, up_writer = Unix.pipe ~cloexec:true () in
  let _ = Unix.clear_close_on_exec down_reader in
  let _ = Unix.clear_close_on_exec up_writer in
  let pid =
    Unix.create_process
      !gsc_path
      [|!gsc_path; "-i"; "-f"; script_path|]
      down_reader
      up_writer
      Unix.stderr
  in
  let _ = Unix.close down_reader in
  let _ = Unix.close up_writer in

  object (self)
    val pid = pid
    val writer_fd = down_writer
    val reader_fd = up_reader
    val short_buffer = Bytes.make 4 '\x00'

    method stop : unit =
      Unix.close writer_fd;
      let _ = Unix.waitpid [] pid in
      Unix.close reader_fd

    method private read_byte : char =
      self#read ~size:1 short_buffer;
      Bytes.get short_buffer 0

    method private write_byte (b : char) : unit =
      Bytes.set short_buffer 0 b;
      self#write ~size:1 short_buffer

    method private read_sized_string : string =
      self#read ~size:4 short_buffer;
      let size = Int32.to_int (Bytes.get_int32_be short_buffer 0) in
      let buffer = Bytes.create size in
      self#read ~size buffer;
      Bytes.to_string buffer

    method private write_sized_string (string : string) : unit =
      let buffer = Bytes.of_string string in
      let size = Bytes.length buffer in
      Bytes.set_int32_be short_buffer 0 (Int32.of_int size);
      self#write ~size:4 short_buffer;
      self#write ~size buffer

    method private read ~(size : int) (bytes : Bytes.t) : unit =
      let n = ref 0 in
      while !n < size do
        n := !n + Unix.read reader_fd bytes !n (size - !n)
      done

    method private write ~(size : int) (bytes : Bytes.t) : unit =
      assert (Unix.write writer_fd bytes 0 size = size)
  end

class pretty_printer =
  object (self)
    inherit inferior "src/pretty-printer.scm"

    method pp (expr : Scm.t) : string =
      let source = Scm.to_string expr in
      self#write_sized_string source;
      self#read_sized_string
  end

let scheme_of_decl (sctx : ScmContext.t) (name, elexp : string * elexp) : Scm.t =
  Scm.List ([Scm.define; Scm.Symbol name; scheme_of_expr sctx elexp])

class gambit_compiler lctx output = object
  inherit Backend.compiler

  val mutable sctx = ScmContext.of_lctx lctx
  val mutable lctx = lctx
  val pretty_printer = new pretty_printer

  method process_decls ldecls =
    let lctx', eldecls = Opslexp.clean_decls lctx ldecls in
    let sctx', eldecls = List.fold_left_map ScmContext.intro_binding sctx eldecls in
    let sdecls = List.map (scheme_of_decl sctx') eldecls in

    sctx <- sctx';
    lctx <- lctx';
    List.iter
      (fun sdecl -> sdecl |> pretty_printer#pp |> output_string output)
      sdecls

  method stop =
    pretty_printer#stop
end
