(* Copyright (C) 2023  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

include Stdlib.String

(* Backport from 5.0. *)
let hash : t -> int = Hashtbl.hash

(* Backport from 4.13. *)
let fold_left (type a) (f : a -> char -> a) (a : a) (chars : string) : a =
  let rec loop n a =
    if n < length chars
    then loop (n + 1) (f a (get chars n))
    else a
  in
  loop 0 a

let for_all (f : char -> bool) (chars : string) : bool =
  let rec loop n =
    if n = length chars
    then true
    else if f (get chars n)
    then loop (n + 1)
    else false
  in
  loop 0
