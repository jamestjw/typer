(* log.ml --- User Errors/Warnings log for Typer.  -*- coding: utf-8 -*-

Copyright (C) 2019-2020  Free Software Foundation, Inc.

Author: Jean-Alexandre Barszcz <jalex_b@hotmail.com>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Util

open Printf

(* LOGGING LEVELS *)

type log_level = Nothing | Fatal | Error | Warning | Info | Debug

let int_of_level lvl =
  match lvl with
  | Nothing -> -1 (* We might not want to print anything during testing *)
  | Fatal -> 0 | Error -> 1 | Warning -> 2 | Info -> 3 | Debug -> 4

let level_of_int i =
  match i with
  | _ when i < 0  -> Nothing
  | 0 -> Fatal | 1 -> Error | 2 -> Warning | 3 -> Info
  | _ -> Debug

let string_of_level lvl =
  match lvl with
  | Nothing -> "Nothing"
  | Fatal -> "Fatal"
  | Error -> "Error"
  | Warning -> "Warning"
  | Info -> "Info"
  | Debug -> "Debug"

let level_of_string str =
  match String.uppercase_ascii str with
  | "NOTHING" -> Nothing
  | "FATAL" -> Fatal
  | "ERROR" -> Error
  | "WARNING" -> Warning
  | "INFO" -> Info
  | "DEBUG" -> Debug
  | _ -> invalid_arg ("`"^str^"` is not a logging level")

let level_color lvl =
  match lvl with
  | Nothing -> None
  | Fatal -> Some Fmt.red
  | Error -> Some Fmt.red
  | Warning -> Some Fmt.yellow
  | Info -> Some Fmt.cyan
  | Debug -> Some Fmt.magenta

(* LOGGING CONFIGURATION *)

type log_config = {
    mutable level : log_level;
    mutable print_at_log : bool;
    mutable print_in_reverse : bool;
    mutable color : bool;
  }

let typer_log_config = {
    level = Warning;
    print_at_log = false;
    print_in_reverse = false;
    color = true;
  }

(* LOG DEFINITION *)

type log_entry = {
    level : log_level;
    kind : string option;
    section : string option;
    print_action : (unit -> unit) option;
    loc : location option;
    msg : string;
  }

type log = log_entry list

let empty_log = []

let typer_log = ref empty_log

(* PRIVATE OPERATIONS *)

let mkEntry level ?kind ?section ?print_action ?loc msg =
  {level; kind; section; print_action; loc; msg}

let log_push (entry : log_entry) =
  typer_log := entry::(!typer_log)

let maybe_color_string color_opt str =
  match color_opt with
  | Some color -> Fmt.color_string color str
  | None -> str

let string_of_log_entry {level; kind; section; loc; msg; _} =
  let color = if typer_log_config.color then level_color level else None in
  let kind_string = Option.value ~default:(string_of_level level) kind in
  let parens s = "(" ^ s ^ ")" in
  sprintf
    "%s %s:%s %s"
    (Option.value ~default:"" (Option.map Source.Location.to_string loc))
    (maybe_color_string color kind_string)
    (Option.value ~default:"" (Option.map parens section))
    msg

let print_entry entry =
  print_endline (string_of_log_entry entry);
  match entry.print_action with
  | Some f -> f ()
  | None -> ()

let log_entry (entry : log_entry) =
  if (entry.level <= typer_log_config.level)
  then (
    log_push entry;
    if (typer_log_config.print_at_log)
    then
      (print_entry entry;
       flush stdout)
  )

let count_msgs (lvlp : log_level -> bool) =
  let count_step count {level; _} =
    if lvlp level then count + 1 else count
  in
  List.fold_left count_step 0 !typer_log

let error_count () = count_msgs (fun level -> level <= Error)
let warning_count () = count_msgs (fun level -> level = Warning)

(* PUBLIC INTERFACE *)

let set_typer_log_level lvl =
  typer_log_config.level <- lvl

let set_typer_log_level_str str =
  try set_typer_log_level (level_of_int (int_of_string str))
  with
    _ -> set_typer_log_level (level_of_string str)

let increment_log_level () : unit =
  typer_log_config.level
  |> int_of_level
  |> (+) 1
  |> level_of_int
  |> set_typer_log_level

let clear_log () = typer_log := empty_log

let print_log () =
  let reverse = typer_log_config.print_in_reverse in
  let l = if reverse then !typer_log else List.rev !typer_log in
  List.iter print_entry l;
  typer_log := empty_log

let log_msg
      (k : string -> 'd)
      level
      ?kind
      ?section
      ?print_action
      ?loc
      (fmt : ('a, unit, string, 'd) format4)
    : 'a =

  let k' message =
    message
    |> mkEntry level ?kind ?section ?print_action ?loc
    |> log_entry;
    k message
  in
  ksprintf k' fmt

exception Internal_error of string
let internal_error fmt =
  ksprintf (fun s -> raise (Internal_error s)) fmt

exception User_error of string
let user_error fmt =
  ksprintf (fun s -> raise (User_error s)) fmt

exception Stop_compilation of string
let stop_compilation fmt =
  ksprintf (fun s -> raise (Stop_compilation s)) fmt

let show_error (message : string) : unit =
  print_log ();
  print_endline message;
  flush stdout

let handle_error ~(on_error : unit -> 'a) (action : unit -> 'a) : 'a =
  try action () with
  | Stop_compilation message
    -> show_error message;
       on_error ()
  | User_error message
    -> show_error ("fatal user error: " ^ message);
       on_error ()
  (* Don't catch these internal errors, so `OCAMLRUNPARAM=b` can
   * give us a usable backtrace.
   * | Internal_error message
   *   -> show_error ("internal error: " ^ message);
   *      on_error () *)

let log_fatal ?section ?print_action ?loc fmt =
  typer_log_config.print_at_log <- true;
  log_msg
    (fun _ -> internal_error "Compiler Fatal Error")
    Fatal
    ~kind:"[X] Fatal    "
    ?section
    ?print_action
    ?loc
    fmt

let log_error ?section ?print_action ?loc fmt =
  log_msg (fun _ -> ()) Error ?kind:None ?section ?print_action ?loc fmt

let log_warning ?section ?print_action ?loc fmt =
  log_msg (fun _ -> ()) Warning ?kind:None ?section ?print_action ?loc fmt

let log_info ?section ?print_action ?loc fmt =
  log_msg (fun _ -> ()) Info ?kind:None ?section ?print_action ?loc fmt

let log_debug ?section ?print_action ?loc fmt =
  log_msg (fun _ -> ()) Debug ?kind:None ?section ?print_action ?loc fmt

let stop_on_error () =
  let count = error_count () in
  if 0 < count then
    stop_compilation "Compiler stopped after: %d errors\n" count

let stop_on_warning () =
  stop_on_error ();
  let count = warning_count () in
  if 0 < count then
    stop_compilation "Compiler stopped after: %d warnings\n" count

(* Compiler Internal Debug print *)
let debug_msg expr =
    if Debug <= typer_log_config.level then (print_string expr; flush stdout)

let not_implemented_error () = internal_error "not implemented"
