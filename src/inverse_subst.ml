(* inverse_subst.ml --- Computing the inverse of a substitution

Copyright (C) 2016-2022  Free Software Foundation, Inc.

Author: Vincent Bonnevalle <tiv.crb@gmail.com>

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

(* During unification, we often have problems of the form
 *
 *     X[σ] = e
 *
 * Where X is a metavariable and `e` can be any term.  We can solve this
 * problem by computing the inverse of the substitution, σ⁻¹, and use:
 *
 *     X = e[σ⁻¹]
 *
 * where σ⁻¹ is such that e[σ⁻¹][σ] = e[σ⁻¹ ∘ σ] = e
 *
 * Another way to look at it is that
 *
 *     X[σ] = e   ==>    X[σ][σ⁻¹] = e[σ⁻¹]
 *
 * so if we can find a σ⁻¹ such that σ ∘ σ⁻¹ = Id, we have X = e[σ⁻¹]
 *
 * So either left or right inverse can be used!
 *)

open Lexp
open Util
module S = Subst

(** Provide inverse function for computing the inverse of a substitution *)

(* Transformation *)

(** Convenience types and variables *)

type inter_subst = lexp list (* Intermediate between "tree"-like substitution and fully flattened subsitution *)

type substIR = ((int * int) list * int * int)

let dsinfo = Sexp.dummy_sinfo

(** Transform a substitution to a more linear substitution
 * makes the inversion easier
 * Example of result : ((new_idx, old_position)::..., shift)*)
let transfo (s: subst) : substIR option =
  let rec transfo (s: subst) (off_acc: int) (idx: int) (imp_cnt : int)
          : substIR option =
    let indexOf (v: lexp): int = (* Helper : return the index of a variabble *)
      match lexp_lexp' v with
      | Var (_, v) -> v
      | _          -> assert false
    in
    let shiftVar (var: lexp) (offset: int): int =
      indexOf (mkSusp var (S.shift offset)) (* Helper : shift the index of a var *)
    in
    match s with
    | S.Cons (e, s, o) when is_var e
      -> let off_acc = off_acc + o in
       (match transfo s off_acc (idx + 1) imp_cnt with
        | Some (tail, off, imp)
          -> let newVar = shiftVar e off_acc
            in if newVar >= off then None (* Error *)
               else Some (((shiftVar e off_acc), idx)::tail, off, imp)
        | None             -> None)
    | S.Cons (e, s, o)
      when pred_imm e (fun s -> Sexp.pred_symbol s (fun n -> n = ""))
      -> transfo s (o + off_acc) (idx + 1) (imp_cnt + 1)
    | S.Identity o        -> Some ([], (o + off_acc), imp_cnt)
    | _                   -> None (* Error *)
  in transfo s 0 0 0

(* Inverse *)

(** Returns the number of element in a sequence of S.Cons
*)
let sizeOf (s: (int * int) list): int = List.length s

(** Returns a dummy variable with the db_index idx
 *)
let counter = ref 0
let mkVar (idx: int) : lexp =
  counter := !counter + 1;
  mkVar ((dsinfo, None), idx)

(** Fill the gap between e_i in the list of couple (e_i, i) by adding
    dummy variables.
    With the exemple of the article :
    should return <code>(1,1)::(2, X)::(3,2)::(4,3)::(5, Z)::[]</code>

    @param l     list of (e_i, i) couples with gap between e_i
    @param size  size of the list to return
    @param acc   recursion accumulator
*)
let fill (l: (int * int) list) (nbVar: int) (shift: int): subst option =
  let rec genDummyVar (beg_: int) (end_: int) (l: subst): subst = (* Create the filler variables *)
    if beg_ < end_
    then S.cons impossible (genDummyVar (beg_ + 1) end_ l)
    else l
  in
  let fill_before (l: (int * int) list) (s: subst) (nbVar: int): subst option = (* Fill if the first var is not 0 *)
    match l with
    | []                      -> Some (genDummyVar 0 nbVar s)
    | (i1, _v1)::_ when i1 > 0 -> Some (genDummyVar 0 i1 s)
    | _                       -> Some s
  in let rec fill_after (l: (int * int) list) (nbVar: int) (shift: int): subst option = (* Fill gaps *)
    match l with
    | (idx1, _val1)::(idx2, _val2)::_tail when (idx1 = idx2)     -> None

    | (idx1, val1)::(idx2, val2)::tail when (idx2 - idx1) > 1 ->
      (match fill_after ((idx2, val2)::tail) nbVar shift with
        | None   -> None
        | Some s -> Some (S.cons (mkVar val1) (genDummyVar (idx1 + 1) idx2 s)))

    | (_idx1, val1)::(idx2, val2)::tail
      -> (match fill_after ((idx2, val2)::tail) nbVar shift with
         | None   -> None
         | Some s -> Some (S.cons (mkVar val1) s))

    | (idx1, val1)::[] when (idx1 + 1) < nbVar
      -> Some (S.cons (mkVar val1)
                     (genDummyVar (idx1 + 1) nbVar (S.shift shift)))

    | (_idx1, val1)::[]
      -> Some (S.cons (mkVar val1) (S.shift shift))

    | []
      -> Some (S.shift shift)
  in match fill_after l nbVar shift with
  | None   -> None
  | Some s -> fill_before l s nbVar

let is_identity s =
  let rec is_identity s acc =
    match s with
    | S.Cons(e, s1, 0) when pred_var e (fun e -> get_var_db_index e = acc)
      -> is_identity s1 (acc + 1)
    | S.Identity o                           -> acc = o
    | _                                      -> S.identity_p s
  in is_identity s 0

(** Compute the inverse, if there is one, of the substitution.

    <code>s:S.subst, l:lexp, s':S.subst</code> where <code>l[s][s'] = l</code> and <code> inverse s = s' </code>
*)
let inverse (s: subst) : subst option =
  let sort = List.sort (fun (ei1, _) (ei2, _) -> compare ei1 ei2)
  in match transfo s with
  | None                   -> None
  | Some (cons_lst, shift_val, imp_cnt)
    -> let size  = imp_cnt + sizeOf cons_lst in
      (* print_string ("imp_cnt = " ^ string_of_int imp_cnt
       *               ^ "; shift_val = " ^ string_of_int shift_val
       *               ^ "; size = " ^ string_of_int size
       *               ^ "\n"); *)
      let res = fill (sort cons_lst) shift_val size in
    (match res with
     | Some s1
       -> if is_identity (Lexp.scompose s s1)
           || is_identity (Lexp.scompose s1 s) then ()
          else
            let message =
              Format.asprintf
                "Subst-inversion-bug: %a   ∘   %a    ==   %a   !!\n"
                Lexp.pp_print_subst
                s
                Lexp.pp_print_subst
                s1
                Lexp.pp_print_subst
                (Lexp.scompose s1 s)
            in
            Log.log_debug "%s" message
     | _ -> ());
    res

(* Returns false if the application of the inverse substitution is not
 * possible.  This happens when the substitution replaces some variables
 * with non-variables, in which case the "inverse" is ambiguous.  *)
let rec invertible (s: subst) : bool = match s with
  | S.Identity _ -> true
  | S.Cons (e, s, _)
    -> (match lexp_lexp' e with Var _ -> true | _ -> e = impossible)
                       && invertible s

exception Not_invertible
exception Ambiguous

(* Lookup variable i in s⁻¹ *)
let rec lookup_inv_subst (i : db_index) (s : subst) : db_index
  = match s with
  | (S.Identity o | S.Cons (_, _, o)) when i < o -> raise Not_invertible
  | S.Identity o -> i - o
  | S.Cons (e, s, o) when pred_var e (fun e -> get_var_db_index e = i - o)
    -> (try let i'' = lookup_inv_subst (get_var_db_index (get_var e)) s in
           assert (i'' != 0);
           raise Ambiguous
       with Not_invertible -> 0)
  | S.Cons (e, s, o)
    -> assert (match lexp_lexp' e with Var _ -> true | _ -> e = impossible);
      1 + lookup_inv_subst (i - o) s

(* When going under a binder, we have the rule
 *   (λe)[s]  ==>  λ(e[lift s])
 * where `lift s` is (#0 · (s ↑1))
 *
 * Here, we want to construct a substitution s' such that
 *
 *     (λe)[s⁻¹]  ==>  λ(e[s'⁻¹])
 *
 * So (lift_inv_subst s) is morally equivalent to ((lift (s⁻¹))⁻¹).
 * And it turns out that ((lift (s⁻¹))⁻¹) = lift s:
 *)

(* Return a substitution s' = (↑n ∘ (s⁻¹))⁻¹ = ((s⁻¹)⁻¹ ∘ (↑n)⁻¹) = (s ∘ (↑n)⁻¹)
 * ↑n is a substitution of type Γ → Γ,x₁,…,xₙ
 * So `s⁻¹` should be a substitution Γ,x₁,…,xₙ → Γ'
 * Hence `s` should be a substitution Γ' → Γ,x₁,…,xₙ
 * and we need to return a substitution Γ' → Γ
 *)
let shift_inv_subst n s
  (* We could define it as:
   * = compose_inv_subst s (S.shift n)
   * But this can fail if an element of `s` maps to a var with index < n *)
  = if n = 0 then s else
      let shift_n = match inverse (S.shift n) with
      | Some s -> s
      | _ -> assert (false) in
     Lexp.scompose s shift_n

(* For metavars, we need to compute: s' ∘ s⁻¹
 * One way to do it is to compute s⁻¹ and then pass it to `compose`.
 * But we can try and do it more directly.
 *)
let rec compose_inv_subst (s' : subst) (s : subst) = match s' with
  | S.Cons (e, s', o) ->
     let s = shift_inv_subst o s in
     (* FIXME: Why don't we ever return a Shift?  *)
     S.cons (apply_inv_subst e s) (compose_inv_subst s' s)
  | S.Identity o -> (match inverse (shift_inv_subst o s) with
                    | Some s -> s
                    (* FIXME: could also be Ambiguous, depending on `s`. *)
                    | None -> raise Not_invertible)

(* Apply s⁻¹ to e.
 * The function presumes that `invertible s` was true.
 * This can be used like mkSusp/push_susp, but it's not lazy.
 * This is because it can signal errors Not_invertible or Ambiguous.  *)
and apply_inv_subst (e : lexp) (s : subst) : lexp =
  match lexp_lexp' e with
  | Imm _ -> e
  | SortLevel (SLz) -> e
  | SortLevel (SLsucc e) -> mkSortLevel (mkSLsucc (apply_inv_subst e s))
  | SortLevel (SLlub (e1, e2))
    (* FIXME: use mkSLlub?  *)
    -> mkSortLevel (mkSLlub' (apply_inv_subst e1 s, apply_inv_subst e2 s))
  | Sort (l, Stype e) -> mkSort (l, Stype (apply_inv_subst e s))
  | Sort (_l, (StypeOmega | StypeLevel)) -> e
  | Builtin _ -> e
  | Var (name, i) -> Lexp.mkVar (name, lookup_inv_subst i s)
  | Proj (l,lxp,lbl) -> mkProj (l, apply_inv_subst lxp s, lbl)
  | Susp (e, s') -> apply_inv_subst (push_susp e s') s
  | Let (l, defs, e)
    -> let s' = L.fold_left (fun s (v, _, _) -> ssink v s) s defs in
      let (_,ndefs)
        = L.fold_left (fun (s,ndefs) (v, def, ty)
                       -> (ssink v s,
                          (v, apply_inv_subst def s', apply_inv_subst ty s)
                          :: ndefs))
                      (s, []) defs in
      mkLet (l, ndefs, apply_inv_subst e s')
  | Arrow (l, ak, v, t1, t2)
    -> mkArrow (l, ak, v, apply_inv_subst t1 s,
               apply_inv_subst t2 (ssink v s))
  | Lambda (ak, v, t, e)
    -> mkLambda (ak, v, apply_inv_subst t s, apply_inv_subst e (ssink v s))
  | Call (l, f, args)
    -> mkCall (l, apply_inv_subst f s,
              L.map (fun (ak, arg) -> (ak, apply_inv_subst arg s)) args)
  | Inductive (l, label, args, cases)
    -> let (s, nargs)
        = L.fold_left (fun (s, nargs) (ak, v, t)
                       -> (ssink v s, (ak, v, apply_inv_subst t s) :: nargs))
                      (s, []) args in
      let nargs = List.rev nargs in
      let ncases = SMap.map (fun args
                             -> let (_, ncase)
                                 = L.fold_left (fun (s, nargs) (ak, v, t)
                                                -> (ssink v s,
                                                   (ak, v, apply_inv_subst t s)
                                                   :: nargs))
                                               (s, []) args in
                               L.rev ncase)
                            cases in
      mkInductive (l, label, nargs, ncases)
  | Cons (it, name) -> mkCons (apply_inv_subst it s, name)
  | Case (l, e, ret, cases, default)
    -> mkCase (l, apply_inv_subst e s, apply_inv_subst ret s,
              SMap.map (fun (l, cargs, e)
                        -> let s' = L.fold_left
                                     (fun s (_,ov) -> ssink ov s)
                                     s cargs in
                           let s'' = ssink (l, None) s' in
                           (l, cargs, apply_inv_subst e s''))
                       cases,
              match default with
              | None -> default
              | Some (v,e) -> Some (v, apply_inv_subst e (ssink (l, None) (ssink v s))))
  | Metavar (id, s', name)
    -> match metavar_lookup id with
      | MVal e -> apply_inv_subst (push_susp e s') s
      | MVar _ -> mkMetavar (id, compose_inv_subst s' s, name)
