(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Specifies recursive data structure for DeBruijn indexing
 *          methods starting with '_' are considered private and should not
 *          elsewhere in the project
 *
 * ---------------------------------------------------------------------------*)

open Fmt
open Lexp
open Sexp
open Util

module M = Myers

let fatal ?print_action ?loc fmt =
  Log.log_fatal ~section:"DEBRUIJN" ?print_action ?loc fmt

(**          Sets of DeBruijn indices          **)

type set = db_offset * unit IMap.t

let set_empty = (0, IMap.empty)

let set_mem i (o, m) = IMap.mem (i - o) m

let set_set i (o, m) = (o, IMap.add (i - o) () m)
let set_reset i (o, m) = (o, IMap.remove (i - o) m)

let set_singleton i = (0, IMap.singleton i ())

(* Adjust a set for use in a deeper scope with `o` additional bindings.  *)
let set_sink o (o', m) = (o + o', m)

(* Adjust a set for use in a higher scope with `o` fewer bindings.  *)
let set_hoist o (o', m) =
  let newo = o' - o in
  let (_, _, newm) = IMap.split (-1 - newo) m
  in (newo, newm)

let set_union (o1, m1) (o2, m2) : set =
  if o1 = o2 then
    (o1, IMap.merge (fun _k _ _ -> Some ()) m1 m2)
  else
    let o = o2 - o1 in
    (o1, IMap.fold (fun i2 () m1
                    -> IMap.add (i2 + o) () m1)
                   m2 m1)

let set_ascending_seq ((o, m) : set) : int Seq.t =
  IMap.to_seq m |> Seq.map (fun x -> fst x + o)

(* Handling scoping/bindings is always tricky.  So it's always important
 * to keep in mind for *every* expression which is its context.
 *
 * In particular, this holds true as well for those expressions that appear
 * in the context.  Traditionally for dependently typed languages we expect
 * the context's rules to say something like:
 *
 *      ⊢ Γ    Γ ⊢ τ:Type
 *      —————————————————
 *          ⊢ Γ,x:τ
 *
 * Which means that we expect (τ) expressions in the context to be typed
 * within the *rest* of that context.
 *
 * This also means that when we look up a binding in the context, we need to
 * adjust the result, since we need to use it in the context where we looked
 * it up, which is different from the context where it was defined.
 *
 * More concretely, this means that lookup(Γ, i) should return an expression
 * where debruijn indices have been shifted by "i".
 *
 * This is nice for "normal bindings", but there's a complication in the
 * case of recursive definitions.  Typically, this is handled by using
 * something like a μx.e construct, which works OK for the theory but tends
 * to become rather inconvenient in practice for mutually recursive
 * definitions.  So instead, we annotate the recursive binding with
 * a "recursion_offset" (of type `db_offset`) to say that rather than being
 * defined in "the rest of the context", they're defined in a slightly larger
 * context that includes "younger" bindings.
 *)


(*  Type definitions
 * ---------------------------------- *)

let dloc   = dummy_location
let dsinfo = dummy_sinfo

let type_level_sort = mkSort (dsinfo, StypeLevel)
let sort_omega = mkSort (dsinfo, StypeOmega)
let type_level = mkBuiltin ((dloc, "TypeLevel"), type_level_sort)
let level0 = mkSortLevel SLz
let level1  = mkSortLevel (mkSLsucc level0)
let level2  = mkSortLevel (mkSLsucc level1)
let type0  = mkSort (dsinfo, Stype level0)
let type1   = mkSort (dsinfo, Stype level1)
let type2   = mkSort (dsinfo, Stype level2)
let type_integer = mkBuiltin ((dloc, "Integer"), type0)
let type_float = mkBuiltin ((dloc, "Float"), type0)
let type_string = mkBuiltin ((dloc, "String"), type0)

(* FIXME: Is this the best way to do this? Originally, I wanted to
 *        define this in Typer and then reference it from OCaml code.
 *)
(* Defining the following:
 *   typecons I i0 i1
 *)
let type_interval
  = mkInductive (dsinfo, (dloc, "I"), [],
                 List.fold_left (fun m name -> SMap.add name [] m)
                                SMap.empty
                                ["i0"; "i1"])

(* FIXME: Is this the best way to do this? Referencing such a definition
 *        in Typer will be a pain because it would depend on the location
 *        in the context.
 *)
let interval_i0 = mkCons (type_interval, (dloc, "i0"))
let interval_i1 = mkCons (type_interval, (dloc, "i1"))


(* FIXME: This definition of `Heq` should preferably be in `builtins.typer`,
 * but we need `type_eq` to handle `Case` expressions in
 * elab/conv_p/check!  :-(  *)
let type_heq_type =
  let lv = (dsinfo, Some "l") in
  let t = (dsinfo, Some "t") in
  mkArrow (dsinfo, Aerasable, lv,
           type_level,
           mkArrow (dsinfo, Aerasable, t,
                    mkArrow (dsinfo, Aerasable, (dsinfo, None), type_interval,
                             mkSort (dsinfo, Stype (mkVar (lv, 1)))),
                    mkArrow (dsinfo, Anormal, (dsinfo, None),
                            mkCall (dsinfo, mkVar (t, 0), [Aerasable, interval_i0]),
                            mkArrow (dsinfo, Anormal, (dsinfo, None),
                                    mkCall (dsinfo, mkVar (t, 1), [Aerasable, interval_i1]),
                                    mkSort (dsinfo, Stype (mkVar (lv, 3)))))))

let type_heq = mkBuiltin ((dloc, "Heq"), type_heq_type)

let builtin_axioms =
  ["Int"; "Elab_Context"; "IO"; "Ref"; "Sexp"; "Array"; "FileHandle"; "Quotient";
   (* From `healp.ml`.  *)
   "Heap"; "DataconsLabel";
   (* Integer axioms *)
   "Integer.1!=0"; "Integer.+-comm"; "Integer.*-comm"; "Integer.*-assoc";
   "Integer.*DistL+"; "Integer.isIntegral"; "Integer.*Lid"; "Integer.*Lzero";
   "Integer.+Linv"; "Integer.+Lid"; "Integer.+-assoc"; "Integer.Negate≡";
   "Integer.*DistL-";]

(* easier to debug with type annotations *)
type env_elem = (vname * varbind * ltype)
type lexp_context = env_elem M.myers

type db_ridx = int (* DeBruijn reverse index (i.e. counting from the root).  *)

(*  Map variable name to its distance in the context *)
type senv_length = int  (* it is not the map true length *)
type senv_type = senv_length * (db_ridx SMap.t)

type lctx_length = db_ridx

type meta_scope
  = scope_level       (* Integer identifying a level.  *)
    * lctx_length     (* Length of ctx when the scope is added.  *)
    * ((meta_id * scope_level) SMap.t ref) (* Metavars already known in this scope.  *)

type typeclass_ctx
  = (ltype * lctx_length) list (* the list of type classes *)
    * bool (* true if new bindings are instances
              (used to implement (dont-)bind-instances) *)
    * set (* The set of bindings that are instances *)

(* This is the *elaboration context* (i.e. a context that holds
 * a lexp context plus some side info.  *)
type elab_context
  = Grammar.t * senv_type * lexp_context * meta_scope * typeclass_ctx

(* Elab context getters *)

let get_size (ctx : elab_context)
  = let (_, (n, _), lctx, _, _) = ctx in
    assert (n = M.length lctx); n

let ectx_get_grammar (ectx : elab_context) : Grammar.t =
  let (grm,_, _, _, _) = ectx in grm

let ectx_get_senv (ectx : elab_context) : senv_type =
  let (_,senv, _, _, _) = ectx in senv

  (* Extract the lexp context from the context used during elaboration.  *)
let ectx_to_lctx (ectx : elab_context) : lexp_context =
  let (_,_, lctx, _, _) = ectx in lctx

let ectx_get_scope (ectx : elab_context) : meta_scope =
  let (_, _, _, scope, _) = ectx in scope

let ectx_to_scope_level ((_, _, _, (sl, _, _), _) : elab_context) : scope_level
  = sl

let ectx_local_scope_size ((_, (_n, _), _, (_, slen, _), _) as ectx) : int
  = get_size ectx - slen

let ectx_to_tcctx (ectx : elab_context) : typeclass_ctx =
  let (_, _, _, _, tcctx) = ectx in tcctx

(* Functional updates *)

let ectx_set_grammar (grm : Grammar.t) (ectx : elab_context) : elab_context =
  let (_, senv, lctx, sl, tcctx) = ectx in
  (grm, senv, lctx, sl, tcctx)

let ectx_set_senv (senv : senv_type) (ectx : elab_context) : elab_context =
  let (grm, _, lctx, sl, tcctx) = ectx in
  (grm, senv, lctx, sl, tcctx)

let ectx_set_lctx (lctx : lexp_context) (ectx : elab_context) : elab_context =
  let (grm, senv, _, sl, tcctx) = ectx in
  (grm, senv, lctx, sl, tcctx)

let ectx_set_meta_scope (ms : meta_scope) (ectx : elab_context) : elab_context =
  let (grm, senv, lctx, _, tcctx) = ectx in
  (grm, senv, lctx, ms, tcctx)

let ectx_set_tcctx (tcctx : typeclass_ctx) (ectx : elab_context) : elab_context =
  let (grm, senv, lctx, sl, _) = ectx in
  (grm, senv, lctx, sl, tcctx)

(*  Public methods: DO USE
 * ---------------------------------- *)

let empty_senv = (0, SMap.empty)
let empty_lctx = M.nil
let empty_tcctx = ([], false, set_empty)

let empty_elab_context : elab_context
  = (Grammar.default_grammar, empty_senv, empty_lctx,
     (0, 0, ref SMap.empty), empty_tcctx)

(* senv_lookup caller were using Not_found exception *)
exception Senv_Lookup_Fail of (string list)
let senv_lookup_fail relateds = raise (Senv_Lookup_Fail relateds)

(* Return its current DeBruijn index.  *)
let senv_lookup (name: string) (ctx: elab_context): int =
  let (n, map) = ectx_get_senv ctx in
  try n - (SMap.find name map) - 1
  with Not_found
       -> let get_related_names (_n : db_ridx) name map =
           let r = Str.regexp (".*"^name^".*") in
           let search r = SMap.fold (fun name _ names
                                     -> if (Str.string_match r name 0) then
                                         name::names
                                       else
                                         names)
                                    map [] in
           if ((String.length name) > 1) &&
                ((String.sub name 0 1) = "_" ||
                 (String.sub name ((String.length name) - 1) 1) = "_") then
             search r
           else [] in

         senv_lookup_fail (get_related_names n name map)

let lexp_ctx_cons (ctx : lexp_context) d v t =
  assert (let offset = match v with | LetDef (o, _) -> o | _ -> 0 in
          offset >= 0
          && (ctx = M.nil
             || match M.car ctx with
               | (_, LetDef (previous_offset, _), _)
                 -> previous_offset >= 0 (* General constraint.  *)
                   (* Either `ctx` is self-standing (doesn't depend on us),
                    * or it depends on us (and maybe other bindings to come), in
                    * which case we have to depend on the exact same bindings.  *)
                   && (previous_offset <= 1
                      || previous_offset = 1 + offset)
               | _ -> true));
  M.cons (d, v, t) ctx

let lctx_extend (ctx : lexp_context) (def: vname) (v: varbind) (t: lexp) =
  lexp_ctx_cons ctx def v t

let tcctx_extend ((tcs, inst_def, insts) : typeclass_ctx) =
  let insts = set_sink 1 insts in
  tcs, inst_def, if inst_def then set_set 0 insts else insts

let tcctx_extend_rec (n : int) ((tcs, inst_def, insts) : typeclass_ctx) =
  let rec set_set_n n s =
    if n = 0 then s else
      set_set_n (n - 1) (set_set (n - 1) s) in
  let insts = set_sink n insts in
  tcs, inst_def, if inst_def then set_set_n n insts else insts

let env_extend_rec (ctx: elab_context) (def: vname) (v: varbind) (t: lexp) =
  let (_loc, oname) = def in
  let (n, map) = ectx_get_senv ctx in
  let nmap = match oname with None -> map | Some name -> SMap.add name n map in
  ctx |> ectx_set_senv (n + 1, nmap)
      |> ectx_set_lctx (lexp_ctx_cons (ectx_to_lctx ctx) def v t)
      |> ectx_set_tcctx (tcctx_extend (ectx_to_tcctx ctx))

let ectx_extend (ctx: elab_context) (def: vname) (v: varbind) (t: lexp) = env_extend_rec ctx def v t

let lctx_extend_rec (ctx : lexp_context) (defs: (vname * lexp * ltype) list) =
  let (ctx, _) =
    List.fold_left
      (fun (ctx, recursion_offset) (def, e, t) ->
        lexp_ctx_cons ctx def (LetDef (recursion_offset, e)) t,
        recursion_offset - 1)
      (ctx, List.length defs) defs in
  ctx

let ectx_extend_rec (ctx: elab_context) (defs: (vname * lexp * ltype) list) =
  let (n, senv) = ectx_get_senv ctx in
  let len = List.length defs in
  let senv', _ = List.fold_left
                   (fun (senv, i) ((_, oname), _, _) ->
                     (match oname with None -> senv
                                     | Some name -> SMap.add name i senv),
                     i + 1)
                   (senv, n) defs in
  ctx |> ectx_set_senv (n + len, senv')
      |> ectx_set_lctx (lctx_extend_rec (ectx_to_lctx ctx) defs)
      |> ectx_set_tcctx (tcctx_extend_rec len (ectx_to_tcctx ctx))

let ectx_new_scope (ectx : elab_context) : elab_context =
  let lctx = ectx_to_lctx ectx in
  let (scope, _, rmmap) = ectx_get_scope ectx in
  ectx_set_meta_scope (scope + 1, Myers.length lctx, ref (!rmmap)) ectx

let env_lookup_by_index index (ctx: lexp_context): env_elem =
  Myers.nth index ctx

let env_lookup_set (s : set) (lctx : lexp_context) : (db_index * env_elem) Seq.t =
  M.nthsi lctx (set_ascending_seq s)

let ectx_add_typeclass (ectx : elab_context) (t : ltype) : elab_context =
  let (tcs, inst_def, insts) = ectx_to_tcctx ectx in
  ectx_set_tcctx ((t, get_size ectx) :: tcs, inst_def, insts) ectx

let ectx_set_inst (ectx : elab_context) (index : int) (inst : bool)
    : elab_context =
  let (tcs, inst_def, insts) = ectx_to_tcctx ectx in
  let ninsts = if inst then set_set index insts else set_reset index insts in
  ectx_set_tcctx (tcs, inst_def, ninsts) ectx

let ectx_set_inst_def (ectx : elab_context) (inst_def : bool) : elab_context =
  let (tcs, _, insts) = ectx_to_tcctx ectx in
  ectx_set_tcctx (tcs, inst_def, insts) ectx

let print_lexp_ctx_n (ctx : lexp_context) (ranges : (int * int) list) =
  print_string (make_title " LEXP CONTEXT ");

  make_rheader
    [(Some ('l',  7), "INDEX");
     (Some ('l',  4), "OFF");
     (Some ('l', 10), "NAME");
     (Some ('l', 42), "VALUE : TYPE")];

  print_string (make_sep '-');

  let prefix = "    |         |      |            | " in

  let print i =
    try
      let r, name, lexp, ty =
        match env_lookup_by_index i ctx with
        | ((_, name), LetDef (r, exp), ty) -> r, name, Some exp, ty
        | ((_, name), _, ty) -> 0, name, None, ty
      in
      let name' = maybename name in
      let short_name =
        if String.length name' > 10
        then
          String.sub name' 0 9 ^ "…"
        else name'
      in
      Printf.printf "    | %-7d | %-4d | %-10s | " i r short_name;
      (match lexp with
       | None -> print_string "<var>"
       | Some lexp
         -> (let str = Lexp.to_string lexp in
             let strs =
               match String.split_on_char '\n' str with
               | hd :: tl -> print_string hd; tl
               | [] -> []
             in
             List.iter
               (fun elem ->
                 print_newline ();
                 print_string prefix;
                 print_string elem)
               strs));
      print_string " : ";
      Lexp.print ty;
      print_newline ()
    with
    | Not_found
      -> print_endline "    | %-7d | Not_found  |"
  in

  let rec print_range lower i =
    let i' = i - 1 in
    if i' >= lower
    then
      (print i';
       print_range lower i')
  in

  let rec print_ranges = function
    | [] -> ()
    | (lower, upper) :: ranges'
      -> (print_range lower upper;
          print_ranges ranges')
  in

  print_ranges ranges;
  print_string (make_sep '=')

(* Only print user defined variables *)
let print_lexp_ctx (lctx : lexp_context) : unit =
  print_lexp_ctx_n lctx [(0, M.length lctx - !builtin_size)]

(* Dump the whole context *)
let dump_lexp_ctx (lctx : lexp_context) : unit =
  print_lexp_ctx_n lctx [(0, M.length lctx)]

let summarize_lctx (lctx : lexp_context) (at : int) : unit =
  let ranges =
    if at < 7
    then [(0, min (max (at + 2) 5) (M.length lctx))]
    else [(at - 2, min (at + 2) (M.length lctx)); (0, 5)]
  in
  print_lexp_ctx_n lctx ranges;
  print_endline "This context was trucated. Pass the option -Vfull-lctx to view it in full."

let log_full_lctx = ref false

(* generic lookup *)
let lctx_lookup (ctx : lexp_context) (v: vref): env_elem  =
  let ((loc, oename), dbi) = v in
  try
    let ret = Myers.nth dbi ctx in
    let _ = match (ret, oename) with
      | (((_, Some name), _, _), Some ename)
        -> (* Check if names match *)
           if not (ename = name) then
             let print_action =
               if !log_full_lctx
               then fun () -> (print_lexp_ctx ctx; print_newline ())
               else fun () -> summarize_lctx ctx dbi
             in
             fatal
               ~loc:(Sexp.location loc) ~print_action
               ({|DeBruijn index %d refers to wrong name.  |}
                ^^ {|Expected: "%s" got "%s"|})
               dbi ename name
      | _ -> () in

    ret
  with
  | Not_found
    -> fatal
         ~loc:(Sexp.location loc)
         "DeBruijn index %d of `%s` out of bounds"
         dbi
         (maybename oename)

let lctx_lookup_type (ctx : lexp_context) (vref : vref) : lexp =
  let (_, i) = vref in
  let (_, _, t) = lctx_lookup ctx vref in
  mkSusp t (S.shift (i + 1))

let lctx_lookup_value (ctx : lexp_context) (vref : vref) : lexp option =
  let (_, i) = vref in
  match lctx_lookup ctx vref with
  | (_, LetDef (o, v), _) -> Some (push_susp v (S.shift (i + 1 - o)))
  | _ -> None

let env_lookup_type ctx (v : vref): lexp =
  lctx_lookup_type (ectx_to_lctx ctx) v

    (* mkSusp ltp (S.shift (idx + 1)) *)

let env_lookup_expr ctx (v : vref): lexp option =
  lctx_lookup_value (ectx_to_lctx ctx) v

type lct_view =
  | CVempty
  | CVlet of vname * varbind * ltype * lexp_context
  | CVfix of (vname * lexp * ltype) list * lexp_context

let lctx_view lctx =
  match lctx with
  | Myers.Mnil -> CVempty
  | Myers.Mcons ((loname, LetDef (1, def), t), lctx, _, _)
    -> let rec loop i lctx defs =
        match lctx with
        | Myers.Mcons ((loname, LetDef (o, def), t), lctx, _, _)
             when o = i
          -> loop (i + 1) lctx ((loname, def, t) :: defs)
        | _ -> CVfix (defs, lctx) in
      loop 2 lctx [(loname, def, t)]
  | Myers.Mcons ((_, LetDef (o, _def), _), _, _, _) when o > 1
    -> fatal "Unexpected lexp_context shape!"
  | Myers.Mcons ((loname, odef, t), lctx, _, _)
    -> CVlet (loname, odef, t, lctx)
