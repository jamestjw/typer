(* REPL.ml --- Read Eval Print Loop (REPL)

Copyright (C) 2016-2023  Free Software Foundation, Inc.

Author: Pierre Delaunay <pierre.delaunay@hec.ca>

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

(*
 *      Description:
 *          Read Eval Print Loop (REPL). It allows you to eval typer code
 *          interactively line by line.
 *
 *      Example:
 *
 *          $./_build/typer [files]
 *            In[ 1] >> sqr = lambda x -> x * x;
 *            In[ 2] >> (sqr 4);
 *              Out[ 2] >> 16
 *            In[ 3] >> let a = 3; b = 3; in
 *                    .     a + b;
 *              Out[ 3] >> 6
 *
 * -------------------------------------------------------------------------- *)

open Printf

open Debruijn
open Eval
open Fmt
open Lexer
open Lexp
open Prelexer
open Sexp
open Util

module OL = Opslexp
module EL = Elexp

let print_input_line i =
  printf "  In[%02d] >> " i

let error fmt = Log.log_error ~section:"REPL" fmt

(*  Read stdin for input. Returns only when the last char is ';'
 *  We can use '%' to prevent parsing
 *  If return is pressed and the last char is not ';' then
 *  indentation will be added                                       *)
let rec read_input i =
    print_input_line i;
    flush stdout;

    let rec loop str i =
        flush stdout;
        let line = input_line stdin in
        let s = String.length str in
        let n = String.length line in
            if s = 0 && n = 0 then read_input i else
            (if n = 0 then (
                print_string "          . ";
                print_string (make_line ' ' (i * 4));
                loop str (i + 1))
            else
        let str = if s > 0 then  String.concat "\n" [str; line] else line in
            if line.[n - 1] = ';' || line.[0] = '%' then
                str
            else (
                print_string "          . ";
                print_string (make_line ' ' (i * 4));
                loop str (i + 1))) in

    loop "" i

let help_msg =
"      %quit         (%q) : leave REPL
      %who          (%w) : print runtime environment
      %info         (%i) : print elaboration environment
      %calltrace    (%ct): print eval trace of last call
      %elabtrace    (%et): print elaboration trace
      %typertrace   (%tt): print typer trace
      %readfile          : read a Typer file
      %help         (%h) : print help
"

(* Elaborate Typer source from an interactive session. Like "normal" Typer,
   declarations are grouped in blocks that are mutually recursive, but unlike
   "normal" Typer, expressions are also accepted in addition to declarations.

   Note that all declarations are pulled before expressions, i.e. an expression
   can refer to variable introduced by later declarations. *)
let eval_interactive
      (interactive : #Backend.interactive)
      (i : int)
      (ectx : elab_context)
      (input : string)
    : elab_context =

  let classify_sexps nodes =
    let rec loop sexps decls exprs =
      match sexps with
      | [] -> (List.rev decls, List.rev exprs)
      | sexp :: sexps
        -> (match sexp with
            | Node (_, Symbol (_, ("_=_" | "_:_")), [Symbol _; _])
              -> loop sexps (sexp :: decls) exprs
            | Node (_, Symbol (_, ("_=_")), [Node _; _])
              -> loop sexps (sexp :: decls) exprs
            | _
              -> loop sexps decls (sexp :: exprs))
    in loop nodes [] []
  in

  let source = Source.of_string ~label:(string_of_int i) input in
  let pretokens = prelex source in
  let tokens = lex Grammar.default_stt pretokens in
  (* FIXME: This is too eager: it prevents one declaration from changing the
     grammar used in subsequent declarations. *)
  let sexps = sexp_parse_all_to_list (ectx_get_grammar ectx) tokens (Some ";") in
  let decls, exprs = classify_sexps sexps in

  (* FIXME We take the parsed input here but we should take the unparsed tokens
     directly instead *)
  let ldecls, ectx' = Elab.lexp_p_decls decls [] ectx in

  let lexprs = Elab.lexp_parse_all exprs ectx' in

  let generalize_lexp ctx lxp =
    Elab.resolve_instances_and_generalize ctx lxp Elab.wrapLambda lxp in
  let lexprs = List.map (generalize_lexp ectx') lexprs in

  let ltypes = List.map (fun lexpr -> OL.check (ectx_to_lctx ectx') lexpr) lexprs in

  List.iter interactive#process_decls ldecls;
  Log.print_log ();

  let values = List.map interactive#eval_expr lexprs in
  List.iter2 (fun v t ->
                Eval.print_eval_result i
                                       v
                                       (Some (t, (ectx_to_lctx ectx'))))
             values ltypes;
  Log.print_log ();

  ectx'

(*  Specials commands %[command-name] [args] *)
let rec repl i (interactive : #Backend.interactive) (ectx : elab_context) =
  let ipt = try read_input i with End_of_file -> "%quit" in
  let ectx =
    match ipt with
    (*  Check special keywords *)
    | "%quit" | "%q" -> exit 0
    | "%help" | "%h" -> (print_string help_msg; ectx)
    | "%calltrace"  | "%ct" -> (print_eval_trace None; ectx)
    | "%typertrace" | "%tt" -> (print_typer_trace None; ectx)
    | "%lcollisions" | "%cl" -> (get_stats_hashtbl (WHC.stats hc_table); ectx)

    (* command with arguments *)
    | _ when ipt.[0] = '%' && ipt.[1] != ' '
      -> (match (str_split ipt ' ') with
          | "%readfile"::args
            -> let ectx' =
                 List.fold_left (Elab.process_file interactive) ectx args
               in
               Log.print_log ();
               ectx'
          | "%who"::args | "%w"::args
            -> let _ = match args with
                 | ["all"] -> interactive#dump_rte_ctx
                 | _       -> interactive#print_rte_ctx in
               ectx
          | "%info"::args | "%i"::args
            -> let _ = match args with
                 | ["all"] -> dump_lexp_ctx (ectx_to_lctx ectx)
                 | _       -> print_lexp_ctx (ectx_to_lctx ectx) in
               ectx

          | cmd::_
            -> error {|"%s" is not a correct REPL command|} cmd;
               ectx

          | _ -> ectx)

    (* eval input *)
    | _
      -> Log.handle_error
           ~on_error:(Fun.const ectx)
           (fun () -> eval_interactive interactive i ectx ipt)
  in
  repl (i + 1) interactive ectx
