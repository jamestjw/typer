(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2022  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Type Erased Lexp expression
 *
 * --------------------------------------------------------------------------- *)

open Sexp

module SMap = Util.SMap

type elexp =
  (* A constant, either string, integer, or float.  *)
  | Imm of sexp

  (* A builtin constant, typically a function implemented in Ocaml.  *)
  | Builtin of symbol

  (* A variable reference, using deBruijn indexing.  *)
  | Var of vref

  | Proj of Source.Location.t * elexp * int

  (* Recursive `let` binding.  *)
  | Let of Source.Location.t * eldecls * elexp

  (* An anonymous function.  *)
  | Lambda of vname * elexp

  (* A (curried) function call.
   * In other words,         Call(f, [e1, e2])
   * is just a shorthand for Call (Call (f, [e1]), [e2]).  *)
  | Call of elexp * elexp list

  (* A data constructor, such as `cons` or `nil`. The int is its arity and the
   * symbol, its name. *)
  | Cons of int * symbol

  (* Case analysis on an agebraic datatype.
   * Case (l, e, branches, default)
   * tests the value of `e`, and either selects the corresponding branch
   * in `branches` or branches to the `default`.  *)
  | Case of Source.Location.t
            * elexp
            * (Source.Location.t * vname list * elexp) SMap.t
            * (vname * elexp) option

  (* A Type expression.  There's no useful operation we can apply to it,
   * but they can appear in the code.  *)
  | Type of Lexp.lexp

and eldecl = vname * elexp
and eldecls = eldecl list

let rec location : elexp -> Source.Location.t = function
  | Imm s -> Sexp.location s
  | Var ((l, _), _) -> Sexp.location l
  | Proj (l, _, _) -> l
  | Builtin ((l, _)) -> l
  | Let (l, _, _) -> l
  | Lambda ((l, _), _) -> Sexp.location l
  | Call (f, _) -> location f
  | Cons (_, (l, _)) -> l
  | Case (l, _, _, _) -> l
  | Type (e) -> Lexp.location e

let rec pp_print (f : Format.formatter) (e : elexp) : unit =
  let open Format in
  match e with
  | Imm (value)
    -> Sexp.pp_print f value

  | Var (name, index)
    -> Lexp.pp_print_var f name index

  | Proj (_, e, field_name)
    -> Lexp.pp_print_proj f pp_print e pp_print_int field_name

  | Builtin (name)
    -> Lexp.pp_print_builtin f name

  | Let (_, decls, body)
    -> Lexp.pp_print_let f pp_print_decl_block decls pp_print body

  | Lambda (param_name, body)
    -> Lexp.pp_print_lambda
         f
         (fun f param_name
          -> fprintf f "(%a)" (Sexp.pp_print_vname ~default:"_") param_name)
         param_name
         Pexp.Anormal
         pp_print
         body

  | Call (callee, args)
    -> Lexp.pp_print_call f pp_print callee pp_print args

  | Cons (_, name)
    -> fprintf f "@{<magenta>datacons@}@ %a" Sym.pp_print name

  | Case (_, e, branches, default)
    -> Lexp.pp_print_case
         f pp_print e (Sexp.pp_print_vname ~default:"_") branches default

  | Type (e)
    -> fprintf f "Type(%a)" Lexp.pp_print e

and pp_print_decl_block (f : Format.formatter) (decls : eldecls) : unit =
  let open Format in
  let pp_print_decl (f : Format.formatter) (name, body : eldecl) : unit =
    fprintf f "@[<hov 2>%s =@ %a;@]" (string_of_vname name) pp_print body
  in
  fprintf
    f
    "@[<v 0>%a@]"
    (pp_print_list ~pp_sep:pp_print_space pp_print_decl)
    decls

let pp_print_decls (f : Format.formatter) (eldecls : eldecls list) : unit =
  let open Format in
  fprintf
    f
    "@[<v 0>%a@]"
    (pp_print_list ~pp_sep:pp_print_cut pp_print_decl_block)
    eldecls

let to_string : elexp -> string =
  Format.asprintf "%a" pp_print
