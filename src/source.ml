(* Copyright (C) 2021-2022  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(** Traditionally, line numbers start at 1… *)
let first_line_of_file : int = 1

let first_column_of_line : int = 0

let is_utf8_head (c : char) : bool =
  Char.code c < 128 || Char.code c >= 192

(** A point is a position within a source file. *)
module Point = struct
  type t = {offset : int; line : int; column : int}

  let zero : t =
    {
      offset = 0;
      line = first_line_of_file;
      column = first_column_of_line;
    }

  (** Two points are equal if they refer to the same offset in a givent file.
      The line and column are metadata and do not participate in determining
      equality. *)
  let equal (l : t) (r : t) : bool =
    l.offset = r.offset

  (** Like [equal], but also checks the line and column. *)
  let same (l : t) (r : t) : bool =
    l.offset = r.offset
    && l.line = r.line
    && l.column = r.column

  let min (l : t) (r : t) : t =
    if l.offset < r.offset
    then l
    else r

  let max (l : t) (r : t) : t =
    if l.offset < r.offset
    then r
    else l

  let (<=) (l : t) (r : t) : bool =
    l.offset <= r.offset
end

(** A container is a thing that can hold Typer source code. *)
module Container = struct
  type lines = string Array.t

  type t =
    | File of string * lines (** A file can contain source code. *)
    | String of string * lines (** A string entred in a REPL can contain source
                                   code. *)

  let dummy : t = String ("<dummy>", [|""|])

  let of_string ~(label : string) (contents : string) : t =
    let lines = contents |> String.split_on_char '\n' |> Array.of_list in
    String (label, lines)

  (** The name of a container is either a path or a label in the case of a
      string entred in the REPL. *)
  let name : t -> string = function
    | File (path, _) -> path
    | String (label, _) -> label

  (** The lines of source code inside the container. There is always at least
      one empty line. *)
  let lines : t -> string array = function
    | File (_, lines) -> lines
    | String (_, lines) -> lines

  (** Indexes into the lines of the text within the container. Raises
      `Invalid_argument` if the index is out of bounds. *)
  let nth_line (container : t) (index : int) : string =
    (lines container).(index - first_line_of_file)

  (** Gets the byte at the given line and offset. Note that this is *not* the
      index of the line, but its line number, which starts at 1. *)
  let get (container : t) (line : int) (offset : int) : char =
    let text = nth_line container line in
    if String.length text = offset
    then '\n'
    else text.[offset]

  (** Returns the point *on* the last line, *after* the last column. *)
  let end_point (self : t) : Point.t =
    let lines = lines self in
    let last_line = lines.(Array.length lines - 1) in
    let column =
      String.fold_left
        (fun n c -> n + if is_utf8_head c then 1 else 0)
        0 last_line
      + first_column_of_line
    in
    {line = Array.length lines - 1 + first_line_of_file;
     column;
     offset = String.length last_line}

  let equal (l : t) (r : t) : bool =
    l = r
end

(** A location is an open interval of characters within a source file. *)
module Location = struct
  type t = {container : Container.t; start : Point.t; end' : Point.t}

  (** Creates a zero-width location around the given point. *)
  let of_point (container : Container.t) (point : Point.t) : t =
    {container; start = point; end' = point}

  (** An empty location at the beginning of an empty file. *)
  let dummy = of_point Container.dummy Point.zero

  let pp_print (f : Format.formatter) ({container; start; end'} : t) : unit =
    let name = Container.name container in
    if Point.equal start end'
    then Format.fprintf f "%s:%d:%d" name start.line start.column
    else
      Format.fprintf
        f "%s:%d:%d-%d:%d" name start.line start.column end'.line end'.column

  let print : t -> unit =
    Format.printf "%a" pp_print

  let to_string : t -> string =
    Format.asprintf "%a" pp_print

  let equal (l : t) (r : t) =
    Container.equal l.container r.container
    && Point.equal l.start r.start
    && Point.equal l.end' r.end'

  let same (l : t) (r : t) =
    Container.equal l.container r.container
    && Point.same l.start r.start
    && Point.same l.end' r.end'

  (** Locations form a partial order where one is smaller than another iff it is
      entirely contained in the latter. *)
  let (<=) (l : t) (r : t) : bool =
    if not (Container.equal l.container r.container)
    then false
    else Point.(<=) r.start l.start && Point.(<=) l.end' r.end'

  (** Returns the least upper bound of two locations, i.e. the smallest location
      that covers both. If the locations do not belong to the same file, simply
      returns the first one. *)
  let extend (l : t) (r : t) : t =
    if not (Container.equal l.container r.container)
    then l
    else if l == dummy
    then r
    else if r == dummy
    then l
    else
      let start = Point.min l.start r.start in
      let end' = Point.max l.end' r.end' in
      {container = l.container; start; end'}
end

type Format.stag += Located of Location.t

let pp_enable_print_locations (f : Format.formatter) : unit =
  let fns = Format.pp_get_formatter_stag_functions f () in
  let fns' =
    {
      fns with
      print_open_stag =
        begin function
          | Located (location)
            -> Format.fprintf f "@[<hov 0>%a@ " Location.pp_print location
          | stag -> fns.print_open_stag stag
        end;
      print_close_stag =
        begin function
          | Located _
            -> Format.pp_close_box f ()
          | stag -> fns.print_close_stag stag
        end;
    }
  in
  Format.pp_set_formatter_stag_functions f fns';
  Format.pp_set_tags f true

(** A source object is text paired with a cursor. The text can be lazily loaded
    as it is accessed byte by byte, but it must be retained for future reference
    by error messages. *)
class t (container : Container.t) (base_point : Point.t) (end_point : Point.t) =
  let line = base_point.line in
  let column = base_point.column in
  let offset = base_point.offset in
  let end_line = end_point.line in
  let end_offset = end_point.offset in
  let read_cursor line offset =
    if line >= end_line && offset >= end_offset
    then None
    else Some (Container.get container line offset)
  in
  object (self)
    val mutable cursor = read_cursor line offset
    val mutable line = line
    val mutable column = column
    val mutable offset = offset

    method container : Container.t = container

    (** Returns the byte at the cursor, or None if the cursor is at the end of
        the text. *)
    method peek : char option =
      cursor

    (** The current point of the cursor. *)
    method point : Point.t =
      {line; column; offset}

    (** Makes a location starting at the given point and ending at the current
      cursor position. *)
    method make_location (start : Point.t) : Location.t =
      {
        container;
        start;
        end' = {offset; line; column};
      }

    (** Slices the text, from (and including) a starting point and to (and
        excluding) the curent cursor offset. The range must be inside a single
        line. *)
    method slice (point : Point.t) : string * Location.t =
      assert (point.line = line);
      let text = Container.nth_line container line in
      String.sub text point.offset (offset - point.offset),
      self#make_location point

    (** Moves the cursor forward one byte. *)
    method advance : unit =
      match cursor with
      | None -> ()
      | Some c
        -> if c = '\n'
           then begin
             line <- line + 1;
             column <- first_column_of_line;
             offset <- 0;
           end
           else if is_utf8_head c
           then begin
             column <- column + 1;
             offset <- offset + 1
           end
           else
             offset <- offset + 1;
           cursor <- read_cursor line offset

    (** Returns the char at the cursor, then advances it forward. *)
    method next : char option =
      let c = self#peek in
      self#advance;
      c
  end

(** Creates a source object from the contents of the file at the given path.
    The file is eagerly read to the end. `Sys_error` is raised if the file
    cannot be openned or read.  *)
let of_path (path : string) =
  let channel = open_in path in
  let unfold_lines () =
    try Some (input_line channel, ()) with
    | End_of_file -> None
  in
  let lines = Array.of_seq (Seq.unfold unfold_lines ()) in
  close_in channel;

  let lines = if Array.length lines = 0 then [|""|] else lines in
  let container = Container.File (path, lines) in
  new t container Point.zero (Container.end_point container)

let of_string ?(label : string = "<unknown>") (contents : string) =
  let container = Container.of_string ~label contents in
  new t container Point.zero (Container.end_point container)

(** Creates a source object from a location. In practice, this means that the
    new source will be a substring of the original source of the location. *)
let of_location (location : Location.t) : #t =
  new t location.container location.start location.end'
