(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Simple interpreter
 *
 * --------------------------------------------------------------------------- *)

open Util

open Sexp
(* open Pexp *)       (* Arg_kind *)
open Lexp       (* Varbind *)

open Elexp
open Builtin
open Grammar
open Debruijn
open Env
open Printf           (* IO Monad *)
module OL = Opslexp

type eval_debug_info = elexp list * elexp list

let dloc = dummy_location
let global_eval_trace = ref ([], [])
let global_eval_ctx = ref make_runtime_ctx
(* let eval_max_recursion_depth = ref 23000 *)

type builtin_function =
  location -> eval_debug_info -> value_type list -> value_type

(** A map of the function name to its implementation and arity. *)
let builtin_functions : (builtin_function * int) SMap.t ref = ref SMap.empty

let add_builtin_function name (f : builtin_function) arity =
  builtin_functions := SMap.add name (f, arity) (!builtin_functions)

let append_eval_trace trace (expr : elexp) =
  let (a, b) = trace in
  let r = expr::a, b in
    global_eval_trace := r; r

let append_typer_trace trace (expr : elexp) =
  let (a, b) = trace in
  let r = (a, expr::b) in
    global_eval_trace := r; r

let get_trace () = !global_eval_trace

let rec_depth trace =
  let (_a, b) = trace in
    List.length b

let fatal loc ?print_action fmt =
  Log.log_fatal ~section:"EVAL" ~loc ?print_action fmt

(* eval error are always fatal *)
let error loc ?print_action fmt =
  Log.log_msg
    (fun s -> raise (Log.Internal_error s))
    Log.Error
    ?kind:None
    ~section:"EVAL"
    ?print_action
    ~loc
    fmt

let warning loc ?print_action fmt =
  Log.log_warning ~section:"EVAL" ~loc ?print_action fmt

(*  Print a message that look like this:
 *
 * [Ln   8, cl   6] ./samples/error.typer
 *   [X] Fatal     EVAL     MESSAGE
 *       > ....
 *       > ....
 *
 *)

let root_string () =
  let a, _ = !global_eval_trace in
  match List.rev a with
  | [] -> ""
  | e :: _ -> Elexp.to_string e

let trace_value (value : value_type) : string =
  sprintf
    "\t> %s : %s\n\t> Root: %s\n"
    (value_name value)
    (value_string value)
    (root_string ())

let trace_elexp (elexp : elexp) : string =
  Format.asprintf
    "\t>%a\n\t> Root: %s\n"
    Elexp.pp_print
    elexp
    (root_string ())

(* FIXME: We're not using predef here.  This will break if we change
 * the definition of `Bool` in builtins.typer.  *)
let ttrue = Vcons ((dloc, "true"), [])
let tfalse = Vcons ((dloc, "false"), [])
let o2v_bool b = if b then ttrue else tfalse

let tunit = Vcons ((dloc, "unit"), [])

(* I'm using a ref because I think register_builtin_functions
   get called after the default_ctx is created in elab.
   Another solution could be to define a function in elaboration step
   and then undefine it. The thing is elab_context may not exist at runtime
   so I must save it somewhere if the Elab.* function persist. *)
let macro_monad_elab_context = ref empty_elab_context

let set_getenv (ectx : elab_context) = macro_monad_elab_context := ectx

(*
 *                  Builtins
 *)
(* Builtin of builtin * string * ltype *)
let add_binary_iop name f =
  let name = "Int." ^ name in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint (v); Vint (w)] -> Vint (f v w)
    | _ -> error loc {|"%s" expects 2 Int arguments|} name in
  add_builtin_function name f 2

let add_binary_iop_with_loc name f =
  let name = "Int." ^ name in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint (v); Vint (w)] -> Vint (f loc v w)
    | _ -> error loc {|"%s" expects 2 Int arguments|} name in
  add_builtin_function name f 2

let add_with_overflow loc a b =
  let c = a + b in
  (* Check that signs of both args are diff. OR sign of result is the
     same as the sign of the args. *)
  if (a lxor b) lor (a lxor (lnot c)) < 0
  then c
  else error loc "Overflow in `Int.+`"

let sub_with_overflow loc a b =
  let c = a - b in
  (* Check that signs of both args are the same OR sign of result is
     the same as the sign of the first arg. *)
  if (a lxor (lnot b)) lor (a lxor (lnot c)) < 0
  then c
  else error loc "Overflow in `Int.-`"

let mul_with_overflow loc a b =
  let c = a * b in
  if b = 0 || not (c = min_int && b = -1) && a = c / b (* Simple but slow *)
  then c
  else error loc "Overflow in `Int.*`"

let div_with_overflow loc a b =
  if not (a = min_int && b = -1)
  then a / b
  else error loc "Overflow in `Int./`"

let lsl_with_shift_check loc a b =
  if b < 0 || b > Sys.int_size
  then error loc {|Invalid shift value %d in "Int.lsl"|} b
  else a lsl b

let lsr_with_shift_check loc a b =
  if b < 0 || b > Sys.int_size
  then error loc {|Invalid shift value %d in "Int.lsr"|} b
  else a lsr b

let asr_with_shift_check loc a b =
  if b < 0 || b > Sys.int_size
  then error loc {|Invalid shift value %d in "Int.ast"|} b
  else a asr b

let _ = add_binary_iop_with_loc "+" add_with_overflow;
        add_binary_iop_with_loc "-" sub_with_overflow;
        add_binary_iop_with_loc "*" mul_with_overflow;
        add_binary_iop_with_loc "/" div_with_overflow;

        add_binary_iop_with_loc "lsl" lsl_with_shift_check;
        add_binary_iop_with_loc "lsr" lsr_with_shift_check;
        add_binary_iop_with_loc "asr" asr_with_shift_check;

        add_binary_iop "mod"  (mod);
        add_binary_iop "and"  (land);
        add_binary_iop "or"   (lor);
        add_binary_iop "xor"  (lxor)

let add_binary_bool_iop name f =
  let name = "Int." ^ name in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint v; Vint w] -> o2v_bool (f v w)
    | _ -> error loc {|"%s" expects 2 Int arguments|} name in
  add_builtin_function name f 2

let _ = add_binary_bool_iop "<"  (<);
        add_binary_bool_iop ">"  (>);
        add_binary_bool_iop "="  (=);
        add_binary_bool_iop ">="  (>=);
        add_binary_bool_iop "<="  (<=)

let _ =
  let name = "Int." ^ "not" in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint v] -> Vint(lnot v)
    | _ -> error loc {|"%s" expects 1 Int arguments|} name in
  add_builtin_function name f 1

(* True integers (aka Z).  *)

let add_binary_biop name f =
  let name = "Integer." ^ name in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vinteger v; Vinteger w] -> Vinteger (f v w)
    | _ -> error loc {|"%s" expects 2 Integer arguments|} name in
  add_builtin_function name f 2

let _ = add_binary_biop "+"  Z.add;
        add_binary_biop "-"  Z.sub;
        add_binary_biop "*"  Z.mul;
        add_binary_biop "/"  Z.div

let add_binary_bool_biop name f =
  let name = "Integer." ^ name in
  let f loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vinteger v; Vinteger w] -> o2v_bool (f v w)
    | _ -> error loc {|"%s" expects 2 Integer arguments|} name in
  add_builtin_function name f 2

let _ = add_binary_bool_biop "<"  Z.lt;
        add_binary_bool_biop ">"  Z.gt;
        add_binary_bool_biop "="  Z.equal;
        add_binary_bool_biop ">=" Z.geq;
        add_binary_bool_biop "<=" Z.leq;
        let name = "Int->Integer" in
        add_builtin_function
          name
          (fun loc (_depth : eval_debug_info) (args_val: value_type list)
           -> match args_val with
             | [Vint v] -> Vinteger (Z.of_int v)
             | _ -> error loc {|"%s" expects 1 Int argument|} name)
          1;
        let name = "Integer->Int" in
        add_builtin_function
          name
          (fun loc (_depth : eval_debug_info) (args_val: value_type list)
           -> match args_val with
              | [Vinteger v]
                -> (try Vint (Z.to_int v) with
                    | Z.Overflow -> error loc {|Overflow in "%s"|} name)
              | _ -> error loc {|"%s" expects 1 Integer argument|} name)
          1;
        let name = "Int->Float" in
        add_builtin_function
          name
          (fun loc (_depth : eval_debug_info) (args_val: value_type list)
            -> match args_val with
              | [Vint v] -> Vfloat (Float.of_int v)
              | _ -> error loc {|"%s" expects 1 Int argument|} name)
          1

(* Floating point numers.  *)

let add_binary_fop name f =
  let name = "Float." ^ name in
  let f loc (_depth : eval_debug_info) = function
    | [Vfloat v; Vfloat w] -> Vfloat (f v w)
    | _ -> error loc {|"%s" expects 2 Float arguments|} name in
  add_builtin_function name f 2

let _ = add_binary_fop "+"  (+.);
        add_binary_fop "-"  (-.);
        add_binary_fop "*" ( *. );
        add_binary_fop "/"  (/.)

let add_binary_bool_fop name f =
  let name = "Float." ^ name in
  let f loc (_depth : eval_debug_info) = function
    | [Vfloat v; Vfloat w] -> o2v_bool (f v w)
    | _ -> error loc {|"%s" expects 2 Float arguments|} name in
  add_builtin_function name f 2

let _ = add_binary_bool_fop "<"  (<);
        add_binary_bool_fop ">"  (>);
        add_binary_bool_fop "="  (=);
        add_binary_bool_fop ">="  (>=);
        add_binary_bool_fop "<="  (<=)

let _ = let name = "Float." ^ "trunc" in
  let f loc (_depth : eval_debug_info) = function
    | [Vfloat v] -> Vfloat ( snd (modf v) )
    | _ -> error loc {|"%s" expects 1 Float argument|} name in
  add_builtin_function name f 1

let make_symbol loc _depth args_val  = match args_val with
  | [Vstring str] -> Vsexp (Symbol (loc, str))
  | _ -> error loc "Sexp.symbol expects one string as argument"

let make_node loc _depth args_val = match args_val with
  | [Vsexp (op); lst]
    -> let args = v2o_list lst in
       let unwrap_sexp = function
         | Vsexp (sexp) -> sexp
         | v
           -> error
                loc
                {|Sexp.node expects "List Sexp" second as arguments\n%s|}
                (trace_value v)
       in
       Vsexp (Node (loc, op, List.map unwrap_sexp args))

  | _  -> error loc "Sexp.node expects a `Sexp` and a `List Sexp`"


let make_string loc _depth args_val  = match args_val with
  | [Vstring str] -> Vsexp (String (loc, str))
  | _ -> error loc "Sexp.string expects one string as argument"

let make_integer loc _depth args_val = match args_val with
  | [Vinteger n] -> Vsexp (Integer (loc, n))
  | _ -> error loc "Sexp.integer expects one integer as argument"

let make_float loc _depth args_val   = match args_val with
  | [Vfloat x] -> Vsexp (Float (loc, x))
  | _ -> error loc "Sexp.float expects one float as argument"

let make_block loc _depth args_val   = match args_val with
  (* From what would we like to make a block? *)
  | [Vstring str]
    -> let source = Source.of_string str in
       Vsexp (Block (loc, (Prelexer.prelex source)))
  | _ -> error loc "Sexp.block expects one string as argument"

let reader_parse loc _depth = function
  | [Velabctx ectx; Vsexp (Block (_, toks))]
    -> let grm = ectx_get_grammar ectx in
       let tokens = Lexer.lex default_stt toks in
       o2v_list (sexp_parse_all_to_list grm tokens (Some ";"))
  | [Velabctx _; s] -> (warning loc "Reader.parse do nothing without a Block"; s)
  | _   -> error loc "Reader.parse expects an ElabContext and a Block as argument"

let string_eq loc _depth args_val = match args_val with
  | [Vstring s1; Vstring s2] -> o2v_bool (s1 = s2)
  | _ -> error loc "String.= expects 2 strings"

(* need either a character type or builtin string operation *)
let string_concat loc _depth args_val = match args_val with
  | [Vstring s1; Vstring s2] -> Vstring (s1 ^ s2)
  | _ -> error loc "String.concat expects 2 strings"

let string_sub loc _depth args_val = match args_val with
  | [Vstring s; Vint start; Vint len] -> Vstring (String.sub s start len)
  | _ -> error loc "String.sub expects 1 string and 2 ints"

let sexp_eq loc _depth args_val = match args_val with
  | [Vsexp (s1); Vsexp (s2)] -> o2v_bool (sexp_equal s1 s2)
  | _ -> error loc "Sexp.= expects 2 sexps"

let sexp_debug_print loc _depth = function
  | [Vsexp (s1)]
    -> let tstr = sexp_name s1 in
       Format.printf "\n\t%s : [%a]\n\n" tstr Sexp.pp_print s1;
       Vcommand (fun () -> Vsexp (s1))
  | _ -> error loc "Sexp.debug_print expects 1 sexps"

let file_open loc _depth args_val = match args_val with
  | [Vstring (file); Vstring (mode)] ->
     (* open file *) (* return a file handle *)
     Vcommand(fun () ->
         match mode with
         | "r" -> Vin (open_in file)
         | "w" -> Vout (open_out file)
         | _ -> error loc "wrong open mode")

  | _ -> error loc "File.open expects 2 strings"

let file_read loc _depth args_val = match args_val with
  (* FIXME: Either rename it to "readline" and drop the second arg,
   * or actually pay attention to the second arg.  *)
  | [Vin channel; Vint _n] -> Vstring (input_line channel)
  | _ -> error loc ~print_action:(fun _ ->
             List.iter (fun v -> value_print v None; print_newline ()) args_val;
           )
           "File.read expects an in_channel. Actual arguments:"

let file_write loc _depth args_val = match args_val with
  | [Vout channel; Vstring msg]
    -> Vcommand (fun () -> fprintf channel "%s" msg;
                       (* FIXME: This should be the unit value!  *)
                       Vundefined)
  | _ -> error loc ~print_action:(fun _ ->
             List.iter (fun v -> value_print v None; print_newline ()) args_val;
           )
           "File.write expects an out_channel and a string. Actual arguments:"

let rec eval lxp (ctx : Env.runtime_env) (trace : eval_debug_info): (value_type) =

    let trace = append_eval_trace trace lxp in
    let eval' lxp ctx = eval lxp ctx trace in

    (* This creates an O(N^2) cost for deep recursion because rec_depth
     * uses `length` on the stack trace.  *)
    (* (if (rec_depth trace) > (!eval_max_recursion_depth) then
     *     fatal (elexp_location lxp) "Recursion Depth exceeded"); *)

    (* Save current trace in a global variable. If an error occur,
       we will be able to retrieve the most recent trace and context *)
    global_eval_ctx := ctx;
    global_eval_trace := trace;

    match lxp with
        (*  Leafs           *)
        (* ---------------- *)
        | Imm(Integer (_, i))       -> Vinteger i
        | Imm(String (_, s))        -> Vstring s
        | Imm(Float (_, n))         -> Vfloat n
        | Imm(sxp)                  -> Vsexp sxp
        | Cons (_, label)           -> Vcons (label, [])
        | Lambda (n, lxp)           -> Closure (n, lxp, ctx)
        | Builtin ((_, str))        -> Vbuiltin str

        (* Return a value stored in env *)
        | Var((loc, name), idx) as e
          -> eval_var ctx e ((loc, name), idx)

        (*  Nodes           *)
        (* ---------------- *)
        | Let(_, decls, inst)
          -> let nctx = eval_decls decls ctx trace in
            eval' inst nctx

        (* Function call *)
        | Call (f, args)
          -> eval_call (Elexp.location f) f trace
                      (eval f ctx trace)
                      (List.map (fun e -> eval e ctx trace) args)
        (* Proj *)
        | Proj (loc, e, i)
          -> (match eval' e ctx with
             | Vcons (_, vtl) -> List.nth vtl i
             | _ -> error loc "Proj on a non-datatype constructor: %s \n"
                     (Elexp.to_string e))
        (* Case *)
        | Case (loc, target, pat, dflt)
          -> (eval_case ctx trace loc target pat dflt)

        (* FIXME: Here, we'd want to apply `ctx` to `e`, but we can't
         * apply a runtime-ctx to a lexp!
         * E.g. say we have `λt ≡> Ind (arg : t)`: by the time we get here,
         * Our `Type` carries `Ind (arg : t)` but `t` is not in `ctx`!
         * So we should either use an elexp instead of a lexp,
         * or somehow recover a lexp-ctx from the runtime-ctx (maybe
         * with extra info that `Type` could carry),
         * or find another trick.
         *
         * We could replace erasable arguments (like t above) with a Metavar,
         * but the other issue of converting Values back to Lexps remains!
         *
         * Hence, the `e` carried by `Vtype` is just indicative and not
         * something we can use.  *)
        | Type e -> Vtype e


and eval_var ctx lxp v =
  let ((sinfo, name as vname), idx) = v in
  try get_rte_variable vname idx ctx with
  | _e
    -> Log.log_fatal
         ~loc:(Sexp.location sinfo)
         "Variable: %s[%d] was not found\n%s"
         (Lexp.maybename name) idx (trace_elexp lxp)

(* unef: unevaluated function (to make the trace readable) *)
and eval_call loc unef i f args =
  match f, args with
  (* return result of eval *)
  | _, [] -> f

  | Vcons (n, fields), _
    -> Vcons (n, List.append fields args)

  | Closure (x, e, ctx), v::vs
    -> let rec bindargs e vs ctx = match (vs, e) with
       | (v::vs, Lambda (x, e))
         (* "Uncurry" on the fly.  *)
         -> bindargs e vs (add_rte_variable x v ctx)
       | ([], _) ->
        let trace = append_typer_trace i unef in
        eval e ctx trace
       | _ -> eval_call loc unef i (eval e ctx i) vs in
     bindargs e vs (add_rte_variable x v ctx)

  | Vbuiltin (name), args
    -> (try let (builtin, arity) = SMap.find name !builtin_functions in
           let nargs = List.length args in
           if nargs = arity then
             builtin loc i args        (* Fast common case.  *)
           else if nargs > arity then
             let rec split n vs acc = match (n, vs) with
               | 0, _ -> let v = eval_call loc unef i f (List.rev acc) in
                        eval_call loc unef i v vs
               | _, (v::vs) -> split (n - 1) vs (v::acc)
               | _ -> error loc "Impossible!"
             in split nargs args []
           else
             let rec buildctx args ctx = match args with
               | [] -> ctx
               | arg::args -> buildctx args (add_rte_variable vdummy arg ctx) in
             let rec buildargs n =
               if n >= 0
               then (Var (vdummy, n))::buildargs (n - 1)
               else [] in
             Closure (vdummy,
                      Call (Builtin (dloc, name), buildargs (List.length args)),
                      buildctx args Myers.nil)

        with
        | Not_found
          -> error loc {|Requested Built-in "%s" does not exist|} name
        | e
          -> warning loc {|Exception thrown from primitive "%s"|} name;
             raise e)

  | Vtype e, _
   (* We may call a Vlexp e.g. for "x = Map Int String".
    * FIXME: The arg will sometimes be a Vlexp but not always, so this is
    * really just broken!  *)
    -> Vtype (Lexp.mkCall (dsinfo, e, [(Pexp.Anormal, mkVar (vdummy, -1))]))
  | _ -> fatal loc "Trying to call a non-function!\n%s" (trace_value f)

and eval_case ctx i loc target pat dflt =
    (* Eval target *)
    let v = eval target ctx i in

    (* extract constructor name and arguments *)
    let ctor_name, args = match v with
      | Vcons((_, cname), args)  -> cname, args
      | _
        -> error
             loc {|Target "%s" is not a Constructor\n%s|}
             (Elexp.to_string target) (trace_value v)
    in

    (*  Get working pattern *)
    try let (_, pat_args, exp) = SMap.find ctor_name pat in
        (* build context (List.fold2 has problem with some cases)  *)
        (* This is more robust                                     *)
        let rec fold2 nctx pats args =
            match pats, args with
            | pat::pats, arg::args
              -> let nctx = add_rte_variable pat arg nctx in
                fold2 nctx pats args
            (* Errors: those should not happen but they might  *)
            (* List.fold2 would complain. we print more info   *)
            | _::_, [] -> warning loc "a) Eval::Case Pattern Error"; nctx
            | [], _::_ -> warning loc "b) Eval::Case Pattern Error"; nctx
            (* Normal case *)
            | [], [] -> nctx in

        let nctx = fold2 ctx pat_args args in
            eval exp nctx i

    (* Run default *)
    with Not_found -> (match dflt with
        | Some (var, lxp)
          -> eval lxp (add_rte_variable var v ctx) i
        | _ -> error loc "Match Failure")

and build_arg_list args ctx i =
    (*  eval every args *)
    let arg_val = List.map (fun (_k, e) -> eval e ctx i) args in

    (*  Add args inside context *)
    List.fold_left (fun c v -> add_rte_variable vdummy v c) ctx arg_val

and eval_decls (decls: (vname * elexp) list)
               (ctx: runtime_env) i: runtime_env =

    let n = (List.length decls) - 1 in

    (* Read declarations once and push them *)
    let nctx = List.fold_left (fun ctx (name, _) ->
      add_rte_variable name Vundefined ctx) ctx decls in

    List.iteri (fun idx (name, lxp) ->
      let v = eval lxp nctx i in
      let offset = n - idx in
        ignore (set_rte_variable offset name v nctx)) decls;

        nctx

(* -------------------------------------------------------------------------- *)
(*              Builtin Implementation  (Some require eval)                   *)

(* Sexp -> (Sexp -> List Sexp -> Sexp) -> (String -> Sexp) ->
    (String -> Sexp) -> (Int -> Sexp) -> (Float -> Sexp) -> (List Sexp -> Sexp)
        ->  Sexp *)
and sexp_dispatch loc depth args =
    let trace_dum = (Var ((epsilon (loc), None), -1)) in
    let eval_call a b = eval_call loc trace_dum depth a b in
    let sxp, nd, sym, str, it, flt, blk = match args with
        | [sxp; nd; sym; str; it; flt; blk] ->
           sxp, nd, sym, str, it, flt, blk
        | _ ->  error loc "sexp_dispatch expects 7 arguments" in

    let sxp = match sxp with
        | Vsexp(sxp)   -> sxp
        | _
          -> fatal
               loc
               "sexp_dispatch expects a Sexp as 1st arg\n%s" (trace_value sxp)
    in

    match sxp with
    | Node    (_, op, s) -> eval_call nd [Vsexp op; o2v_list s]
    | Symbol  (_ , s)    -> eval_call sym [Vstring s]
    | String  (_ , s)    -> eval_call str [Vstring s]
    | Integer (_ , i)    -> eval_call it [Vinteger i]
    | Float   (_ , f)    -> eval_call flt [Vfloat f]
    | Block   (_ , _) as b ->
       (* I think this code breaks what Blocks are.  We delay parsing but parse
          with default_stt and default_grammar... *)
       (* let toks = Lexer.lex default_stt s in
          let s     = sexp_parse_all_to_list default_grammar toks (Some ";") in*)
       eval_call blk [Vsexp b]

(* -------------------------------------------------------------------------- *)
and print_eval_result i lxp ltype =
  Printf.printf "     Out[%02d] >> " i;
  value_print lxp ltype;
  print_string "\n";

and print_typer_trace' trace =

  let trace = List.rev trace in

  print_string (Fmt.make_title "Typer Trace");
  print_string (Fmt.make_sep '-');

  let _ = List.iteri (fun i expr ->
    print_string "    ";
    Fmt.print_ct_tree i; print_string "+- ";
    print_string ((Elexp.to_string expr) ^ "\n")) trace in

  print_string (Fmt.make_sep '=')

and print_typer_trace trace =
  match trace with
    | Some t -> print_typer_trace' t
    | None -> let (_a, b) = !global_eval_trace in
      print_typer_trace' b

and print_trace title trace default =
  (* If no trace is provided take the most revent one *)
  let trace = match trace with
    | Some trace -> trace
    | None -> default in

  (* Trace is upside down by default *)
  let trace = List.rev trace in

  (* Now eval trace and elab trace are the same *)
  let print_trace = (fun type_string type_loc i expr ->
    (* Print location info *)
    print_string ("    [" ^ (Source.Location.to_string (type_loc expr)) ^ "] ");

    (* Print call trace visualization *)
    Fmt.print_ct_tree i; print_string "+- ";

    (* Print element *)
    print_string ((type_string expr) ^ "\n")
  ) in

  let elexp_trace = print_trace Elexp.to_string Elexp.location in

  (* Print the trace*)
  print_string (Fmt.make_title title);
  print_string (Fmt.make_sep '-');
  let _ = List.iteri elexp_trace trace in
  print_string (Fmt.make_sep '=')

and print_eval_trace trace =
    let (a, _b) = !global_eval_trace in
      print_trace " EVAL TRACE " trace a

let io_bind loc depth args_val =
  let trace_dum = (Var ((epsilon (loc), None), -1)) in

  match args_val with
  | [Vcommand cmd; callback]
    -> (* bind returns another Vcommand *)
     Vcommand (fun ()
               -> match eval_call loc trace_dum depth callback [cmd ()] with
                 | Vcommand cmd -> cmd ()
                 | _ -> error loc "IO.bind second arg did not return a command")
  | _ -> error loc "Wrong number of args or wrong first arg value in `IO.bind`"

let io_run loc _depth args_val = match args_val with
    | [Vcommand cmd; v] -> let _ = cmd () in v
    | _ -> error loc "IO.run expects a monad and a function as arguments"

let io_return loc _depth args_val = match args_val with
  | [v] -> Vcommand (fun () -> v)
  | _ -> error loc "IO.return takes a single argument"

let float_to_string loc _depth args_val = match args_val with
  | [Vfloat x] -> Vstring (string_of_float x)
  | _ -> error loc "Float->String expects one Float argument"

let int_to_string loc _depth args_val = match args_val with
  | [Vint x] -> Vstring (string_of_int x)
  | _ -> error loc "Int->String expects one Int argument"

let integer_to_string loc _depth args_val = match args_val with
  | [Vinteger x] -> Vstring (Z.to_string x)
  | _ -> error loc "Integer->String expects one Integer argument"

let sys_exit loc _depth args_val = match args_val with
  | [Vint n] -> Vcommand (fun _ -> exit n)
  | _ -> error loc "Sys.exit takes a single Int argument"

let y_operator loc _depth args =
  match args with
  | [f] -> let yf_ref = ref Vundefined in
          let fname = (dsinfo, Some "f") in
          let yfname = (dsinfo, Some "yf") in
          let yf = Closure(vdummy,
                           Call (Var (fname, 1),
                                 [Var (yfname, 2);
                                  Var (vdummy, 0)]),
                           Myers.cons (fname, ref f)
                                      (Myers.cons (yfname, yf_ref)
                                                  Myers.nil)) in
          yf_ref := yf;
          yf
  | _ -> error loc ("Y expects 1 (function) argument")

let arity0_fun loc _ _ = error loc "Called a 0-arity function!?"

let heq_uneq loc _ vs = match vs with
  | [x; _y] -> x
  | _ -> error loc "Heq_uneq takes 2 arguments"

let nop_fun loc _ vs = match vs with
  | [v] -> v
  | _ -> error loc "Wrong number of argument to nop"

let ref_make loc _depth args_val = match args_val with
  | [v] -> Vcommand (fun () -> Vref (ref v))
  | _ -> error loc "Ref.make takes a single value as argument"

let ref_read loc _depth args_val = match args_val with
  | [Vref (v)] -> Vcommand (fun () -> !v)
  | _ -> error loc "Ref.read takes a single Ref as argument"

let ref_write loc _depth args_val = match args_val with
  | [value; Vref (actual)] ->
    Vcommand (fun () -> actual := value; tunit)
  | _ -> error loc "Ref.write takes a value and a Ref as argument"

let gensym = let count = ref 0 in
  (fun loc _depth args_val -> match args_val with
    | [_v] -> Vcommand (fun () -> (
      count := ((!count) + 1);
      Vsexp (Symbol (dloc,(" %gensym% no "^(string_of_int (!count))^" ")))))
    | _ -> error loc "gensym takes a Unit as argument")

let getenv loc _depth args_val = match args_val with
  | [_v] -> Vcommand (fun () -> Velabctx !macro_monad_elab_context)
  | _ -> error loc "getenv takes a single Unit as argument"

let is_bound loc _depth args_val = match args_val with
  | [Vstring name; Velabctx ectx]
    -> o2v_bool (try (ignore (senv_lookup name ectx); true)
                with Senv_Lookup_Fail _ -> false)
  | _ -> error loc "Elab.isbound takes an Elab_Context and a String as arguments"

let constructor_p name ectx =
  try let idx = senv_lookup name ectx in
      (* Use `lexp_whnf` so that `name` can be indirectly
       * defined as a constructor
       * (e.g. as in `let foo = cons in case foo x xs | ...`  *)
      match OL.lexp'_whnf (mkVar ((dsinfo, Some name), idx)) (ectx_to_lctx ectx) with
        | Cons _ -> true           (* It's indeed a constructor!  *)
        | _ -> false
  with Senv_Lookup_Fail _ -> false

let erasable_p name nth ectx =
  let is_erasable ctors = match (SMap.find_opt name ctors) with
                            | (Some args) ->
                              if (nth < (List.length args) && nth >= 0) then
                                ( match (List.nth args nth) with
                                    | (k, _, _) -> k = Pexp.Aerasable )
                              else false
                            | _ -> false in
  try let idx = senv_lookup name ectx in
    match OL.lexp'_whnf (mkVar ((dsinfo, Some name), idx)) (ectx_to_lctx ectx) with
      | Cons (e, _) when is_var e
        -> (match (env_lookup_expr ectx (get_var e)) with
            | Some i when is_inductive i
              -> is_erasable (get_inductive_ctor (get_inductive i))
            | _ -> false)
      | _ -> false
  with Senv_Lookup_Fail _ -> false

let erasable_p2 t name ectx =
  let is_erasable ctors =
    match (SMap.find_opt t ctors) with
    | Some args
      -> (List.exists
          (fun (k, oname, _)
           -> match oname with
             | (_, Some n) -> (n = name && k = Pexp.Aerasable)
             | _ -> false)
          args)
    | _ -> false in
  try let idx = senv_lookup t ectx in
    match OL.lexp'_whnf (mkVar ((dsinfo, Some t), idx)) (ectx_to_lctx ectx) with
      | Cons (e, _) when is_var e
        -> (match (env_lookup_expr ectx (get_var e)) with
            | Some i when is_inductive i
              -> is_erasable (get_inductive_ctor (get_inductive i))
            | _ -> false)
      | _ -> false
  with Senv_Lookup_Fail _ -> false

let nth_ctor_arg name nth ectx =
  let find_nth ctors = match (SMap.find_opt name ctors) with
    | Some args ->
       (match (List.nth args nth) with
        | (_, (_, Some n), _) -> n
        | _ -> "_"
        | exception (Failure _) -> "_" )
    | _ -> "_" in
  try let idx = senv_lookup name ectx in
  match OL.lexp'_whnf (mkVar ((dsinfo, Some name), idx)) (ectx_to_lctx ectx) with
      | Cons (e, _) when is_var e
        -> (match (env_lookup_expr ectx (get_var e)) with
            | Some i when is_inductive i
              -> find_nth (get_inductive_ctor (get_inductive i))
            | _ -> "_")
      | _ -> "_"
  with Senv_Lookup_Fail _ -> "_"

let ctor_arg_pos name arg ectx =
  let rec find_opt xs n = match xs with
    | [] -> None
    | (_, (_, Some x), _)::xs -> if x = arg then Some n else find_opt xs (n + 1)
    | _::xs -> find_opt xs (n + 1) in
  let find_arg ctors = match (SMap.find_opt name ctors) with
                            | (Some args) ->
                              ( match (find_opt args 0) with
                                  | None -> (-1)
                                  | Some n -> n )
                            | _ -> (-1) in
  try let idx = senv_lookup name ectx in
  match OL.lexp'_whnf (mkVar ((dsinfo, Some name), idx)) (ectx_to_lctx ectx) with
      | Cons (e, _) when is_var e
        -> (match (env_lookup_expr ectx (get_var e)) with
            | Some i when is_inductive i
              -> find_arg (get_inductive_ctor (get_inductive i))
            | _ -> (-1))
      | _ -> (-1)
  with Senv_Lookup_Fail _ -> (-1)

let is_constructor loc _depth args_val = match args_val with
  | [Vstring name; Velabctx ectx] -> o2v_bool (constructor_p name ectx)
  | _ -> error loc "Elab.isconstructor takes a String and an Elab_Context as arguments"

let is_nth_erasable loc _depth args_val = match args_val with
  | [Vstring name; Vint nth_arg; Velabctx ectx] -> o2v_bool (erasable_p name nth_arg ectx)
  | _ -> error loc "Elab.is-nth-erasable takes a String, an Int and an Elab_Context as arguments"

let is_arg_erasable loc _depth args_val = match args_val with
  | [Vstring t; Vstring arg; Velabctx ectx] -> o2v_bool (erasable_p2 t arg ectx)
  | _ -> error loc "Elab.is-arg-erasable takes two String and an Elab_Context as arguments"

let nth_arg loc _depth args_val = match args_val with
  | [Vstring t; Vint nth; Velabctx ectx] -> Vstring (nth_ctor_arg t nth ectx)
  | _ -> error loc "Elab.nth-arg takes a String, an Int and an Elab_Context as arguments"

let arg_pos loc _depth args_val = match args_val with
  | [Vstring t; Vstring a; Velabctx ectx] -> Vint (ctor_arg_pos t a ectx)
  | _ -> error loc "Elab.arg-pos takes two String and an Elab_Context as arguments"

let array_append loc _depth args_val = match args_val with
  | [v; Varray a] ->
    Varray (Array.append (Array.map (fun v -> v) a) (Array.make 1 v))
  | _ -> error loc "Array.append takes a value followed by an Array as arguments"

let array_create loc _depth args_val = match args_val with
  | [Vint len; v] -> Varray (Array.make len v)
  | _ -> error loc "Array.make takes an Int and a value and as arguemnts"

let array_length loc _depth args_val = match args_val with
  | [Varray a] -> Vint (Array.length a)
  | _ -> error loc "Array.length takes an Array as argument"

let array_set loc _depth args_val = match args_val with
  | [Vint idx; v; Varray a] -> if (idx > (Array.length a) || idx < 0)
    then
      (warning loc "Array.set index out of bounds (array unchanged)";
      (Varray a))
    else
      let copy = (Array.map (fun v -> v) a) in
      (Array.set copy idx v; Varray copy)
  | _ -> error loc "Array.set takes an Int, a value and an Array as arguments"

let array_get loc _depth args_val = match args_val with
  | [dflt; Vint idx; Varray a] -> if (idx >= (Array.length a)) || (idx < 0)
    then
      dflt
    else
      Array.get a idx
  | _ -> error loc "Array.get takes a default value, an Int and an Array as arguments"


(*  Vundefined is used in array_empty because we have no default value
    ("array_create 0 0" has a default value v (Vint 0)).
*)
let array_empty loc _depth args_val = match args_val with
  | [_] -> Varray (Array.make 0 Vundefined)
  | _ -> error loc "Array.empty takes a Unit as single argument"

let test_fatal loc _depth args_val = match args_val with
  | [Vstring section; Vstring msg] ->
     Vcommand (fun () ->
         Log.print_entry
           (Log.mkEntry Log.Fatal ~kind:"(Unit test fatal)" ~loc ~section msg);
         Log.user_error "%s" msg
       )
  | _ -> error loc "Test.fatal takes two String as argument"

let test_warning loc _depth args_val = match args_val with
  | [Vstring section; Vstring msg] ->
     Vcommand (fun () ->
         Log.print_entry
           (Log.mkEntry Log.Warning ~kind:"(Unit test warning)" ~loc ~section msg);
         tunit)
  | _ -> error loc "Test.warning takes two String as argument"

let test_info loc _depth args_val = match args_val with
  | [Vstring section; Vstring msg] ->
     Vcommand (fun () ->
         Log.print_entry
           (Log.mkEntry Log.Info ~kind:"(Unit test Info)" ~loc ~section msg);
         tunit)
  | _ -> error loc "Test.info takes two String as argument"

let test_location loc _depth args_val = match args_val with
  | [_] -> Vstring (Source.Location.to_string loc)
  | _ -> error loc "Test.location takes a Unit as argument"

let test_true loc _depth args_val = match args_val with
  | [Vstring name; Vcons ((_dloc, b), [])] ->
     if b = "true" then
       Vcommand (fun () -> print_string ("[  OK] "^name^"\n");
                           ttrue)
     else
       Vcommand (fun () -> print_string ("[FAIL] "^name^"\n");
                           tfalse)
  | _ -> error loc "Test.true takes a String and a Bool as argument"

let test_false loc _depth args_val = match args_val with
  | [Vstring name; Vcons ((_dloc, b), [])] ->
     if b = "false" then
       Vcommand (fun () -> print_string ("[  OK] "^name^"\n");
                           ttrue)
     else
       Vcommand (fun () -> print_string ("[FAIL] "^name^"\n");
                           tfalse)
  | _ -> error loc "Test.false takes a String and a Bool as argument"

let test_eq loc _depth args_val = match args_val with
  | [Vstring name; v0; v1] -> if Env.value_equal v0 v1 then
                 Vcommand (fun () -> print_string ("[  OK] "^name^"\n");
                                     ttrue)
               else
                 Vcommand (fun () -> print_string ("[FAIL] "^name^"\n");
                                     tfalse)
  | _ -> error loc "Test.eq takes a String and two values as argument"

let test_neq loc _depth args_val = match args_val with
  | [Vstring name; v0; v1] -> if Env.value_equal v0 v1 then
                 Vcommand (fun () -> print_string ("[FAIL] "^name^"\n");
                                     tfalse)
               else
                 Vcommand (fun () -> print_string ("[  OK] "^name^"\n");
                                     ttrue)
  | _ -> error loc "Test.neq takes a String and two values as argument"

let typelevel_succ loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint v] -> Vint(v + 1)
    | _ -> error loc ("`Typlevel.succ` expects 1 TypeLevel argument")
let typelevel_lub loc (_depth : eval_debug_info) (args_val: value_type list) =
    match args_val with
    | [Vint v1; Vint v2] -> Vint(max v1 v2)
    | _ -> error loc ("`Typlevel.⊔` expects 2 TypeLevel argument2")

(* Dummy "implementation" of a type constructor.  *)
let type_constructor loc _ _args =
  error loc "No implementation of the construction of this type"

let quotient_elim loc depth args =
    let trace_dum = (Var ((epsilon (loc), None), -1)) in
    match args with
      | [f; q] ->
          eval_call loc trace_dum depth f [q]
      | _ -> error loc "Quotient.elim expects 2 arguments"

let quotient_trunc loc _ args =
    match args with
      | [_; _] -> Vundefined
      | _ -> error loc "Quotient.trunc expects 2 arguments"

let interval_meet loc _ args =
  match args with
    | [Vcons (sym1, _) as x; Vcons (sym2, _) as y] ->
      (match (Sym.name sym1, Sym.name sym2) with
        | "i0", _ -> x
        | "i1", _ -> y
        | _ -> error loc "Unexpected arguments passed to I.meet")
    | _ -> error loc "I.meet expects 2 arguments"

let interval_not loc _ args =
  match args with
    | [Vcons (sym, _)] ->
      (match Sym.name sym with
        | "i0" -> Vcons ((dloc, "i1"), [])
        | "i1" -> Vcons ((dloc, "i0"), [])
        | _ -> error loc "Unexpected argument passed to I.not")
    | _ -> error loc "I.not expects 1 argument"

let integer_discrete loc _ args =
  match args with
    | [Vinteger x; Vinteger y] ->
      (match Z.equal x y with
        (* Return a Decidable  *)
       | true -> Vcons ((dloc, "yes"), [])
       | false -> Vcons ((dloc, "no"), []))
    | _ -> error loc "Integer.discrete expects 2 arguments"
let propext loc _ args =
    match args with
      | [_; _] -> Vundefined
      | _ -> error loc "propext expects 2 arguments"

let register_builtin_functions () =
  List.iter (fun (name, f, arity) -> add_builtin_function name f arity)
            [
              ("Sexp.block"    , make_block, 1);
              ("Sexp.symbol"   , make_symbol, 1);
              ("Sexp.string"   , make_string, 1);
              ("Sexp.integer"  , make_integer, 1);
              ("Sexp.float"    , make_float, 1);
              ("Sexp.node"     , make_node, 2);
              ("Sexp.dispatch" , sexp_dispatch, 7);
              ("TypeLevel.succ" , typelevel_succ, 1);
              ("TypeLevel.⊔"  , typelevel_lub, 2);
              ("Reader.parse" , reader_parse,2);
              ("String.="      , string_eq, 2);
              ("String.concat" , string_concat, 2);
              ("String.sub"    , string_sub, 3);
              ("Sexp.="        , sexp_eq, 2);
              ("Sexp.debug_print", sexp_debug_print, 1);
              ("Float->String" , float_to_string, 1);
              ("Int->String"   , int_to_string, 1);
              ("Integer->String", integer_to_string, 1);
              ("IO"            , type_constructor, max_int);
              ("IO.bind"       , io_bind, 2);
              ("IO.return"     , io_return, 1);
              ("IO.run"        , io_run, 2);
              ("Sys.exit"      , sys_exit, 1);
              ("File.open"     , file_open, 2);
              ("File.read"     , file_read, 2);
              ("File.write"    , file_write, 2);
              ("Heq.cast"      , nop_fun, 1);
              ("Heq.eq"        , arity0_fun, 0);
              ("Heq.uneq"      , heq_uneq, 2);
              ("Y"             , y_operator, 1);
              ("Ref"           , type_constructor, max_int);
              ("Ref.make"      , ref_make, 1);
              ("Ref.read"      , ref_read, 1);
              ("Ref.write"     , ref_write, 2);
              ("gensym"        , gensym, 1);
              ("Elab.getenv"   , getenv, 1);
              ("Elab.isbound"  , is_bound, 2);
              ("Elab.isconstructor", is_constructor, 2);
              ("Elab.is-nth-erasable", is_nth_erasable, 3);
              ("Elab.is-arg-erasable", is_arg_erasable, 3);
              ("Elab.nth-arg"  , nth_arg, 3);
              ("Elab.arg-pos"  , arg_pos, 3);
              ("Array"         , type_constructor, max_int);
              ("Array.append"  , array_append,2);
              ("Array.create"  , array_create,2);
              ("Array.length"  , array_length,1);
              ("Array.set"     , array_set,3);
              ("Array.get"     , array_get,3);
              ("Array.empty"   , array_empty,1);
              ("Test.fatal"    , test_fatal,2);
              ("Test.warning"  , test_warning,2);
              ("Test.info"     , test_info,2);
              ("Test.location" , test_location,1);
              ("Test.true"     , test_true,2);
              ("Test.false"    , test_false,2);
              ("Test.eq"       , test_eq,3);
              ("Test.neq"      , test_neq,3);
              ("Quotient"      , type_constructor, max_int);
              ("Quotient.in"   , nop_fun, 1);
              ("Quotient.eq"   , nop_fun, 1); 
              ("Quotient.trunc", quotient_trunc, 2);
              ("Quotient.elim" , quotient_elim, 2);
              ("I.transp"      , nop_fun, 1);
              ("I.meet"        , interval_meet, 2);
              ("I.not"         , interval_not, 1);
              ("Integer.discrete", integer_discrete, 2);
              ("propExt"       , propext, 2);
            ]
let _ = register_builtin_functions ()

let builtin_constant v loc _depth args_val = match args_val with
  (* FIXME: Dummy arg because we currently can't define a Builtin
   * *constant*.  *)
  | [_] -> v
  | _ -> error loc "Builtin almost-constant takes a unit argument"

let register_builtin_constants () =
  List.iter (fun (name, v) -> add_builtin_function name (builtin_constant v) 1)
            [
              ("File.stdout", Vout stdout);
              ("Sys.cpu_time", Vcommand (fun () -> Vfloat (Sys.time ())))
            ]
let _ = register_builtin_constants ()

let eval lxp ctx = eval lxp ctx ([], [])

let debug_eval lxp ctx =
    try eval lxp ctx
    with e -> (
      let ectx = !global_eval_ctx in
      let eval_trace = fst (get_trace ()) in
      Log.log_info ~section:"EVAL" ~print_action:(fun _ ->
          print_rte_ctx ectx;
          print_eval_trace (Some eval_trace)
        )
        "Exception occured during evaluation in the following context:";
      raise e)

let eval_decls decls ctx = eval_decls decls ctx ([], [])

let eval_decls_toplevel (decls: (vname * elexp) list list) ctx =
  (* Add toplevel decls function *)
  List.fold_left (fun ctx decls -> eval_decls decls ctx)
                 ctx decls

(*  Eval a list of lexp *)
let eval_all lxps rctx silent =
    let evalfun = if silent then eval else debug_eval in
    List.map (fun g -> evalfun g rctx) lxps


module CMap
  (* Memoization table.  FIXME: Ideally the keys should be "weak", but
   * I haven't found any such functionality in OCaml's libs.  *)
  = Hashtbl.Make
      (struct type t = lexp_context let hash = Hashtbl.hash let equal = (==) end)
let ctx_memo = CMap.create 1000

let not_closed rctx ((o, vm) : DB.set) =
  IMap.fold (fun i () nc -> let i = i + o in
                         let (_, rc) = Myers.nth i rctx in
                         match !rc with Vundefined -> i::nc | _ -> nc)
            vm []

let closed_p rctx (fvs, (mvs, _)) =
  not_closed rctx fvs = []
  (* FIXME: Handle metavars!  *)
  && IMap.is_empty mvs

let from_lctx (lctx: lexp_context): runtime_env =
  (* FIXME: `eval` with a disabled IO.run.  *)
  let rec from_lctx' (lctx: lexp_context): runtime_env =
    match lctx_view lctx with
    | CVempty -> Myers.nil
    | CVlet (loname, def, _, lctx)
      -> let rctx = from_lctx lctx in
         Myers.cons (loname,
                     ref (match def with
                          | LetDef (_, e)
                            -> if closed_p rctx (OL.fv e) then
                                eval (OL.erase_type lctx e) rctx
                              else Vundefined
                          | _ -> Vundefined))
                    rctx
    | CVfix (defs, lctx)
      -> let fvs = List.fold_left
                     (fun fvs (_, e, _)
                      -> OL.fv_union fvs (OL.fv e))
                     OL.fv_empty
                     defs in
         let rctx = from_lctx lctx in
         let (nrctx, evs, alldefs)
           = List.fold_left (fun (rctx, evs, alldefs) (loname, e, _)
                             -> let rc = ref Vundefined in
                                let nrctx = Myers.cons (loname, rc) rctx in
                                (nrctx, (e, rc)::evs, alldefs))
                            (rctx, [], true) defs in
         let _ =
           (* FIXME: Evaluate those defs that we can, even if not all defs
            * are present!  *)
           let lctx' = DB.lctx_extend_rec lctx defs in
           if alldefs && closed_p rctx (OL.fv_hoist (List.length defs) fvs) then
             List.iter (fun (e, rc) -> rc := eval (OL.erase_type lctx' e) nrctx) evs
           else () in
         nrctx
  and from_lctx lctx =
    try CMap.find ctx_memo lctx
    with Not_found
         -> let r = from_lctx' lctx in
            CMap.add ctx_memo lctx r;
            r
  in from_lctx lctx

(* build a rctx from a ectx.  *)
let from_ectx (ctx: elab_context): runtime_env =
  from_lctx (ectx_to_lctx ctx)

class ast_interpreter lctx = object
  inherit Backend.interactive

  val mutable rctx = from_lctx lctx
  val mutable lctx = lctx

  method process_decls ldecls =
    let lctx', eldecls = Opslexp.clean_decls lctx ldecls in
    rctx <- eval_decls eldecls rctx;
    lctx <- lctx'

  method eval_expr lexp =
    let elexp = Opslexp.erase_type lctx lexp in
    debug_eval elexp rctx

  method print_rte_ctx = Env.print_rte_ctx rctx

  method dump_rte_ctx = Env.dump_rte_ctx rctx
end
