(* sexp.ml --- The Lisp-style Sexp abstract syntax tree.

Copyright (C) 2011-2023  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Util
open Prelexer

let sexp_error ?print_action loc fmt =
  Log.log_error ~section:"SEXP" ?print_action ~loc fmt

open struct
  let escape_sym_name (name : string) : string =
    let is_normal_char c =
      c > ' '
      && c <> '\\'
      && c <> '%'
      && c <> '"'
      && c <> '{'
      && c <> '}'
      && Grammar.default_stt.(Char.code c) = Grammar.CKnormal
    in
    let escape_char c =
      let cc = Seq.return c in
      if is_normal_char c then cc else Seq.cons '\\' cc
    in

    if String.for_all is_normal_char name
    then name
    else name |> String.to_seq |> Seq.flat_map escape_char |> String.of_seq
end

module Sym = struct
  type t = Source.Location.t * string

  let intern : location : Source.Location.t -> string -> t =
    let module SHash = Hashtbl.Make(String) in
    let shash = SHash.create 1000 in
    fun ~location name ->
    let name' =
      try SHash.find shash name with
      | Not_found -> SHash.add shash name name; name
    in
    (location, name')

  let location : t -> location =
    fst

  let name : t -> string =
    snd

  let equal (l : t) (r : t) : bool =
    name l = name r

  let same (l : t) (r : t) : bool =
    Source.Location.same (location l) (location r)
    && name l = name r

  (** Prints the symbol. Unlike simply printing the name of the symbol, this
      function will escape special characters so it can be read back.

      Note that we do not track the lexical environment whence it came, so we
      assume the default configuration. *)
  let pp_print (f : Format.formatter) (_, name : t) : unit =
    name |> escape_sym_name |> Format.pp_print_string f

  let to_string (sym : t) : string =
    Format.asprintf "%a" pp_print sym
end
type symbol = Sym.t

(** Syntactic expression, kind of like Lisp.  *)
type t =
  | Block of Source.Location.t * pretoken list
  | Symbol of Sym.t
  | String of Source.Location.t * string
  | Integer of Source.Location.t * Z.t
  | Float of Source.Location.t * float
  | Node of Source.Location.t * t * t list
type sexp = t
type token = sexp
type sinfo = sexp

let location : sexp -> location = function
  | Block (l, _) -> l
  | Symbol (l, _) -> l
  | String (l, _) -> l
  | Integer (l, _) -> l
  | Float (l, _) -> l
  | Node (l, _, _) -> l

let symbol ~(location : Source.Location.t) (name : string) : t =
  Symbol (Sym.intern ~location name)

let node (head : t) (tail : t list) : t =
  let location =
    List.fold_left
      (fun l e -> Source.Location.extend l (location e))
      (location head)
      tail
  in
  Node (location, head, tail)

let epsilon (location : Source.Location.t) : sexp =
  Symbol (Sym.intern ~location "")

let dummy_epsilon = epsilon dummy_location
let dummy_sinfo = epsilon dummy_location

type vname = sinfo * string option
type vref = vname * db_index

let string_of_vname ?(default : string = "<anon>") : vname -> string = function
  | (_, None) -> default
  | (_, Some (name)) -> escape_sym_name name

let pp_print_vname
      ?(default : string option) (f : Format.formatter) (name : vname)
    : unit =
  Format.pp_print_string f (string_of_vname ?default name)

(********************** Sexp tests **********************)

let pred_symbol s pred =
  match s with
  | Symbol (_, n) -> pred n
  | _ -> false

(*************** The Sexp Printer *********************)

let rec pp_print (f : Format.formatter) (sexp : sexp) : unit =
  Format.pp_open_stag f (Source.Located (location sexp));
  begin match sexp with
  | Block (_, pretokens)
    -> Format.fprintf f "@[<hov 1>{%a}@]" Pretoken.pp_print_list pretokens
  | Symbol (_, "")
    -> Format.pp_print_string f "ε"
  | Symbol (_, name)
    -> Format.pp_print_string f name
  | String (_, value)
    -> Format.fprintf f {|"%s"|} (String.escaped value)
  | Integer (_, value)
    -> Z.pp_print f value
  | Float (_, value)
    -> Format.pp_print_float f value
  | Node (_, head, tail)
    -> Format.fprintf f "@[<hov 1>(%a)@]" pp_print_list (head :: tail)
  end;
  Format.pp_close_stag f ()

and pp_print_list (f : Format.formatter) (sexps : sexp list) : unit =
  Format.pp_print_list ~pp_sep:Format.pp_print_space pp_print f sexps

let to_string (sexp : sexp) : string =
  Format.asprintf "%a" pp_print sexp

let print (sexp : sexp) : unit =
  Format.printf "%a" pp_print sexp

let sexp_name s =
  match s with
    | Block   _ -> "Block"
    | Symbol (_, "") -> "ε"
    | Symbol  _ -> "Symbol"
    | String  _ -> "String"
    | Integer _ -> "Integer"
    | Float   _ -> "Float"
    | Node    _ -> "Node"

(*************** The Sexp Parser *********************)

let sexp_parse
      (g : Grammar.t) (tokens : sexp list) (level : int)
    : sexp * sexp list =

  let compose_symbol (ss : symbol list) = match ss with
      | [] -> (Log.internal_error "empty operator!")
      | (l,_)::_ -> (l, String.concat "_" (List.map (fun (_,s) -> s)
                                                   (List.rev ss))) in

  let push_largs largs rargs closer = match List.rev rargs with
      | [] -> if closer then dummy_epsilon :: largs else largs
      | e :: [] -> e :: largs
      | e :: es -> node e es :: largs
  in

  let make_node op largs rargs closer =
    let args = List.rev (push_largs largs rargs closer) in
    match op, args with
    | [], [] -> dummy_epsilon
    | [], [e] -> e
    | [], e :: es -> node e es
    | ss, _
      -> let head_location, head_name = compose_symbol ss in
         match head_name, args with
         (* FIXME: While it's usualy good to strip away parens, this makes
            assumptions about the grammar (i.e. there's a rule
            « exp ::= '(' exp ')' »), and this is sometimes not desired (e.g. to
            distinguish "a b ⊢ c d" from "(a b) ⊢ (c d)"). *)
         | "(_)", [arg] -> arg (* Strip away parens.  *)
         | _
           -> let head = symbol ~location:head_location head_name in
              node head args
  in

  (* `op' is the operator being parsed. E.g. for let...in... it would be either
     ["let"] or ["in";"let"] depending on which part we've parsed already.

     `largs' are the args to the left of the latest infix; they belong to `op'.

     `rargs' are the sexps that compose the arg to the right of the latest infix
     symbol. They may belong to `op' or to a later infix operator we haven't
     seen yet which binds more tightly. *)
  let rec recur rest level op largs rargs =
    match rest with
    | (Symbol ((l, name) as s) as e) :: rest'
      -> begin match Grammar.find name g with
         | (None, None)
           -> recur rest' level op largs (e :: rargs)

         (* Open paren or prefix.  *)
         | (None, Some rl)
           -> let (e, rest) = recur rest' rl [s] [] []
              in recur rest level op largs (e :: rargs)

         (* A symbol that closes the currently parsed element.  *)
         | (Some ll, _) when ll < level
           -> let node = make_node ((l, "") :: op) largs rargs true in
              (node, rest)

         (* A closer without matching opener or a postfix symbol that binds very
            tightly. Previously, we signaled an error (assuming the former), but
            it prevented the use tightly binding postfix operators.

            For example, it signaled spurious errors when parsing expressions with
            a tightly binding postfix # operator that implemented record
            construction by taking an inductive as argument and returning the
            inductive's only constructor. *)
         | (Some ll, None) when ll > level
           -> let rarg = make_node [(l, name); (l, "")] [] rargs true in
              recur rest' level op largs [rarg]

         (* A new infix which binds more tightly, i.e. does not close the
            current `op' but takes its `rargs' instead. *)
         | (Some ll, Some rl) when ll > level
           -> let largs' = push_largs [] rargs true in
              let (e, rest'') = recur rest' rl [s; (l, "")] largs' [] in
              recur rest'' level op largs [e]

         | (Some ll, Some rl)
           -> let op' =
                (* Having the same left and right precedence means it is a
                   separator such as ‘;’ or ‘|’. In that case, only add it the
                   operator name if it is not present. For example, adding ‘|’ to
                   ‘case_’ yields ‘case_|’, but adding another ‘|’ to ‘case_|’
                   also yields ‘case_|’. *)
                if
                  ll == rl
                  && match op with (_, name') :: _ -> name = name' | _ -> false
                then op
                else s :: op
              in
              recur rest' rl op' (push_largs largs rargs true) []

         | (Some _ll, None)
           -> let node = make_node (s :: op) largs rargs true in
              (node, rest')
         end

    | e :: rest -> recur rest level op largs (e :: rargs)

    | []
      -> let op' = match rargs with [] -> op | _ -> (dummy_location, "") :: op in
         let node = make_node op' largs rargs false in
         (node, [])
  in
  recur tokens level [] [] []

let sexp_parse_all grm tokens limit : sexp * token list =
  let level =
    match limit with
    | None -> min_int
    | Some token ->
       match Grammar.find token grm with
       | (Some ll, Some _) -> ll + 1
       | _ -> Log.internal_error {|Can't find level of "%s"|} token
  in
  let (e, rest) = sexp_parse grm tokens level in
  let se = match e with
    | Node (_, Symbol (_, ""), [e]) -> e
    | Node (location, Symbol (_, ""), e :: es) -> Node (location, e, es)
    | _ -> Log.internal_error "Didn't find a toplevel"
  in
  match rest with
  | [] -> (se,rest)
  | Symbol (l,t) :: rest
    -> if not (Some t = limit)
       then sexp_error l {|Stray closing token: "%s"|} t;
       (se,rest)
  | _ -> (Log.internal_error "Stopped parsing before the end!")

(* "sexp_p" is for "parsing" and "sexp_u" is for "unparsing".  *)

let sexp_p_list (s : sexp) (exceptions : string list) : sexp list =
  match s with
  | Symbol (_, "") -> []
  | Node (_, Symbol (_, head), _tail) when List.mem head exceptions -> [s]
  | Node (_, head, tail)  -> head :: tail
  | _ -> [s]

let sexp_u_list (ss : sexp list) (location : Source.Location.t) : sexp =
  match ss with
  | [] -> dummy_epsilon
  | [s] -> s
  | (s :: ss) -> Node (location, s, ss)

(*  Parse all the Sexp *)
let sexp_parse_all_to_list grm tokens limit : sexp list =
    let rec sexp_parse_impl grm tokens limit acc =
        match tokens with
            (* We are done parsing *)
            | [] -> List.rev acc    (* What does list rev do ? *)
            (* Keep going *)
            | _ -> let (sxp, rest) = sexp_parse_all grm tokens limit in
                    sexp_parse_impl grm rest limit (sxp :: acc) in
    sexp_parse_impl grm tokens limit []

(** Sexp comparison, ignoring source-line-number info, used for tests.  *)
let rec sexp_equal s1 s2 = match s1, s2 with
  | Block (_, ps1), Block (_, ps2) -> List.equal Pretoken.equal ps1 ps2
  | Symbol (_, s1), Symbol (_, s2) -> s1 = s2
  | String (_, s1), String (_, s2) -> s1 = s2
  | Integer (_, n1), Integer (_, n2) -> n1 = n2
  | Float (_, n1), Float (_, n2) -> n1 = n2
  | Node (_, s1, ss1), Node (_, s2, ss2)
    -> sexp_equal s1 s2 && List.equal sexp_equal ss1 ss2
  | _ -> false

(** Sexp comparison, *with* source-line-number info, used for tests.  *)
let rec same l r =
  match l, r with
  | Block (l_location, l_pretokens), Block (r_location, r_pretokens)
    -> Source.Location.same l_location r_location
       && List.equal Pretoken.same l_pretokens r_pretokens
  | Symbol (l_sym), Symbol (r_sym)
    -> Sym.same l_sym r_sym
  | String (l_location, l_value), String (r_location, r_value)
    -> Source.Location.same l_location r_location
       && l_value = r_value
  | Integer (l_location, l_value), Integer (r_location, r_value)
    -> Source.Location.same l_location r_location
       && l_value = r_value
  | Float (l_location, l_value), Float (r_location, r_value)
    -> Source.Location.same l_location r_location
       && l_value = r_value
  | Node (l_location, l_head, l_tail), Node (r_location, r_head, r_tail)
    -> Source.Location.same l_location r_location
       && same l_head r_head
       && List.equal same l_tail r_tail
  | _, _ -> false
