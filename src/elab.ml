(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2023  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * Description:
 *
 * Elaborate Sexp expression into Lexp.
 * This includes type inference, and macro expansion.
 *
 * While elaboration will discover most type errors, strictly speaking
 * this phase does not need to perform any checks, and it can instead presume
 * that the code is well-behaved.  The real type checks (and totality
 * checks) are performed later in OL.check.
 *
 * -------------------------------------------------------------------------- *)

open Util

open Prelexer
open Lexer

open Sexp
open Pexp
open Lexp

open Env
open Debruijn
module DB = Debruijn
module M = Myers
module EV = Eval

open Grammar
module BI = Builtin

module Unif = Unification
module Inst = Instargs

module OL = Opslexp
module EL = Elexp

open Printf

(* dummies *)
let dloc = dummy_location
let dsinfo = DB.dsinfo

let parsing_internals = ref false
let btl_folder =
  try Sys.getenv "TYPER_BUILTINS"
  with Not_found -> "./btl"

let fatal ?print_action ?loc fmt =
  Log.log_fatal ~section:"ELAB" ?print_action ?loc fmt
let error ?print_action ?loc fmt =
  Log.log_error ~section:"ELAB" ?print_action ?loc fmt
let warning ?print_action ?loc fmt =
  Log.log_warning ~section:"ELAB" ?print_action ?loc fmt
let info ?print_action ?loc fmt =
  Log.log_info ~section:"ELAB" ?print_action ?loc fmt

let indent_line str =
  "        > " ^ str
let print_indent_line str =
  print_endline (indent_line str)
let print_details to_name to_str elem =
  print_indent_line ((to_name elem) ^ ": " ^ (to_str elem))
let lexp_print_details lexp () =
  print_indent_line (Lexp.to_string lexp)
let value_print_details value () =
  print_details value_name value_string value

let lexp_error loc lexp fmt =
  error ~loc ~print_action:(lexp_print_details lexp) fmt
let lexp_fatal loc lexp =
  fatal ~loc ~print_action:(lexp_print_details lexp)
let value_fatal loc value =
  fatal ~loc ~print_action:(value_print_details value)

let macro_tracing_enabled = ref false
let trace_macro ~location fmt = Log.log_debug ~loc:location ~section:"MACRO" fmt

(** Type info returned by elaboration.  *)
type sform_type =
  | Checked       (* Checked that the expression has the requested type.  *)
  | Inferred of ltype            (* Hasn't looked as the requested type.  *)
  | Lazy    (* Hasn't looked as the requested type, nor inferred a type.  *)

type special_forms_map =
  (elab_context -> sinfo -> sexp list -> ltype option
   -> (lexp * sform_type)) SMap.t

type special_decl_forms_map =
  (elab_context -> sinfo -> sexp list -> elab_context) SMap.t

let special_forms : special_forms_map ref = ref SMap.empty
let special_decl_forms : special_decl_forms_map ref = ref SMap.empty
let type_special_form = BI.new_builtin_type "Special-Form" type0
let type_special_decl_form = BI.new_builtin_type "Special-Decl-Form" type0

let add_special_form (name, func) =
  OL.add_builtin_cst name (mkBuiltin ((dloc, name), type_special_form));
  special_forms := SMap.add name func (!special_forms)

let add_special_decl_form (name, func) =
  OL.add_builtin_cst name (mkBuiltin ((dloc, name), type_special_decl_form));
  special_decl_forms := SMap.add name func (!special_decl_forms)

let get_special_form name =
  SMap.find name (!special_forms)

let get_special_decl_form name =
  SMap.find name (!special_decl_forms)

(* Used for sform_load because sform are
 * added before default context's function. *)
let sform_default_ectx = ref empty_elab_context

(* The prefix `elab_check_` is used for functions which do internal checking
 * (i.e. errors signalled here correspond to internal errors rather than
 * to errors in the user's code).  *)

let elab_check_sort (ctx : elab_context) lsort (var: vname) ltp =
  match (try OL.lexp'_whnf lsort (ectx_to_lctx ctx)
         with e ->
           info ~print_action:(fun _ -> Lexp.print lsort; print_newline ())
             ~loc:(Lexp.location lsort)
             "Exception during whnf of sort:";
           raise e) with
  | Sort (_, _) -> () (* All clear!  *)
  | _
    -> let tystr = Lexp.to_string ltp ^ " : " ^ Lexp.to_string lsort in
       match var with
       | (l, None) -> lexp_error (Sexp.location l) ltp {|"%s" is not a proper type|} tystr
       | (l, Some name)
         -> lexp_error (Sexp.location l) ltp {|Type of "%s" is not a proper type: %s|} name tystr

let elab_check_proper_type (ctx : elab_context) ltp var =
  try elab_check_sort ctx (OL.check (ectx_to_lctx ctx) ltp) var ltp
  with e -> match e with
            | Log.Stop_compilation _ -> raise e
            | _
              -> info
                   ~print_action:(fun _ ->
                     print_lexp_ctx (ectx_to_lctx ctx); print_newline ()
                   )
                   ~loc:(Lexp.location ltp)
                   {|Exception while checking type "%s"%s|}
                   (Lexp.to_string ltp)
                   (match var with
                    | (_, None) -> ""
                    | (_, Some name) -> " of var `" ^ name ^ "`");
                 raise e

let elab_check_def (ctx : elab_context) var lxp ltype =
  let lctx = ectx_to_lctx ctx in
  let loc = lexp_sinfo lxp in

  let ltype' = try OL.check lctx lxp
    with e -> match e with
      | Log.Stop_compilation _ -> raise e
      | _ ->
         info
           ~print_action:(fun _ ->
             lexp_print_details lxp ();
             print_lexp_ctx (ectx_to_lctx ctx);
             print_newline ()
           )
           ~loc:(Sexp.location loc)
           "Error while type-checking";
         raise e in
  if (try OL.conv_p (ectx_to_lctx ctx) ltype ltype'
      with
      | e
        -> info
             ~print_action:(lexp_print_details lxp)
             ~loc:(Sexp.location loc)
             "Exception while conversion-checking types: %s and %s"
             (Lexp.to_string ltype)
             (Lexp.to_string ltype');
           raise e)
  then
    elab_check_proper_type ctx ltype var
  else
    fatal
      ~print_action:(fun _ ->
        List.iter print_indent_line [
            (match var with (_, Some n) -> n | _ -> "<anon>")
            ^ " = " ^ Lexp.to_string lxp ^ " !: " ^ Lexp.to_string ltype;
            "                    because";
            Lexp.to_string ltype' ^ " != " ^ Lexp.to_string ltype
      ])
      ~loc:(Sexp.location loc)
      "Type check error: ¡¡ctx_define error!!"

let ctx_extend (ctx: elab_context) (var : vname) def ltype =
  elab_check_proper_type ctx ltype var;
  ectx_extend ctx var def ltype

let ctx_define (ctx: elab_context) var lxp ltype =
  elab_check_def ctx var lxp ltype;
  ectx_extend ctx var (LetDef (0, lxp)) ltype

let ctx_define_rec (ctx: elab_context) decls =
  let nctx = ectx_extend_rec ctx decls in
  let _ = List.fold_left (fun n (var, _lxp, ltp)
                          -> elab_check_proper_type
                              nctx (push_susp ltp (S.shift n)) var;
                            n - 1)
                         (List.length decls)
                         decls in
  let _ = List.fold_left (fun n (var, lxp, ltp)
                          -> elab_check_def nctx var lxp
                                           (push_susp ltp (S.shift n));
                            n - 1)
                         (List.length decls)
                         decls in
  nctx

(*  The main job of lexp (currently) is to determine variable name (index)
 *  and to regroup type specification with their variable
 *
 *  elab_context is composed of two environment: senv and env.
 *  the senv environment is used to find the correct debruijn index
 *  while the env environment is used to save variable information.
 *  the env environment look a lot like the runtime environment that will be
 *  used in the eval section.
 *
 *  While most of the time senv and env will be synchronised it is
 *  possible for env to hold more variables than senv since senv is a map
 *  which does not allow multiple definition while env does.
 *
 *)

(*
 *      Type Inference
 * --------------------- *)
(* Parsing a Pexp into an Lexp is really "elaboration", i.e. it needs to
 * infer the types and perform macro-expansion.
 *
 * More specifically, we do it with 2 mutually recursive functions:
 * - `check` takes an Sexp along with its expected type and returns an Lexp
 *   of that type (hopefully)
 * - `infer` takes an Sexp and infers its type (which it returns along with
 *   the Lexp).
 * This is the idea of "bidirectional type checking", which minimizes
 * the amount of "guessing" and/or annotations.  Since we infer types anyway
 * it doesn't really reduce the amount of type annotations for us, but it
 * reduces the amount of inference and checking, i.e. it reduces the number of
 * metavars we create/instantiate/dereference as well as the number of call to
 * the unification algorithm.
 * Basically guessing/annotations is only needed at those few places where the
 * code is not fully-normalized (which in normal programs is only in "let"
 * definitions) as well as when we fill implicit arguments.
 *)

let newMetavar (ctx : lexp_context) sl name t =
  let meta = Unif.create_metavar ctx sl t in
  mkMetavar (meta, S.identity, name)

(* Metavars can be instantiated/solved by instance resolution if their
   context is saved. `newInstanceMetavar` creates a metavar and saves
   its elab context for this purpose. This is a necessary, but not
   sufficient condition: the metavar also needs to have a type that is
   a class for resolution to happen. `newShiftedInstanceMetavar` does
   the same as `newInstanceMetavar`, but for metavars whose context
   does not include the variables in the current scope level (which
   seems to be the case for explicit metavariables, currently). *)

let newInstanceMetavar (ctx : elab_context) name t =
  let lctx = ectx_to_lctx ctx in
  let sl = ectx_to_scope_level ctx in
  let meta = Unif.create_metavar lctx sl t in
  Inst.save_metavar_resolution_ctxt meta ctx (fst name);
  mkMetavar (meta, S.identity, name)

let newShiftedInstanceMetavar (ctx : elab_context) name t =
  let ctx_shift = ectx_local_scope_size ctx in
  let octx = Myers.nthcdr ctx_shift (ectx_to_lctx ctx) in
  let sl = ectx_to_scope_level ctx in
  let subst = S.shift ctx_shift in
  let shift_senv (n, senv_map) =
    let new_n = n - ctx_shift in
    (new_n, SMap.filter (fun _ n' -> n' <= new_n) senv_map) in
  let shift_tcctx (tcs, inst_def, insts) =
    (tcs, inst_def, DB.set_hoist ctx_shift insts) in
  let nctx = ctx
             |> ectx_set_senv (shift_senv (ectx_get_senv ctx))
             |> ectx_set_lctx octx
             |> ectx_set_tcctx (shift_tcctx (ectx_to_tcctx ctx)) in
  let meta = Unif.create_metavar octx sl t in
  Inst.save_metavar_resolution_ctxt meta nctx (fst name);
  mkMetavar (meta, subst, name)

let newMetalevel (ctx : lexp_context) sl loc =
  newMetavar ctx sl (loc, Some "ℓ") type_level

let newMetatype (ctx : lexp_context) sl loc
  = newMetavar ctx sl (loc, Some "τ")
               (mkSort (loc, Stype (newMetalevel ctx sl loc)))

(* Functions used when we need to return some lexp/ltype but
 * an error makes it impossible to return "the right one".  *)
let mkDummy_type ctx loc = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc
let mkDummy_check ctx loc t = newMetavar (ectx_to_lctx ctx) dummy_scope_level
                                         (loc, None) t
let mkDummy_infer ctx loc =
  let t = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc in
  (mkDummy_check ctx loc t, t)

let sdform_define_operator (ctx : elab_context) loc sargs : elab_context =
  match sargs with
  | [String (_, name); l; r]
    -> let level s = match s with
        | Symbol (_, "") -> None
        | Integer (_, n) -> Some (Z.to_int n)
        | _ -> sexp_error (Sexp.location s) "Expecting an integer or ()"; None in
       let grm = ectx_get_grammar ctx in
       ectx_set_grammar (Grammar.add name (level l, level r) grm) ctx
  | [o; _; _]
    -> sexp_error (Sexp.location o) "Expecting a string"; ctx
  | _
    -> sexp_error (Sexp.location loc) "define-operator expects 3 argument"; ctx

let sform_dummy_ret ctx loc =
  let t = newMetatype (ectx_to_lctx ctx) dummy_scope_level loc in
  (newMetavar (ectx_to_lctx ctx) dummy_scope_level
     (loc, Some "special-form-error") t,
   Inferred t)

let elab_varref ctx (loc, name)
  = try
      let idx = senv_lookup name ctx in
      let id = (loc, Some name) in
      let lxp = mkVar (id, idx) in
      let ltp = env_lookup_type ctx (id, idx) in
      (lxp, Inferred ltp)
  with Senv_Lookup_Fail xs ->
    (let relateds =
       if ((List.length xs) > 0) then
         ". Did you mean: " ^ (String.concat " or " xs) ^" ?"
       else "" in
     sexp_error (Sexp.location loc) {|The variable "%s" was not declared%s|} name relateds;
     sform_dummy_ret ctx loc)

(* Turn metavar into plain vars after generalization.
 * ids: an IMap that maps metavars to their position as argument
 *   (first arg gets position 0).  *)
let meta_to_var ids (e : lexp) =

  let count = IMap.cardinal ids in

  (* Yuck!  Yuck!  Yuck!
   * This is very messy.  What we need to do starts as follows:
   * We have a `Γ ⊢ e : τ` and this `e` contains some metavars `m₁…mₙ : τ₁…τₙ`
   * that we want to change into formal arguments, i.e. we want to turn `e`
   * into something like
   *
   *     Γ ⊢ λ x₁…xₙ ≡> e[x₁…xₙ/m₁…mₙ] : τ₁…τₙ ≡> τ
   *
   * The above substitution is not the usual capture-avoiding substitution
   * since it replaces metavars with vars rather than vars with terms.
   * It's more like *instanciation* of those metavars.
   * And indeed, ideally it should be a simple matter of instanciating
   * those metavars with temrs that are variable references.
   *
   * An important aspect here is that the rest of `e` also needs to be
   * changed because it will now live in a new context:
   *
   *     Γ,x₁:τ₁,…,xₙ:τₙ ⊢ e[x₁…xₙ/m₁…mₙ] : τ
   *
   * So we need to adjust all the variable references in `e` to account for
   * that, which we normally do with a simple `S.shift n`.
   *
   * The first problem comes here: `S.shift n` takes a term from
   * `Γ` to `Γ,x₁:τ₁,…,xₙ:τₙ`, making sure it still references the same
   * bindings as before, i.e. it makes sure the result *cannot* refer to
   * any `x₁…xₙ`!
   * I first thought "it's OK, I'll first do the `S.shift n` and I only
   * instanciate the metavars afterwards", but that does not help,
   * because every reference to a metavar is properly wrapped in a
   * pending application of the relevant substitution, so the `S.shift n`
   * still applies to it.
   *
   * For this reason, we have a `loop` below which does the substitution
   * of variable references for the metavars (since just instanciating
   * the metavars doesn't work).
   *
   * The second problem comes with the other metavars in `e`.
   * There are 3 kinds of metavars in `e`:
   *
   * A. Those that we want to replace with variable references.
   * B. Those that live in some higher enclosing scope.
   * C. The "others".
   *
   * Presumably, we have (A) under control.
   * For (B) the situation is easy enough: since they live in a higher
   * enlosing scope they can only refer to those variables that exist in
   * some prefix of `Γ`, so we just need to apply `S.shift n` to them.
   *
   * For (C), we'd like those metavars (which originally could refer to
   * any var in `Γ`) to now be a able to also refer to any of the
   * new vars `x₁…xₙ`.  So `S.shift n` is definitely not right for them.
   * Instead, I ended up writing `adjust_subst` which hacks up
   * the substitution attached to each metavar reference so it can
   * now refer to its original context *and* to `x₁…xₙ`.
   *
   * Arguably, now that we handle (C), we could handle (A) by
   * first treating them as (C) and then using instantiation.
   * But the code for (A) was already written, and it doesn't seem like
   * it would make things any simpler currently.
   *
   * Note: in the original HM algorithm (and in Twelf), (C) cannot occur
   * because every metavariable that's not in (B) is generalized
   * (i.e. will be in A).  But here we need (C) for cases like:
   *
   *     List : Type ? -> Type ?;
   *
   * Since the second `?` should not be generalized but should
   * be inferred from the definition.  I.e. the above should be
   * generalized to:
   *
   *     List : (ℓ : TypeLevel) ≡> Type ℓ -> Type ?;
   *
   * where the remaining `?` will be inferred by unification
   * when type-checking the definition of `Box` where ti be unified with `ℓ`,
   * the newly introduced variable!
   *)

  (* `o` is the binding offset until the root.  *)
  let rec adjust_subst o s = match s with
    | S.Identity n
      -> let o' = o - n in
        if o' < 0 then
          (* This metavar's original context is outside of our scope
           * (case (B) above), so don't let it refer to the new vars. *)
          S.Identity (n + count)
        else
          S.Identity n
    | S.Cons (e, s', n)
      -> let o' = o - n in
        if o' < 0 then
          (* This metavar's original context is outside of our scope
           * (case (B) above), so don't let it refer to the new vars. *)
          S.Cons (e, s', n + count)
        else
          S.Cons (loop o' e, adjust_subst o' s', n)

  (* `o` is the binding depth at which we are relative to the "root"
   * of the expression (i.e. where the new vars will be inserted).  *)
  and loop o e =
    match lexp_lexp' e with
    | Imm _ -> e
    | SortLevel SLz -> e
    | SortLevel (SLsucc e) -> mkSortLevel (mkSLsucc (loop o e))
    | SortLevel (SLlub (e1, e2)) -> mkSortLevel (mkSLlub' (loop o e1, loop o e2))
    | Sort (l, Stype e) -> mkSort (l, Stype (loop o e))
    | Sort (_, (StypeOmega | StypeLevel)) -> e
    | Builtin _ -> e
    | Var (n,i) -> if i < o then e else mkVar (n, i + count)
    | Proj (l, lxp, lbl) -> mkProj (l, loop o lxp, lbl)
    | Susp (e, s) -> loop o (push_susp e s)
    | Let (l, defs, e)
      -> let len = List.length defs in
        let (_, ndefs)
          = List.fold_right (fun (l,e,t) (o', defs)
                             -> let o' = o' - 1 in
                               (o', (l, loop (len + o) e,
                                     loop (o' + o) t) :: defs))
                            defs (len, []) in
        mkLet (l, ndefs, loop (len + o) e)
    | Arrow (l, ak, v, t1, t2)
      -> mkArrow (l, ak, v, loop o t1, loop (1 + o) t2)
    | Lambda (ak, v, t, e)
      -> mkLambda (ak, v, loop o t, loop (1 + o) e)
    | Call (l, f, args)
      -> mkCall (l, loop o f, List.map (fun (ak, e) -> (ak, loop o e)) args)
    | Inductive (l, label, args, cases)
      -> let alen = List.length args in
        let (_, nargs)
          = List.fold_right (fun (ak, v, t) (o', args)
                             -> let o' = o' - 1 in
                               (o', (ak, v, loop (o' + o) t)
                                    :: args))
                            args (alen, []) in
        let ncases
          = SMap.map
              (fun fields
               -> let flen = List.length fields in
                 let (_, nfields)
                   = List.fold_right
                       (fun (ak, v, t) (o', fields)
                        -> let o' = o' - 1 in
                          (o', (ak, v, loop (o' + o) t)
                               :: fields))
                       fields (flen + alen, []) in
                 nfields)
              cases in
        mkInductive (l, label, nargs, ncases)
    | Cons (t, l) -> mkCons (loop o t, l)
    | Case (l, e, t, cases, default)
      -> let ncases
          = SMap.map
              (fun (l, fields, e)
               -> (l, fields, loop (o + List.length fields + 1) e))
              cases in
        mkCase (l, loop o e, loop o t, ncases,
                match default with None -> None
                                 | Some (v, e) -> Some (v, loop (2 + o) e))
    | Metavar (id, s, name)
      -> if IMap.mem id ids then
          mkVar (name, o + count - IMap.find id ids)
        else match metavar_lookup id with
             | MVal e -> loop o (push_susp e s)
             | _ -> mkMetavar (id, adjust_subst o s, name)
  in loop 0 e

let sort_generalized_metavars sl cl ctx mfvs =
  (* TypeLevel parameters have to come first, and the metavariables
     must be sorted topologically, so that the type of a parameter can
     only refer to earlier parameters.

     We do a DFS through the metavariable types to sort them and we
     store any TypeLevel argument in a separate list. *)
  let rec handle_metavar id (sl', mt, cl', vname) (* The metavar *)
            ((typelevels, (* The front of the list (in reverse) *)
              params, (* The rest of the list (in reverse) *)
              seen) (* The set of indices already added to the lists *)
             as acc) =
    if sl' < sl || (* This metavar appears in the context, so we can't
                      generalize over it.  *)
         (IMap.mem id seen) (* We've already handled this metavar, skip it. *)
    then acc else
      (assert (cl' >= cl);
       let mt = if cl' > cl then
                  Inverse_subst.apply_inv_subst mt (S.shift (cl' - cl))
                else mt in
       let mv = (id, vname, mt) in
       if (OL.conv_p ctx mt type_level) then
         ((mv :: typelevels), params, (IMap.add id () seen))
       else
         let (_, (mt_mfvs, _)) = OL.fv mt in
         let (typelevels, params, seen) = handle_metavars mt_mfvs acc in
         (typelevels, mv :: params, (IMap.add id () seen))
         )
    and handle_metavars ids acc = IMap.fold handle_metavar ids acc in
  let (typelevels, params, _) = handle_metavars mfvs ([], [], IMap.empty) in
  List.append (List.rev typelevels) (List.rev params)

(* Generalize expression `e` with respect to its uninstantiated metavars.
 * `wrap` is the function that adds the relevant quantification, typically
 * either mkArrow or mkLambda.  *)
let generalize (nctx : elab_context) e =
  let l = Lexp.location e in
  let sl = ectx_to_scope_level nctx in
  let cl = Myers.length (ectx_to_lctx nctx) in
  let (_, (mfvs, nes)) = OL.fv e in
  if mfvs = IMap.empty
  then (fun _wrap e -> e) (* Nothing to generalize, yay!  *)
  else
    (* Sort `mvfs' topologically, and bring typelevel args to the front *)
    let mfvs = sort_generalized_metavars sl cl (ectx_to_lctx nctx) mfvs in
    let len = List.length mfvs in
    fun wrap e ->
    let rec loop ids n mfvs =
      assert (n = IMap.cardinal ids);
      match mfvs with
      | [] -> assert (n = len);
             meta_to_var ids e
      | ((id, vname, mt) :: mfvs)
        -> let mt' = meta_to_var ids mt in
          let n = n + 1 in
          let e' = loop (IMap.add id n ids) n mfvs in
          wrap (IMap.mem id nes) vname mt' l e' in
    loop (IMap.empty) 0 mfvs

let wrapLambda ne vname t _l e =
  mkLambda ((if ne then Aimplicit else Aerasable), vname, t, e)

let elab_p_id ((l,name) : symbol) : vname =
  (Symbol (l, name), match name with "_" -> None | _ -> Some name)

let rec sform_immediate ctx loc sargs ot =
  match sargs with
  | [(String _) as se]  -> mkImm (se), Inferred DB.type_string
  | [(Integer _) as se] -> mkImm (se), Inferred DB.type_integer
  | [(Float _) as se]   -> mkImm (se), Inferred DB.type_float
  | [Block (_location, pts)]
    -> let grm = ectx_get_grammar ctx in
       let tokens = lex default_stt pts in
       let (se, _) = sexp_parse_all grm tokens None in
       elaborate ctx se ot
  | [_se]
    -> sexp_error (Sexp.location loc) "Non-immediate passed to ##typer-immediate";
       sform_dummy_ret ctx loc
  | _
    -> sexp_error (Sexp.location loc) "Too many args to ##typer-immediate";
       sform_dummy_ret ctx loc


and sform_identifier ctx loc sargs ot =
  match sargs with
  | [Symbol (l,name)]
       when String.length name >= 1 && String.get name 0 == '#'
    -> if String.length name > 2 && String.get name 1 == '#' then
         let name = string_sub name 2 (String.length name) in
         try let e = OL.get_builtin name in
             (e, Inferred (OL.get_type (ectx_to_lctx ctx) e))
         with
         | Not_found
           -> sexp_error l {|Unknown builtin "%s"|} name;
              sform_dummy_ret ctx loc
       else
         (sexp_error l {|Invalid special identifier "%s"|} name;
          sform_dummy_ret ctx loc)

  | [Symbol (_loc, name)]
       when String.length name >= 1 && String.get name 0 = '?'
    -> let name = if name = "?" then "" else
                   string_sub name 1 (String.length name) in
      (* Shift the var so it can't refer to the local vars.
       * This is used so that in cases like "lambda t (y : ?) ... "
       * type inference can guess ? without having to wonder whether it
       * can refer to `t` or not.  If the user wants ? to be able to refer
       * to `t`, then they should explicitly write (y : ? t).  *)
      let ctx_shift = ectx_local_scope_size ctx in
      let octx = Myers.nthcdr ctx_shift (ectx_to_lctx ctx) in
      let sl = ectx_to_scope_level ctx in
      let subst = S.shift ctx_shift in
      let (_, _, rmmap) = ectx_get_scope ctx in
      if not (name = "") && SMap.mem name (!rmmap) then
        let (idx, sl') = SMap.find name (!rmmap) in
        if sl = sl' then
          (mkMetavar (idx, subst, (loc, Some name)), Lazy)
        else
          (* FIXME: The variable is from another scope_level! It
             means that `subst` is not the right substitution for
             the metavar! *)
          fatal ~loc:(Sexp.location loc) ("Bug in the elaboration of a metavar"
                                          ^^ " repeated at a different scope level!")
      else
        let t = match ot with
          | None -> newMetatype octx sl loc
          | Some t
            (* `t` is defined in ctx instead of octx. *)
            -> Inverse_subst.apply_inv_subst t subst in
        let mv = newShiftedInstanceMetavar ctx (loc, Some name) t in
        (if not (name = "") then
           let idx =
           match lexp_lexp' mv with
             | Metavar (idx, _, _) -> idx
             | _ -> fatal ~loc:(Sexp.location loc) "newMetavar returned a non-Metavar" in
           rmmap := SMap.add name (idx, sl) (!rmmap));
        (mv, match ot with Some _ -> Checked | None -> Lazy)

  (* Normal identifier.  *)
  | [Symbol (_n, name)]
    -> elab_varref ctx (loc, name)

  | [_se]
    -> sexp_error (Sexp.location loc) "Non-symbol passed to ##typer-identifier";
       sform_dummy_ret ctx loc

  | _
    -> sexp_error (Sexp.location loc) "Too many args to ##typer-identifier";
       sform_dummy_ret ctx loc

and elab_via head default ctx se ot =
  match elab_varref ctx head with
  | (f, Inferred t)
    -> if (OL.conv_p (ectx_to_lctx ctx) t type_special_form) then
        elab_special_form ctx f [se] ot
      else if (OL.conv_p (ectx_to_lctx ctx) t
                 (BI.get_predef "Macro" ctx)) then
        elab_macro_call ctx f [se] ot
      else
        default ctx se [se] ot
  | _ -> default ctx se [se] ot

(* Infer or check, as the case may be.  *)
and elaborate ctx se ot =
  let (e, res_t) = match se with
  (* Rewrite SYM to `typer-identifier SYM`.  *)
  | Symbol _
    -> elab_via (se, "typer-identifier") sform_identifier ctx se ot

  (* Rewrite IMM to `typer-immediate IMM`.  *)
  | (Integer _ | Float _ | String _ | Block _)
    -> let l = Sexp.location se in
       elaborate ctx (Node (l, Symbol (l, "typer-immediate"), [se])) ot

  | Node (_, se, []) -> elaborate ctx se ot

  | Node (_, func, args)
    (* FIXME: Use (or merge with) `elab_via`!  *)
    -> let (f, t) as ft = infer func ctx in
      if (OL.conv_p (ectx_to_lctx ctx) t type_special_form) then
        elab_special_form ctx f args ot
      else if (OL.conv_p (ectx_to_lctx ctx) t
                         (BI.get_predef "Macro" ctx)) then
        elab_macro_call ctx f args ot
      else
        (* FIXME: I'd like to do something like:
         *    elab_via (l, "typer-funcall") sform_call ctx se ot
         * but that would force `typer-funcall` to elaborate `func`
         * a second time!
         * Maybe I should only elaborate `func` above if it's a symbol
         * (and maybe even use `elab_varref` rather than indirecting
         * through `typer-identifier`)?  *)
        elab_call ctx ft args in
  match ot with
  | Some t ->
     (match res_t with
      | Checked -> (e, res_t)
      | _ -> let inferred_t =
               match res_t with
               | Inferred t -> t
               | _ -> OL.get_type (ectx_to_lctx ctx) e in
             (check_inferred ctx e inferred_t t, Checked))
  | _ -> (e, res_t)

and infer (p : sexp) (ctx : elab_context): lexp * ltype =
  match elaborate ctx p None with
  | (_, Checked) -> fatal ~loc:(Sexp.location p) "`infer` got Checked!"
  | (e, Lazy) -> (e, OL.get_type (ectx_to_lctx ctx) e)
  | (e, Inferred t) -> (e, t)

and elab_special_form ctx f args ot =
  let loc = lexp_sinfo f in
  match (OL.lexp'_whnf f (ectx_to_lctx ctx)) with
  | Builtin ((_, name), _) ->
     (* Special form.  *)
     (get_special_form name) ctx loc args ot

  | _
    -> lexp_error (Sexp.location loc) f "Unknown special-form: %s" (Lexp.to_string f);
       sform_dummy_ret ctx loc

and elab_special_decl_form ctx f args =
  let loc = lexp_sinfo f in
  match (OL.lexp'_whnf f (ectx_to_lctx ctx)) with
  | Builtin ((_, name), _) ->
     (* Special form.  *)
     (get_special_decl_form name) ctx loc args
  | _ -> lexp_error (Sexp.location loc) f "Unknown special-decl-form: %s" (Lexp.to_string f); ctx

(* Build the list of implicit arguments to instantiate.  *)
and instantiate_implicit e t ctx =
  let rec instantiate t args =
    match OL.lexp'_whnf t (ectx_to_lctx ctx) with
    | Arrow (_, ((Aerasable | Aimplicit) as ak), (_, v), t1, t2)
      -> let arg = newInstanceMetavar ctx (lexp_sinfo e, v) t1 in
         instantiate (mkSusp t2 (S.substitute arg)) ((ak, arg)::args)
    | _ -> (mkCall (lexp_sinfo e, e, List.rev args), t)
  in instantiate t []

and resolve_instances e =
  Inst.resolve_instances instantiate_implicit e

and resolve_instances_and_generalize ctx e =
  resolve_instances e;
  generalize ctx e

and sdform_typeclass (ctx : elab_context) (_l : sinfo) sargs
    : elab_context =
  let make_typeclass ctx sarg =
    let (t, _) = infer sarg ctx in
    Inst.add_typeclass ctx t
  in
  List.fold_left make_typeclass ctx sargs

and sdform_instance (inst : bool) (ctx : elab_context) (_l : sinfo) sargs
    : elab_context =
  let make_instance ctx sarg =
    let (lxp, _) = infer sarg ctx in
    match lexp_lexp' lxp with
    | Var (_, idx) -> ectx_set_inst ctx idx inst
    | _ -> (lexp_error (Lexp.location lxp) lxp
              "Only variables can be instances"; ctx) in
  List.fold_left make_instance ctx sargs

and infer_type pexp ectx (var: vname) =
  (* We could also use lexp_check with an argument of the form
   * Sort (?s), but in most cases the metavar would be allocated
   * unnecessarily.  *)
  let t, s = infer pexp ectx in
  (match OL.lexp'_whnf s (ectx_to_lctx ectx) with
   | Sort (_, _) -> () (* All clear!  *)
   (* FIXME: We could automatically coerce Type levels to Sorts, so we
    * could write `(a : TypeLevel) -> a -> a` instead of
    * `(a : TypeLevel) -> Type_ a -> Type_ a`  *)
   | _ ->
      (* FIXME: Here we rule out TypeLevel/TypeOmega.
       * Maybe it's actually correct?!  *)
      let l = lexp_sinfo s in
      match
        Unif.unify (mkSort (l, Stype (newMetalevel
                                        (ectx_to_lctx ectx)
                                        (ectx_to_scope_level ectx)
                                        l)))
                   s
                   (ectx_to_lctx ectx) with
      | (_::_)
        -> (let typestr = Lexp.to_string t ^ " : " ^ Lexp.to_string s in
           match var with
           | (l, None)
             -> lexp_error (Sexp.location l) t {|"%s" is not a proper type|} typestr
           | (l, Some name)
             -> lexp_error
                  (Sexp.location l) t {|Type of "%s" is not a proper type: %s|} name typestr)
      | [] -> ());
  t

and lexp_let_decls declss (body: lexp) _ctx =
  List.fold_right (fun decls lxp -> mkLet (dsinfo, decls, lxp))
                  declss body

and unify_with_arrow ctx tloc lxp kind var aty
  = let arg = match aty with
      | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx) tloc
      | Some laty -> laty in
    let nctx = ectx_extend ctx var Variable arg in
    let body = newMetatype (ectx_to_lctx nctx) (ectx_to_scope_level ctx) tloc in
    let (l, _) = var in
    let arrow = mkArrow (l, kind, var, arg, body) in
    match Unif.unify arrow lxp (ectx_to_lctx ctx) with
    | ((_ck, _ctx, t1, t2)::_)
      -> lexp_error
           (Sexp.location tloc) lxp {|Types:\n    %s\n and:\n    %s\n do not match!|}
           (Lexp.to_string t1) (Lexp.to_string t2);
         (mkDummy_type ctx l, mkDummy_type nctx l)
    | [] -> arg, body

and check (p : sexp) (t : ltype) (ctx : elab_context): lexp =
  fst (elaborate ctx p (Some t))

and unify_or_error lctx lxp ?lxp_name expect actual =
  match Unif.unify expect actual lctx with
   | ((ck, _ctx, t1, t2)::_)
     -> lexp_error
          (Lexp.location lxp) lxp
          ("Type mismatch%s!  Context expected:\n%s"
           ^^ "\nbut %s has type:\n    %s\n"
           ^^ "can't unify:\n    %s\nwith:\n    %s")
          (match ck with
           | Unif.CKimpossible -> ""
           | Unif.CKresidual -> " (residue)")
          (Lexp.to_string expect)
          (Option.value ~default:"expression" lxp_name)
          (Lexp.to_string actual)
          (Lexp.to_string t1)
          (Lexp.to_string t2);
        (* assert (not (OL.conv_p lctx expect actual)) *)
   | [] -> ()

(* This is a crucial function: take an expression `e` of type `inferred_t`
 * and convert it into something of type `t`.  Currently the only conversion
 * we use is to instantiate implicit arguments when needed, but we could/should
 * do lots of other things.  *)
and check_inferred ctx e inferred_t t =
  let (e, inferred_t) =
  match OL.lexp'_whnf t (ectx_to_lctx ctx) with
    | Arrow (_, (Aerasable | Aimplicit), _, _, _)
      -> (e, inferred_t)
    | _ -> instantiate_implicit e inferred_t ctx in
  unify_or_error (ectx_to_lctx ctx) e t inferred_t;
  e

(* Lexp.case can sometimes be inferred, but we prefer to always check.  *)
and check_case rtype (loc, target, ppatterns) ctx =
  (* Helpers *)

  let pat_string p = Sexp.to_string (pexp_u_pat p) in

  let uniqueness_warn pat =
    warning
      ~loc:(pexp_pat_location pat)
      "Pattern %s is a duplicate.  It will override a previous pattern."
      (pat_string pat)
  in

    let check_uniqueness pat name map =
      if SMap.mem name map then uniqueness_warn pat in

    (* get target and its type *)
    let tlxp, tltp = infer target ctx in
    let tknd = OL.get_type (ectx_to_lctx ctx) tltp in
    let tlvl = match OL.lexp'_whnf tknd (ectx_to_lctx ctx) with
      | Sort (_, Stype l) -> l
      | _ -> fatal "Target lexp's kind is not a sort" in
    let it_cs_as = ref None in
    let ltarget = ref tlxp in

    let get_cs_as it' lctor =
      let unify_ind expected actual =
        match Unif.unify actual expected (ectx_to_lctx ctx) with
        | (_::_)
          -> lexp_error
               (Sexp.location loc) lctor {|Expected pattern of type "%s", but got "%s"|}
               (Lexp.to_string expected) (Lexp.to_string actual)
        | [] -> () in
      match !it_cs_as with
      | Some (it, cs, args)
        -> unify_ind it it'; (cs, args)
      | None
        -> match OL.lexp'_whnf it' (ectx_to_lctx ctx) with
          | Inductive (_, _, fargs, constructors)
            -> let (_s, targs) = List.fold_left
                                 (fun (s, targs) (ak, name, t)
                                  -> let arg = newMetavar
                                                (ectx_to_lctx ctx)
                                                (ectx_to_scope_level ctx)
                                                name (mkSusp t s) in
                                    (S.cons arg s, (ak, arg) :: targs))
                                 (S.identity, [])
                                 fargs in
              let (cs, args) = (constructors, List.rev targs) in
              ltarget := check_inferred ctx tlxp tltp (mkCall (loc, it', args));
              it_cs_as := Some (it', cs, args);
              (cs, args)
          | _ -> let call_split e =
                      match OL.lexp'_whnf e (ectx_to_lctx ctx) with
                       | Call (_, f, args) -> (f, args)
                       | _ -> (e,[]) in
                let (it, targs) = call_split tltp in
                unify_ind it it';
                let constructors =
                  match OL.lexp'_whnf it (ectx_to_lctx ctx) with
                  | Inductive (_, _, fargs, constructors)
                    -> assert (List.length fargs = List.length targs);
                       constructors
                  | _
                    -> lexp_error
                         (Sexp.location target) tlxp
                         {|Can't "case" on objects of type "%s"|} (Lexp.to_string tltp);
                       SMap.empty in
                it_cs_as := Some (it, constructors, targs);
                (constructors, targs) in

    (*  Read patterns one by one *)
    let fold_fun (lbranches, dflt) (pat, pexp) =

      let shift_to_extended_ctx nctx lexp =
        mkSusp lexp (S.shift (M.length (ectx_to_lctx nctx)
                              - M.length (ectx_to_lctx ctx))) in

      let ctx_extend_with_eq nctx head_lexp =
        (* Add a proof of equality between the target and the branch
           head to the context *)
        let tlxp' = shift_to_extended_ctx nctx tlxp in
        let tlvl' = shift_to_extended_ctx nctx tlvl in
        let tltp' = shift_to_extended_ctx nctx tltp in
        let tfn' = (mkLambda (Aerasable, (dsinfo, None), type_interval,
                             mkSusp tltp' (S.shift 1))) in
        let eqty = mkCall (loc, DB.type_heq,
                           [(Aerasable, tlvl');    (* Typelevel *)
                            (Aerasable, tfn');     (* Inductive type *)
                            (Anormal, head_lexp);  (* Lexp of the branch head *)
                            (Anormal, tlxp')])     (* Target lexp *)
        in ctx_extend nctx (loc, None) Variable eqty
      in

      let add_default v =
        (if dflt != None then uniqueness_warn pat);
        let nctx = ctx_extend ctx v Variable tltp in
        let head_lexp = mkVar (v, 0) in
        let nctx = ctx_extend_with_eq nctx head_lexp in
        let rtype' = shift_to_extended_ctx nctx rtype in
        let lexp = check pexp rtype' nctx in
        lbranches, Some (v, lexp) in

      let add_branch pctor pargs =
        let loc = Sexp.location pctor in
        let lctor, _ct = infer pctor ctx in
        let rec inst_args ctx e =
        let lxp = OL.lexp_whnf e (ectx_to_lctx ctx) in
         match lexp_lexp' lxp with
          | Lambda (Aerasable, v, t, body)
            -> let arg = newMetavar (ectx_to_lctx ctx) (ectx_to_scope_level ctx)
                          v t in
              let nctx = ctx_extend ctx v Variable t in
              let body = inst_args nctx body in
              mkSusp body (S.substitute arg)
          | _e -> lxp in
        match lexp_lexp' (nosusp (inst_args ctx lctor)) with
        | Cons (it', (_, cons_name))
          -> let _ = check_uniqueness pat cons_name lbranches in
             let (constructors, targs) = get_cs_as it' lctor in
             let cargs =
               try SMap.find cons_name constructors with
               | Not_found
                 -> lexp_error
                      loc lctor {|"%s" does not a have a "%s" constructor|}
                      (Lexp.to_string it') cons_name;
                    [] in

            let subst = List.fold_left (fun s (_, t) -> S.cons t s)
                                        S.identity targs in
            let rec make_nctx ctx   (* elab context.  *)
                              s     (* Pending substitution.  *)
                              pargs (* Pattern arguments.  *)
                              cargs (* Constructor arguments.  *)
                              pe    (* Pending explicit pattern args.  *)
                              acc = (* Accumulated result.  *)
              match (pargs, cargs) with
              | (_, []) when not (SMap.is_empty pe)
                -> let pending = SMap.bindings pe in
                  sexp_error
                    loc {|Explicit pattern args "%s" have no matching fields|}
                    (String.concat ", " (List.map fst pending));
                  make_nctx ctx s pargs cargs SMap.empty acc
              | [], [] -> ctx, List.rev acc
              | (_, _pat)::_, []
                -> lexp_error loc lctor
                             "Too many pattern args to the constructor";
                  make_nctx ctx s [] [] pe acc
              | (_, (ak, (_, Some fname), fty)::cargs)
                   when SMap.mem fname pe
                -> let var = SMap.find fname pe in
                  let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                  make_nctx nctx (ssink var s) pargs cargs
                            (SMap.remove fname pe)
                            ((ak, var)::acc)
              | ((ef, var)::pargs, (ak, _, fty)::cargs)
                   when (match (ef, ak) with
                         | (Some (_, "_"), _) | (None, Anormal) -> true
                         | _ -> false)
                -> let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                  make_nctx nctx (ssink var s) pargs cargs pe
                            ((ak, var)::acc)
              | ((Some (l, fname), var)::pargs, cargs)
                -> if SMap.mem fname pe then
                     sexp_error l {|Duplicate explicit field "%s"|} fname;
                   make_nctx ctx s pargs cargs (SMap.add fname var pe) acc
              | pargs, (ak, fname, fty)::cargs
                -> let var = ((Symbol(loc, "")), None) in
                   let nctx = ctx_extend ctx var Variable (mkSusp fty s) in
                   if ak = Anormal then
                     sexp_error
                       loc {|Missing pattern for normal field%s|}
                       (match fname with
                        | (_, Some n) -> sprintf {| "%s"|} n
                        | _ -> "");
                   make_nctx nctx (ssink var s) pargs cargs pe
                             ((ak, var)::acc) in
            let nctx, fargs = make_nctx ctx subst pargs cargs SMap.empty [] in
            let head_lexp_ctor =
              shift_to_extended_ctx nctx
                (mkCall (dsinfo, lctor, List.map (fun (_, a) -> (Aerasable, a)) targs)) in
            let head_lexp_args =
              List.mapi (fun i (ak, vname) ->
                  (ak, mkVar (vname, List.length fargs - i - 1))) fargs in
            let head_lexp = mkCall (dsinfo, head_lexp_ctor, head_lexp_args) in
            let nctx = ctx_extend_with_eq nctx head_lexp in
            let rtype' = shift_to_extended_ctx nctx rtype in
            let lexp = check pexp rtype' nctx in
            SMap.add cons_name (Symbol(loc, ""), fargs, lexp) lbranches,
            dflt
        (* FIXME: If `ct` is Special-Form or Macro, pass pargs to it
         * and try again with the result.  *)
        | _ -> lexp_error loc lctor "Not a constructor"; lbranches, dflt
      in

      match pat with
      | Ppatsym ((_, None) as var) -> add_default var
      | Ppatsym ((l, Some name) as var)
        -> if Eval.constructor_p name ctx then
            add_branch (Symbol (Sexp.location l, name)) []
          else add_default var (* A named default branch.  *)

      | Ppatcons (_, pctor, pargs) -> add_branch pctor pargs in

    let (lpattern, dflt) =
        List.fold_left fold_fun (SMap.empty, None) ppatterns in

    mkCase (loc, tlxp, rtype, lpattern, dflt)

and elab_macro_call
      (ctx : elab_context)
      (func : lexp)
      (args : token list)
      (ot : ltype option)
  : lexp * sform_type =

  let location = lexp_sinfo func in
  let t =
    match ot with
    | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx) location
    | Some t -> t
  in

  if !macro_tracing_enabled
  then
    (trace_macro ~location:(Sexp.location location) {|expanding: %s|} (Lexp.to_string func);
     List.iteri
       (fun i arg ->
         arg |> Sexp.to_string |> trace_macro ~location:(Sexp.location location) {|input %d: %s|} i)
       args);

  let sxp = lexp_expand_macro location func args ctx (Some t) in
  if !macro_tracing_enabled
  then trace_macro ~location:(Sexp.location location) {|output: %s|} (Sexp.to_string sxp);

  elaborate ctx sxp ot

(*  Identify Call Type and return processed call.  *)
and elab_call ctx (func, ltp) (sargs: sexp list) =
  let loc = lexp_sinfo func in

  let rec handle_fun_args largs sargs pending ltp =
    let ltp' = OL.lexp_whnf ltp (ectx_to_lctx ctx) in
    match sargs, lexp_lexp' ltp' with
    | _, Arrow (_, ak, (_, Some aname), arg_type, ret_type)
         when SMap.mem aname pending
      -> let sarg = SMap.find aname pending in
        let larg = check sarg arg_type ctx in
        handle_fun_args ((ak, larg) :: largs) sargs
                        (SMap.remove aname pending)
                        (Lexp.mkSusp ret_type (S.substitute larg))

    | Node (_, Symbol (_, "_:=_"), [Symbol (_, aname); sarg]) :: sargs,
      Arrow (_, ak, _, arg_type, ret_type)
         when (aname = "_")
                (* Explicit-implicit argument.  *)
      -> let larg = check sarg arg_type ctx in
        handle_fun_args ((ak, larg) :: largs) sargs pending
                        (Lexp.mkSusp ret_type (S.substitute larg))

    | Node (_, Symbol (_, "_:=_"), [Symbol (l, aname); sarg]) :: sargs,
      Arrow _
      -> if SMap.mem aname pending then
           sexp_error l {|Duplicate explicit arg "%s"|} aname;
         handle_fun_args largs sargs (SMap.add aname sarg pending) ltp

    | Node (_, Symbol (_, "_:=_"), Symbol (l, aname) :: _) :: sargs, _
      -> sexp_error
           l {|Explicit arg "%s" to non-function (type = %s)|}
           aname (Lexp.to_string ltp);
         handle_fun_args largs sargs pending ltp

    (* Aerasable *)
    | _, Arrow (_, ((Aerasable | Aimplicit) as ak), (_l,v), arg_type, ret_type)
         (* Don't instantiate after the last explicit arg: the rest is done,
          * when needed in infer_and_check (via instantiate_implicit).  *)
         when not (sargs = [] && SMap.is_empty pending)
      -> let arg_loc = try (List.hd sargs) with _ -> loc in
         let larg = newInstanceMetavar ctx (arg_loc, v) arg_type in
         handle_fun_args ((ak, larg) :: largs) sargs pending
           (Lexp.mkSusp ret_type (S.substitute larg))
    | [], _
      -> (if not (SMap.is_empty pending) then
            let pending = SMap.bindings pending in
            let loc = match pending with
              | (_, sarg)::_ -> Sexp.location sarg
              | _ -> assert false in
            lexp_error
              loc func {|Explicit actual args "%s" have no matching formal args|}
              (String.concat ", " (List.map fst pending)));
         largs, ltp

    | sarg :: sargs, _
      -> let (arg_type, ret_type) = match lexp_lexp' ltp' with
          | Arrow (_, ak, _, arg_type, ret_type)
            -> assert (ak = Anormal); (arg_type, ret_type)
          | _ -> unify_with_arrow ctx (sarg)
                                 ltp' Anormal (sarg, None) None in
        let larg = check sarg arg_type ctx in
        handle_fun_args ((Anormal, larg) :: largs) sargs pending
                        (Lexp.mkSusp ret_type (S.substitute larg)) in

  let (largs, ret_type) = handle_fun_args [] sargs SMap.empty ltp in
  (mkCall (loc, func, List.rev largs), Inferred ret_type)

(*  Parse inductive type definition.  *)
and lexp_parse_inductive ctors ctx =

  let make_args (args:(arg_kind * vname * sexp) list) ctx
      : (arg_kind * vname * ltype) list =
    let nctx = ectx_new_scope ctx in
    let rec loop args acc ctx =
          match args with
          | [] -> let acc = List.rev acc in
                 (* Convert the list of fields into a Lexp expression.
                  * The actual expression doesn't matter, as long as its
                  * scoping is right: we only use it so we can pass it to
                  * things like `fv` and `meta_to_var`.  *)
                 let altacc = List.fold_right
                                (fun (ak, n, t) aa
                                 -> mkArrow (dsinfo, ak, n, t, aa))
                                acc impossible in
                 let g = resolve_instances_and_generalize nctx altacc in
                 let altacc' = g (fun _ne vname t _l e
                                  -> mkArrow (lexp_sinfo e, Aerasable, vname, t, e))
                                 altacc in
                 if altacc' == altacc
                 then acc       (* No generalization!  *)
                 else
                   (* Convert the Lexp back into a list of fields.  *)
                   let rec loop e =
                    match lexp_lexp' e with
                     | Arrow (_, ak, n, t, e) -> (ak, n, t)::(loop e)
                     | _ -> assert (e = impossible); [] in
                   loop altacc'
          | (kind, var, exp)::tl
            -> let lxp = infer_type exp ctx var in
              let nctx = ectx_extend ctx var Variable lxp in
              loop tl ((kind, var, lxp)::acc) nctx in
    loop args [] nctx in

  List.fold_left
    (fun lctors ((_, name), args) ->
      SMap.add name (make_args args ctx) lctors)
    SMap.empty ctors

and track_fv rctx lctx e =
  let (fvs, (mvs, _)) = OL.fv e in
  let nc = EV.not_closed rctx fvs in
  if nc = [] && not (IMap.is_empty mvs) then
    "metavars"
  else if nc = [] then
    "a bug"
  else let tfv i =
         let name = match Myers.nth i rctx with
           | ((_, Some n),_) -> n
           | _ -> "<anon>" in
         match Myers.nth i lctx with
         | (_, LetDef (o, e), _)
           -> let drop = i + 1 - o in
             if drop <= 0 then
               "somevars[" ^ string_of_int i ^ "-" ^ string_of_int o ^ "]"
             else
               name ^ " ("
               ^ track_fv (Myers.nthcdr drop rctx)
                          (Myers.nthcdr drop lctx)
                          e
               ^ ")"
         | _ -> name
       in String.concat " " (List.map tfv nc)

and lexp_eval ectx e =
  let ee = OL.erase_type (ectx_to_lctx ectx) e in
  let rctx = EV.from_ectx ectx in

  if not (EV.closed_p rctx (OL.fv e)) then
    lexp_error
      (Lexp.location e) e {|Expression "%s" is not closed: %s|}
      (Lexp.to_string e) (track_fv rctx (ectx_to_lctx ectx) e);

  try EV.eval ee rctx
  with exc ->
    let eval_trace = fst (EV.get_trace ()) in
    info ~print_action:(fun _ -> EV.print_eval_trace (Some eval_trace))
      "Exception happened during evaluation:";
    raise exc

and lexp_expand_macro loc macro_funct sargs ctx (_ot : ltype option)
    : sexp =

  (* Build the function to be called *)
  let macro_expand = BI.get_predef "Macro_expand" ctx in
  (* FIXME: Rather than remember the lexp of "expand_macro" in predef,
   * we should remember its value so we don't have to re-eval it everytime.  *)
  let macro_expand = lexp_eval ctx macro_expand in
  (* FIXME: provide `ot` (the optional expected type) for non-decl macros.  *)
  let macro = lexp_eval ctx macro_funct in
  let args = [macro; BI.o2v_list sargs] in

  (* FIXME: Make a proper `Var`.  *)
  let value = EV.eval_call (Sexp.location loc) (EL.Var ((dsinfo, Some "expand_macro"), 0))
                ([], []) macro_expand args in
  match value with
  | Vcommand cmd
    -> (match cmd () with
        | Vsexp sxp -> sxp
        | v -> value_fatal (Sexp.location loc) v "Macro `%s` should return an IO sexp"
                 (Lexp.to_string macro_funct))
  | v -> value_fatal (Sexp.location loc) v "Macro `%s` should return an IO"
           (Lexp.to_string macro_funct)


(* Print each generated decls *)
(* and sexp_decls_macro_print sxp_decls =
 *   match sxp_decls with
 *     | Node (Symbol (_, "_;_"), decls) ->
 *       List.iter (fun sxp -> sexp_decls_macro_print sxp) decls
 *     | e -> sexp_print e; print_string "\n" *)

(* Elaborate a bunch of mutually-recursive definitions.
 * FIXME: Currently, we never apply generalization to recursive definitions,
 * which can leave "bogus" metavariables in the term (and  it also means we
 * can't define `List_length` or `List_map` without adding explicit type
 * annotations :-( ).  *)
and lexp_check_decls (ectx : elab_context) (* External context.  *)
                     (nctx : elab_context) (* Context with type declarations. *)
                     (defs : (symbol * sexp) list)
    : (vname * lexp * ltype) list * elab_context =
  (* FIXME: Generalize when/where possible, so things like `map` can be
     defined without type annotations!  *)
  (* Preserve the new operators added to nctx.  *)
  let (declmap)
    = List.fold_right
                  (fun ((_l, vname), sexp) (map) ->
                    let i = senv_lookup vname nctx in
                    assert (i < List.length defs);
                    match Myers.nth i (ectx_to_lctx nctx) with
                    | (_v', ForwardRef, t)
                      -> let adjusted_t = push_susp t (S.shift (i + 1)) in
                        (* We elab `sexp` within the original `nctx`, i.e.
                         * the context where the other vars don't have
                         * a definition yet.  This is because we can't yet
                         * build the proper `nctx`: a partially filled
                         * mutually-recursive block would not have the
                         * proper format, e.g. for `lctx_view`!  *)
                        let e = check sexp adjusted_t nctx in
                        (* let d = (v', LetDef (i + 1, e), t) in *)
                        (IMap.add i ((sexp, Some vname), e, t) map)
                    | _ -> Log.internal_error "Defining same slot!")
                  defs (IMap.empty) in
  let decls = List.rev (List.map (fun (_, d) -> d) (IMap.bindings declmap)) in
  List.iter (fun (_,e,_) -> resolve_instances e) decls;
  let ectx = ctx_define_rec ectx decls |> ectx_set_grammar (ectx_get_grammar nctx)
                                       |> ectx_set_tcctx (ectx_to_tcctx nctx) in
  decls, ectx

and infer_and_generalize_type (ctx : elab_context) se name =
  let nctx = ectx_new_scope ctx in
  let t = infer_type se nctx name in
  (* We should not generalize over metavars which only occur on the rightmost
   * side of arrows in type annotations (aka declarations), since there's no
   * way for the callee to return something of the proper type if those
   * metavars don't also occur somewhere in the arguments.
   * E.g. for annotations like
   *
   *     x : ?;
   *     F : Type ? → Type ?;
   *
   * it makes no sense to generalize them to:
   *
   *     x : (ℓ : TypeLevel) ≡> (t : Type ℓ) ≡> t;
   *     F : (ℓ₁ : TypeLevel) ≡> (ℓ₂ : TypeLevel) ≡> Type ℓ₁ → Type ℓ₂;
   *
   * since there can't be corresponding definitions.  So replace the final
   * return type with some arbitrary closed constant before computing the
   * set of free metavars.
   * The same argument holds for other *positive* positions, e.g.
   *
   *     f : (? -> Int) -> Int;
   *
   * But we don't bother trying to catch all cases currently.
   *)
  let rec strip_rettype t =
    match lexp_lexp' t with
    | Arrow (l, ak, v, t1, t2)
     -> mkArrow (l, ak, v, t1, strip_rettype t2)
    | Sort _ | Metavar _ -> type0 (* Abritrary closed constant.  *)
    | _ -> t in
  let g = resolve_instances_and_generalize nctx (strip_rettype t) in
  g (fun _ne name t l e
     -> mkArrow (Symbol(l,""), Aerasable, name, t, e))
    t

and infer_and_generalize_def (ctx : elab_context) se =
  let nctx = ectx_new_scope ctx in
  let (e,t) = infer se nctx in
  let g = resolve_instances_and_generalize nctx e in
  let e' = g wrapLambda e in
  let t' = g (fun ne name t _l e
              -> mkArrow (se, (if ne then Aimplicit else Aerasable),
                         name, t, e))
             t in
  (e', t')

and lexp_decls_1
      (sdecls : sexp list)                        (* What's already parsed *)
      (tokens : token list)                       (* Rest of input *)
      (ectx : elab_context)                       (* External ctx.  *)
      (nctx : elab_context)                       (* New context.  *)
      (pending_decls : location SMap.t)           (* Pending type decls. *)
      (pending_defs : (symbol * sexp) list)       (* Pending definitions. *)
    : (vname * lexp * ltype) list * sexp list * token list * elab_context =

  let rec lexp_decls_1 sdecls tokens nctx pending_decls pending_defs =
    let sdecl, sdecls, toks =
      match (sdecls, tokens) with
      | (s :: sdecls, _) -> Some s, sdecls, tokens
      | ([], []) -> None, [], []
      | ([], _) ->
         let (s, toks) = sexp_parse_all (ectx_get_grammar nctx)
                           tokens (Some ";") in
         Some s, [], toks in
    let recur prepend_sdecls nctx pending_decls pending_defs =
      lexp_decls_1 (List.append prepend_sdecls sdecls)
        toks nctx pending_decls pending_defs in
    match sdecl with
    | None
      -> if not (SMap.is_empty pending_decls) then
           let (s, loc) = SMap.choose pending_decls in
           error ~loc {|Variable "%s" declared but not defined!|} s
         else
           assert (pending_defs == []);
         [], [], [], nctx

    | Some (Symbol (_, ""))
      -> recur [] nctx pending_decls pending_defs

    | Some (Node (_, Symbol (_, ("_;_" (* | "_;" | ";_" *))), sdecls'))
      -> recur sdecls' nctx pending_decls pending_defs

    | Some (Node (_, Symbol (loc, "_:_"), args) as thesexp)
      (* FIXME: Move this to a "special form"!  *)
      -> (match args with
          | [Symbol (loc, vname); stp]
            -> let ltp = infer_and_generalize_type nctx stp
                          (stp, Some vname) in
               if SMap.mem vname pending_decls then
                 (* Don't burp: take'em all and unify!  *)
                 let pt_idx = senv_lookup vname nctx in
                 (* Take the previous type annotation.  *)
                 let pt = match Myers.nth pt_idx (ectx_to_lctx nctx) with
                   | (_, ForwardRef, t) -> push_susp t (S.shift (pt_idx + 1))
                   | _ -> Log.internal_error "Var not found at its index!" in
                 (* Unify it with the new one.  *)
                 let _ = match Unif.unify ltp pt (ectx_to_lctx nctx) with
                   | (_::_)
                     -> lexp_error
                          loc ltp
                          {|New type annotation "%s" incompatible with
                           previous "%s"|}
                          (Lexp.to_string ltp) (Lexp.to_string pt)
                   | [] -> () in
                 recur [] nctx pending_decls pending_defs
               else if List.exists (fun ((_, vname'), _) -> vname = vname')
                         pending_defs then
                 (error ~loc {|Variable "%s" already defined!|} vname;
                  recur [] nctx pending_decls pending_defs)
               else recur [] (ectx_extend nctx (stp, Some vname)
                              ForwardRef ltp)
                      (SMap.add vname loc pending_decls)
                      pending_defs
          | _
            -> error
                 ~loc {|Invalid type declaration syntax : "%s"|}
                 (Sexp.to_string thesexp);
               recur [] nctx pending_decls pending_defs)

    | Some (Node (_, (Symbol (loc, "_=_") as head), args) as thesexp)
      (* FIXME: Move this to a "special form"!  *)
      -> (match args with
          | [Symbol ((_l, vname)); sexp]
               when SMap.is_empty pending_decls
            -> assert (pending_defs == []);
               (* Used to be true before we added define-operator.  *)
               (* assert (ectx == nctx); *)
               let (lexp, ltp) = infer_and_generalize_def nctx sexp in
               let var = (thesexp, Some vname) in
               (* Lexp decls are always recursive, so we have to shift by 1 to
                * account for the extra var (ourselves).  *)
               [(var, mkSusp lexp (S.shift 1), ltp)], sdecls, toks,
               ctx_define nctx var lexp ltp

          | [Symbol (loc, vname); sexp]
            -> if SMap.mem vname pending_decls then
                 let pending_decls = SMap.remove vname pending_decls in
                 let pending_defs = (((loc, vname), sexp) :: pending_defs) in
                 if SMap.is_empty pending_decls then
                   let nctx = ectx_new_scope nctx in
                   let decls, nctx = lexp_check_decls ectx nctx pending_defs in
                   decls, sdecls, toks, nctx
                 else
                   recur [] nctx pending_decls pending_defs

               else
                 (error ~loc {|"%s" defined but not declared!|} vname;
                  recur [] nctx pending_decls pending_defs)

          | [Node (location, Symbol s, args) as d; body]
            -> (* FIXME: Make it a macro (and don't hardcode `lambda_->_`)!  *)
             recur [Node (location,
                          head,
                          [Symbol s;
                           Node (location,
                                 Symbol (Sexp.location d, "lambda_->_"),
                                 [sexp_u_list args location; body])])]
               nctx pending_decls pending_defs

          | _
            -> error
                 ~loc {|Invalid definition syntax : "%s"|} (Sexp.to_string thesexp);
               recur [] nctx pending_decls pending_defs)

    | Some sexp
      -> (* sdforms or macros *)
       let sxp, sargs = match sexp with
         | Node (_, sxp, sargs) -> sxp, sargs
         | _ -> sexp, [] in
       let lctx = ectx_to_lctx nctx in
       let (f, t) = infer sxp nctx in
       if (OL.conv_p lctx t type_special_decl_form) then
         recur [] (elab_special_decl_form nctx f sargs) pending_decls pending_defs
       else if (OL.conv_p lctx t (BI.get_predef "Macro" nctx)) then
         (* expand macro and get the generated declarations *)
         let lxp, _ltp = infer sxp nctx in
         let sdecl' = lexp_expand_macro sxp lxp sargs nctx None in
         recur [sdecl'] nctx pending_decls pending_defs
       else (
         error ~loc:(Sexp.location sxp) "Invalid declaration syntax";
         recur [] nctx pending_decls pending_defs
       )

  in (EV.set_getenv nctx;
      let res = lexp_decls_1 sdecls tokens nctx
                  pending_decls pending_defs in
        (Log.stop_on_error (); res))

(* Why is the return value a list of list?
   - each `(vname * lexp * ltype)` is a definition
   - each `(vname * lexp * ltype) list` is a list of definitions which can refer
     to each other (i.e. they can be mutually recursive).
   - hence the overall "list of lists" is a sequence of such blocs of
     mutually-recursive definitions. *)
and lexp_p_decls (sdecls : sexp list) (tokens : token list) (ctx : elab_context)
    : ((vname * lexp * ltype) list list * elab_context) =
  let rec impl sdecls tokens ctx =
    match (sdecls, tokens) with
    | ([], []) -> [], ectx_new_scope ctx
    | _ ->
       let decls, sdecls, tokens, nctx =
         lexp_decls_1 sdecls tokens ctx ctx SMap.empty [] in
       Log.stop_on_error ();
       let declss, nnctx = impl sdecls tokens nctx in
       decls :: declss, nnctx in
  impl sdecls tokens ctx

and lexp_parse_all (p: sexp list) (ctx: elab_context) : lexp list =
  let res = List.map (fun pe -> let e, _ = infer pe ctx in e) p in
    (Log.stop_on_error (); res)

and lexp_parse_sexp (ctx: elab_context) (e : sexp) : lexp =
  let e, _ = infer e ctx in (Log.stop_on_error (); e)

(* --------------------------------------------------------------------------
 *  Special forms implementation
 * -------------------------------------------------------------------------- *)

and sform_declexpr ctx loc sargs _ot =
  match List.map (lexp_parse_sexp ctx) sargs with
  | [e] when is_var e
    -> (match DB.env_lookup_expr ctx ((loc, U.get_vname_name_option (get_var_vname (get_var e))), get_var_db_index (get_var e)) with
        | Some lxp -> (lxp, Lazy)
        | None -> error ~loc:(Sexp.location loc) "no expr available";
                  sform_dummy_ret ctx loc)
  | _ -> error ~loc:(Sexp.location loc) "declexpr expects one argument";
         sform_dummy_ret ctx loc


let sform_decltype ctx loc sargs _ot =
  match List.map (lexp_parse_sexp ctx) sargs with
  | [e] when is_var e
    -> (DB.env_lookup_type ctx ((loc, U.get_vname_name_option (get_var_vname (get_var e))), get_var_db_index (get_var e)), Lazy)
  | _ -> error ~loc:(Sexp.location loc) "decltype expects one argument";
         sform_dummy_ret ctx loc



let builtin_value_types : ltype option SMap.t ref = ref SMap.empty

let sform_built_in ctx loc sargs ot =
  match !parsing_internals, sargs with
  | true, [String (_, name)]
    -> (match ot with
       | Some ltp
         (* FIXME: This `L.clean` is basically the last remaining use of the
          * function.  It's not indispensible, tho it might still be useful for
          * performance of type-inference (at least until we have proper
          * memoization of push_susp and/or whnf).  *)
         -> let ltp' = Lexp.clean (OL.lexp_close (ectx_to_lctx ctx) ltp) in
            let bi = mkBuiltin ((Sexp.location loc, name), ltp') in
            if not (SMap.mem name (!EV.builtin_functions)
                  || List.mem name DB.builtin_axioms) then
              sexp_error (Sexp.location loc) {|Unknown built-in "%s"|} name;
            OL.add_builtin_cst name bi;
            (bi, Checked)
       | None -> error ~loc:(Sexp.location loc) "Built-in's type not provided by context!";
                sform_dummy_ret ctx loc)

  | true, _ -> error ~loc:(Sexp.location loc) "Wrong Usage of `Built-in`";
              sform_dummy_ret ctx loc

  | false, _ -> error ~loc:(Sexp.location loc) "Use of `Built-in` in user code";
               sform_dummy_ret ctx loc

let sform_datacons ctx loc sargs _ot =
  match sargs with
  | [t; Symbol ((_sloc, _cname) as sym)]
    -> let idt, _ = infer t ctx in
      (mkCons (idt, sym), Lazy)

  | [_;_] -> sexp_error (Sexp.location loc) "Second arg of ##constr should be a symbol";
            sform_dummy_ret ctx loc
  | _ -> sexp_error (Sexp.location loc) "##constr requires two arguments";
        sform_dummy_ret ctx loc

let elab_colon_to_ak k = match k with
  | "_:::_" -> Aerasable
  | "_::_" -> Aimplicit
  | _ -> Anormal

let elab_datacons_arg s = match s with
  | Node (_, Symbol (_, (("_:::_" | "_::_" | "_:_") as k)), [Symbol s; t])
    -> (elab_colon_to_ak k, elab_p_id s, t)
  | _ -> (Anormal, (s, None), s)

let elab_typecons_arg arg : (arg_kind * vname * sexp option) =
  match arg with
  | Node (_, Symbol (_, (("_:::_" | "_::_" | "_:_") as k)), [Symbol (_l,name); e])
    -> (elab_colon_to_ak k,
       (arg, Some name), Some e)
  | Symbol (_l, name) -> (Anormal, (arg, Some name), None)
  | _ -> sexp_error ~print_action:(fun _ -> Sexp.print arg; print_newline ())
           (Sexp.location arg)
           "Unrecognized formal arg";
         (Anormal, (arg, None), None)

let sform_typecons ctx loc sargs _ot =
  match sargs with
  | [] -> sexp_error (Sexp.location loc) "No arg to ##typecons!"; (mkDummy_type ctx loc, Lazy)
  | formals :: constrs
    -> let (label, formals) = match formals with
        | Node (_, label, formals) -> (label, formals)
        | _ -> (formals, []) in
      let label = match label with
        | Symbol label -> label
        | _ -> let loc = Sexp.location label in
              sexp_error loc "Unrecognized inductive type name";
              (loc, "<error>") in

      let rec parse_formals sformals rformals ctx = match sformals with
        | [] -> (List.rev rformals, ctx)
        | sformal :: sformals
          -> let (kind, var, opxp) = elab_typecons_arg sformal in
            let ltp = match opxp with
              | Some pxp -> let (l,_) = infer pxp ctx in l
              | None -> let (l,_) = var in
                       newMetatype (ectx_to_lctx ctx)
                                   (ectx_to_scope_level ctx) l in

            parse_formals sformals ((kind, var, ltp) :: rformals)
                          (ectx_extend ctx var Variable ltp) in

      let (formals, nctx) = parse_formals formals [] ctx in

      let ctors
        = List.fold_right
          (fun case pcases
           -> match case with
             (* read Constructor name + args => Type ((Symbol * args) list) *)
             | Node (_, Symbol s, cases)
               -> (s, List.map elab_datacons_arg cases)::pcases
             (* This is a constructor with no args *)
             | Symbol s -> (s, [])::pcases

             | _ -> sexp_error (Sexp.location case)
                              "Unrecognized constructor declaration";
                   pcases)
          constrs [] in

      let map_ctor = lexp_parse_inductive ctors nctx in
      (mkInductive (loc, label, formals, map_ctor), Lazy)

let sform_hastype ctx loc sargs _ot =
  match sargs with
  | [se; st] -> let lt = infer_type st ctx (loc, None) in
               let le = check se lt ctx in
               (le, Inferred lt)
  | _ -> sexp_error (Sexp.location loc) "##_:_ takes two arguments";
        sform_dummy_ret ctx loc

let sform_arrow kind ctx loc sargs _ot =
  match sargs with
  | [st1; st2]
    -> let (v, st1) = match st1 with
        | Node (_, Symbol (_, "_:_"), [Symbol v; st1]) -> (elab_p_id v, st1)
        | _ -> ((st1, None), st1) in
      let lt1 = infer_type st1 ctx v in
      let nctx = ectx_extend ctx v Variable lt1 in
      let lt2 = infer_type st2 nctx (st2, None) in
      (mkArrow (loc, kind, v, lt1, lt2), Lazy)
  | _ -> sexp_error (Sexp.location loc) "##_->_ takes two arguments";
        sform_dummy_ret ctx loc

let rec sform_lambda kind ctx loc sargs ot =
  match sargs with
  | [sarg; sbody]
    -> let (arg, ost1) = match sarg with
         | Node (_, Symbol (_, "_:_"), [Symbol arg; st]) -> (elab_p_id arg, Some st)
         | Symbol arg -> (elab_p_id arg, None)
         | _ -> sexp_error (Sexp.location sarg)
                           "Unrecognized lambda argument";
                ((dsinfo, None), None) in

       let olt1 = match ost1 with
         | Some st -> Some (infer_type st ctx arg)
         | _ -> None in

       let mklam lt1 olt2 =
         let nctx = ectx_extend ctx arg Variable lt1 in
         let olt2 = match olt2 with
           | None -> None
           (* Apply a "dummy" substitution which replace #0 with #0
            * in order to account for the fact that `arg` and `v`
            * might not be the same name!  *)
           | Some lt2
             -> Some (srename arg lt2) in
         let (lbody, alt) = elaborate nctx sbody olt2 in
         (mkLambda (kind, arg, lt1, lbody),
          match alt with
          | Inferred lt2 -> Inferred (mkArrow (loc, kind, arg, lt1, lt2))
          | _ -> alt) in

       (match ot with
        | None -> mklam (match olt1 with
                        | Some lt1 -> lt1
                        | None -> newMetatype (ectx_to_lctx ctx)
                                             (ectx_to_scope_level ctx) loc)
                       None
        (* Read var type from the provided type *)
        | Some t
          -> let lp = OL.lexp_whnf t (ectx_to_lctx ctx) in
            match lexp_lexp' lp with
            | Arrow (_, ak2, _, lt1, lt2) when ak2 = kind
              -> (match olt1 with
                 | None -> ()
                 | Some lt1'
                   -> unify_or_error (ectx_to_lctx ctx) lt1'
                        ~lxp_name:"parameter" lt1 lt1');
                mklam lt1 (Some lt2)

            | Arrow (_, ak2, v, lt1, lt2) when kind = Anormal
              (* `t` is an implicit arrow and `kind` is Anormal,
               * so auto-add a corresponding Lambda wrapper!
               * FIXME: This should be moved to a macro.  *)
              -> (* FIXME: Here we end up adding a local variable `v` whose
                 * name is not lexically present, so there's a risk of
                 * name capture.  We should make those vars anonymous?  *)
                let nctx = ectx_extend ctx v Variable lt1 in
                (* FIXME: Don't go back to sform_lambda, but use an internal
                 * loop to avoid re-computing olt1 each time.  *)
                let (lam, alt) = sform_lambda kind nctx loc sargs (Some lt2) in
                (mkLambda (ak2, v, lt1, lam),
                 match alt with
                 | Inferred lt2' -> Inferred (mkArrow (loc, ak2, v, lt1, lt2'))
                 | _ -> alt)

            | _lt
              -> let (lt1, lt2) = unify_with_arrow ctx loc lp kind arg olt1
                in mklam lt1 (Some lt2))

  | _
    -> sexp_error
         (Sexp.location loc) "##lambda_%s_ takes two arguments"
         (match kind with
          | Anormal -> "->"
          | Aimplicit -> "=>"
          | Aerasable -> "≡>");
       sform_dummy_ret ctx loc

let rec sform_case ctx sinfo sargs ot = match sargs with
  | [Node (_, Symbol (_, "_|_"), se :: scases)]
    -> let parse_case branch = match branch with
        | Node (_, Symbol (_, "_=>_"), [pat; code])
          -> (pexp_p_pat pat, code)
        | _ -> let l = (Sexp.location branch) in
              sexp_error l "Unrecognized simple case branch";
              (Ppatsym (se, None), Symbol (l, "?")) in
      let pcases = List.map parse_case scases in
      let t = match ot with
        | Some t -> t
        | None -> newMetatype (ectx_to_lctx ctx) (ectx_to_scope_level ctx) sinfo in
      let le = check_case t (sinfo, se, pcases) ctx in
      (le, match ot with Some _ -> Checked | None -> Inferred t)

  (* In case there are no branches, pretend there was a | anyway.  *)
  | [_e]
    -> let loc = Sexp.location sinfo in
       sform_case ctx sinfo [Node (loc, Symbol (loc, "_|_"), sargs)] ot
  | _
    -> sexp_error (Sexp.location sinfo) "Unrecognized case expression";
       sform_dummy_ret ctx sinfo

let sform_letin ctx loc sargs ot = match sargs with
  | [sdecls; sbody]
    -> let declss, nctx = lexp_p_decls [sdecls] [] ctx in
       let s, off =
         List.fold_left (fun (s, off) decls ->
             (OL.lexp_defs_subst loc s decls, off + List.length decls))
           (S.identity, 0) declss in
       let ot = Option.map (fun t -> mkSusp t (S.shift off)) ot in
       let bdy, ot = elaborate nctx sbody ot in
       let ot = match ot with
         | Inferred t -> Inferred (mkSusp t s)
         | _ -> ot in
      (lexp_let_decls declss bdy nctx, ot)
  | _ -> sexp_error (Sexp.location loc) "Unrecognized let_in_ expression";
        sform_dummy_ret ctx loc

let sform_proj ctx loc sargs _ =
  match sargs with
  | [lxp;(Symbol (loca,str))]
    -> let (ret,_) = infer lxp ctx in
       let e = mkProj (loc,ret,(loca,str)) in
       (e, Lazy)
  | [_;_]
    -> sexp_error (Sexp.location loc) "Second argument is not a Symbol!";
      sform_dummy_ret ctx loc
  | _ -> sexp_error (Sexp.location loc) "Wrong arg number!";
         sform_dummy_ret ctx loc

let rec infer_level ctx se : lexp =
  match se with
  | Symbol (_, "z") -> mkSortLevel SLz
  | Symbol _ -> check se type_level ctx
  | Node (_, Symbol (_, "s"), [se])
    -> mkSortLevel (SLsucc (infer_level ctx se))
  | Node (_, Symbol (_, "_∪_"), [se1; se2])
    -> OL.mkSLlub (ectx_to_lctx ctx) (infer_level ctx se1) (infer_level ctx se2)
  | _ -> let l = (Sexp.location se) in
        (sexp_error l "Unrecognized TypeLevel: %s" (Sexp.to_string se);
         newMetalevel (ectx_to_lctx ctx) (ectx_to_scope_level ctx) se)

(* Actually `Type_` could also be defined as a plain constant
 *     Lambda("l", TypeLevel, Sort (Stype (Var "l")))
 * But it would be less efficient (such a lambda can't be passed as argument
 * so it really can only be used applied to something, so it always generates
 * β-redexes.  Furthermore, I'm not sure if my PTS definition is correct to
 * allow such a lambda.  *)
let sform_type ctx loc sargs _ot =
  match sargs with
  | [se] -> let l = infer_level ctx se in
           (mkSort (loc, Stype l),
            Inferred (mkSort (loc, Stype (mkSortLevel (mkSLsucc l)))))
  | _ -> (sexp_error (Sexp.location loc) "##Type_ expects one argument";
         sform_dummy_ret ctx loc)

let sform_debruijn ctx loc sargs _ot =
  match sargs with
  | [Integer (l,i)]
    -> let i = Z.to_int i in if i < 0 || i > get_size ctx then
        (sexp_error l "##DeBruijn index out of bounds";
         sform_dummy_ret ctx loc)
      else
        let lxp = mkVar ((loc, None), i) in (lxp, Lazy)
  | _ -> (sexp_error (Sexp.location loc) "##DeBruijn expects one integer argument";
         sform_dummy_ret ctx loc)

(*  Only print var info *)
let lexp_print_var_info ctx =
    let ((_m, _), env, _) = ctx in
    let n = Myers.length env in

    for i = 0 to n - 1 do (
        let (_, (_, name), exp, tp) = Myers.nth i env in
        print_int i; print_string " ";
        print_string name; (*   name must match *)
        print_string " = ";
         (match exp with
             | None -> print_string "<var>"
             | Some exp -> Lexp.print exp);
        print_string ": ";
        Lexp.print tp;
        print_string "\n")
    done

let in_pervasive = ref true

(*  arguments :
      elab_context from where load is called,
      loc is location of load call,
      sargs should be an array of one file name,
      ot is the expected type of output tuple.
*)
let sform_load usr_elctx loc sargs _ot =

  let read_file file_name elctx =
    let source = Source.of_path file_name in
    let pres =
      try prelex source with
      | Sys_error _
        -> error ~loc:(Sexp.location  loc) {|Could not load "%s": file not found.|} file_name; []
    in
    let sxps = lex default_stt pres in
    let _, elctx = lexp_p_decls [] sxps elctx
    in elctx in

  (* read file as elab_context *)
  let ld_elctx = match sargs with
    | [String (_,file_name)] -> if !in_pervasive then
        read_file file_name usr_elctx
      else
        read_file file_name !sform_default_ectx
    | _ -> (error ~loc:(Sexp.location loc) "argument to load should be one file name (String)";
            !sform_default_ectx) in

  (* get lexp_context *)
  let usr_lctx  = ectx_to_lctx usr_elctx in
  let ld_lctx   = ectx_to_lctx ld_elctx in
  let dflt_lctx = ectx_to_lctx !sform_default_ectx in

  (* length of some lexp_context *)
  let usr_len   = M.length usr_lctx in
  let dflt_len  = M.length dflt_lctx in

  (* create a tuple from context and shift it to user context         *
   *  also check if we are in pervasive in which case                 *
   *  we want to load in the current context rather than the default  *)
  let tuple = if !in_pervasive then
      OL.ctx2tup usr_lctx ld_lctx
    else
      OL.ctx2tup dflt_lctx ld_lctx in

  let tuple' = if !in_pervasive then
      tuple
    else
      (Lexp.mkSusp tuple (S.shift (usr_len - dflt_len))) in

  (tuple',Lazy)

(* Register special forms.  *)
let register_special_forms () =
  List.iter add_special_form
            [
              ("Built-in",      sform_built_in);
              ("DeBruijn",      sform_debruijn);
              ("typer-identifier", sform_identifier);
              ("typer-immediate", sform_immediate);
              ("datacons",      sform_datacons);
              ("typecons",      sform_typecons);
              ("_:_",           sform_hastype);
              ("__.__",         sform_proj);
              ("lambda_->_",    sform_lambda Anormal);
              ("lambda_=>_",    sform_lambda Aimplicit);
              ("lambda_≡>_",   sform_lambda Aerasable);
              ("_->_",          sform_arrow Anormal);
              ("_=>_",          sform_arrow Aimplicit);
              ("_≡>_",         sform_arrow Aerasable);
              ("case_",         sform_case);
              ("let_in_",       sform_letin);
              ("Type_",         sform_type);
              ("load",          sform_load);
              (* FIXME: We should add here `let_in_`, `case_`, etc...  *)
              (* FIXME: These should be functions!  *)
              ("decltype",      sform_decltype);
              ("declexpr",      sform_declexpr);
            ];
  List.iter add_special_decl_form
            [
              ("define-operator", sdform_define_operator);
              ("typeclass", sdform_typeclass);
              ("instance", sdform_instance true);
              ("not-instance", sdform_instance false);
              ("bind-instances", (fun ctx _ _ -> ectx_set_inst_def ctx true));
              ("dont-bind-instances", (fun ctx _ _ -> ectx_set_inst_def ctx false));
            ]

(*      Default context with builtin types
 * --------------------------------------------------------- *)

let dynamic_bind r v body =
  let old = !r in
  try r := v;
      let res = body () in
      r := old;
      res
  with e -> r := old; raise e

(* Make lxp context with built-in types *)
let default_ectx
  = try let _ = register_special_forms () in

    (* Read BTL files *)
    let read_file file_name elctx =
      let source = Source.of_path file_name in
      let pres = prelex source in
      let sxps = lex default_stt pres in
      let _, lctx = lexp_p_decls [] sxps elctx
      in lctx in

    (* Register predef *)
    let register_predefs elctx =
      try List.iter (fun name ->
            let idx = senv_lookup name elctx in
            let v = mkVar ((dsinfo, Some name), idx) in
              BI.set_predef name v) BI.predef_names;
      with Senv_Lookup_Fail _ ->
        warning "Predef not found"; in

    (* Empty context *)
    let ectx = empty_elab_context in
    let ectx = SMap.fold (fun key e ctx
                          -> if String.get key 0 = '-' then ctx
                            else ctx_define ctx (dsinfo, Some key) e
                                   (OL.get_type (ectx_to_lctx ectx) e))
                         (!OL.lmap) ectx in

    Heap.register_builtins ();
    (* read base file *)
    let ectx = dynamic_bind parsing_internals true
                            (fun ()
                             -> read_file (btl_folder ^ "/builtins.typer")
                                          ectx) in
    let _ = register_predefs ectx in

    (* Does not work, not sure why
    let files = ["list.typer"; "quote.typer"; "type.typer"] in
    let ectx = List.fold_left (fun ectx file_name ->
      read_file (btl_folder ^ "/" ^ file_name) ectx) ectx files in *)


    builtin_size := get_size ectx;
    let ectx = dynamic_bind in_pervasive true
                 (fun () -> read_file (btl_folder ^ "/pervasive.typer") ectx) in
    let ectx = DB.ectx_set_inst_def ectx true in
    let _ = sform_default_ectx := ectx in
    ectx
  with e ->
    error "Compilation stopped in default context";
    Log.print_log ();
    Log.clear_log ();
    raise e

let default_rctx = EV.from_ectx default_ectx

(*      String Parsing
 * --------------------------------------------------------- *)

let lexp_expr_str str ctx =
  let tenv = default_stt in
  let grm = ectx_get_grammar ctx in
  let limit = Some ";" in
  let source = Source.of_string str in
  let pxps = sexp_parse_source source tenv grm limit in
  let lexps = lexp_parse_all pxps ctx in
  List.iter (fun lxp ->
      resolve_instances lxp;
      ignore (OL.check (ectx_to_lctx ctx) lxp))
    lexps;
  lexps

let lexp_decl_str str ctx =
  let tenv = default_stt in
  let source = Source.of_string str in
  let tokens = lex_source source tenv in
  lexp_p_decls [] tokens ctx


(*  Eval String
 * --------------------------------------------------------- *)
(* Because we cant include Elab in eval.ml *)

let eval_expr_str str ectx rctx =
  let lxps = lexp_expr_str str ectx in
  let lctx = ectx_to_lctx ectx in
  let elxps = List.map (OL.erase_type lctx) lxps in
  EV.eval_all elxps rctx false

let eval_decl_str str ectx rctx =
  let lxps, ectx' = lexp_decl_str str ectx in
  let lctx = ectx_to_lctx ectx in
  let _, elxps = List.fold_left_map (OL.clean_decls) lctx lxps in
  (EV.eval_decls_toplevel elxps rctx), ectx'

let process_file
      (backend : #Backend.t)
      (ectx : elab_context)
      (file_name : string)
    : elab_context =

  try
    let source = Source.of_path file_name in
    let pretokens = prelex source in
    let tokens = lex Grammar.default_stt pretokens in
    let ldecls, ectx' = lexp_p_decls [] tokens ectx in
    List.iter backend#process_decls ldecls;
    ectx'
  with
  | Sys_error _
    -> (error {|file %s does not exist.|} file_name;
        ectx)
