(* Copyright (C) 2021, 2022  Free Software Foundation, Inc.
 *
 * Author: Simon Génier <simon.genier@umontreal.ca>
 * Keywords: languages, lisp, dependent types.
 *
 * This file is part of Typer.
 *
 * Typer is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>. *)

(* Positivity is a syntactic property of type which prevent a common source of
   contradiction. In a nutshell, it means that a type T doesn't contain an arrow
   with T as its domain, or another inductive type with T as a parameter. How
   could we use this to create a contradiction? Take this definition for
   example.

       type Evil
       | make-evil (Evil -> Void);

   If this were allowed we could write this function.

       innocent-function : Evil -> Void;
       innocent-function evil =
         case evil
         | make-evil e => e evil

   Finally, we create a value of type Void.

       what-have-we-done? : Void;
       what-have-we-done? = innocent-function (make-evil innocent-function)

   We created an infinite loop. If our function looks innocuous enough, it's
   because we hid the loop inside our mischievous make-evil constructor.
   Checking inductive types for positivity prevents us from creating such
   constructors. *)

open Lexp
open Util

(* Computes if the judgement x ∉ fv(τ) holds. *)
let absent index lexp =
  let fv, _ = Opslexp.fv lexp in
  not (Debruijn.set_mem index fv)

let rec absent_in_bindings index bindings =
  match bindings with
  | [] -> true
  | (_, _, tau) :: bindings
    -> absent index tau && absent_in_bindings (index + 1) bindings

(* Computes if the judgement x ⊢ e pos holds. *)
let rec positive (index : db_index) (lexp : lexp) : bool =
  (*
   *  x ∉ fv(e)
   * ───────────
   *  x ⊢ e pos
   *)
  absent index lexp || positive' index lexp

and positive' index lexp =
  match Lexp.lexp_lexp' lexp with
  | Lexp.Arrow (_, _, _, tau, e)
    (*
     *  x ⊢ e pos  x ∉ fv(τ)
     * ──────────────────────
     *  x ⊢ (y : τ) → e pos
     *)
    -> absent index tau && positive' (index + 1) e

  | Lexp.Call (_, f, args)
    (*
     *   x ∉ fv(e⃗)
     * ──────────────
     *  x ⊢ x e⃗ pos
     *)
    -> begin
      match Lexp.lexp_lexp' (Lexp.nosusp f) with
      | Lexp.Var (_, index') when Int.equal index index'
        -> List.for_all (fun (_, arg) -> absent index arg) args
      | _
        (* If x ∈ fv(f e⃗), but f ≠ x, then x must occur in e⃗. *)
        -> false
    end

  | Lexp.Inductive (_, _, vs, cs)
    (*
     * The rules for μ-types and union types are respectively
     *
     *  x ⊢ e pos  x ∉ fv(τ)     x ⊢ τ₀ pos  x ⊢ τ₁ pos
     * ──────────────────────   ────────────────────────
     *    x ⊢ μy:τ.e pos            x ⊢ τ₀ ∪ τ₁ pos
     *
     * Since the two are not separate in Typer, this rule combines both.
     *)
    -> absent_in_bindings index vs
      && let index = index + List.length vs in
      Util.SMap.for_all (fun _ c -> positive_in_constructor index c) cs

  | Lexp.Lambda (_, _, _, e) -> positive index e

  | Lexp.Susp (e, s) -> positive index (Lexp.push_susp e s)

  | Lexp.Var _
    (*
     * Since we know that x ∈ fv(e), this must be our variable. The same rule
     * as the Call applies as a degenerate case with no arguments.
     *)
    -> true
  | Lexp.Proj _ -> false

  | Lexp.Case _ | Lexp.Cons _ | Lexp.Let _ | Lexp.Sort _ | Lexp.SortLevel _
    (* There are no rules that have these synctactic forms as a conclusion. *)
    -> false

  | Lexp.Builtin _ | Lexp.Imm _
    -> failwith "must never happen since we already know that x ∈ fv(e)"

  | Lexp.Metavar _
    -> failwith "positivity must be checked after metavariables instanciation"

and positive_in_constructor index vs =
  match vs with
  | [] -> true
  | (_, _, tau) :: vs
    -> positive index tau && positive_in_constructor (index + 1) vs
