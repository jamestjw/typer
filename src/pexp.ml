(* pexp.ml --- Proto lambda-expressions, half-way between Sexp and Lexp.

Copyright (C) 2011-2020  Free Software Foundation, Inc.

Author: Stefan Monnier <monnier@iro.umontreal.ca>
Keywords: languages, lisp, dependent types.

This file is part of Typer.

Typer is free software; you can redistribute it and/or modify it under the
terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

Typer is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.  *)

open Sexp   (* Symbol *)

let pexp_error loc = Log.log_error ~section:"PEXP" ~loc

(*************** The Pexp Parser *********************)

type arg_kind =
  | Anormal
  | Aimplicit
  | Aerasable (** eraseable ⇒ implicit.  *)

module ArgKind = struct
  type t = arg_kind

  let to_arrow : t -> string = function
    | Anormal -> "->" | Aimplicit -> "=>" | Aerasable -> "≡>"

  let to_colon : t -> string = function
    | Anormal -> ":" | Aimplicit -> "::" | Aerasable -> ":::"
end

(*  This is Dangerously misleading since pvar is NOT pexp but Pvar is *)
type pvar = symbol
(* type sort = Type | Ext *)
(* type tag = string *)

type ppat =
  | Ppatsym of vname (** A named default pattern, or a 0-ary constructor.  *)
  | Ppatcons of Source.Location.t * sexp * (symbol option * vname) list

let pexp_pat_location : ppat -> Source.Location.t = function
  | Ppatsym (l, _) -> Sexp.location l
  | Ppatcons (l, _, _) -> l

let pexp_u_formal_arg (arg : arg_kind * pvar * sexp option) =
  match arg with
  | (Anormal, s, None) -> Symbol s
  | (ak, ((l,_) as s), t)
    -> let head =
         Symbol (l, match ak with Aerasable -> ":::"
                                | Aimplicit -> "::"
                                | Anormal -> ":")
       in
       let ty = match t with Some e -> e | None -> Symbol (l, "_") in
       let location = Source.Location.extend l (Sexp.location ty) in
       Node (location, head, [Symbol s; ty])

let pexp_p_pat_arg (s : sexp) = match s with
  | Symbol (_l , n) -> (None, (s, match n with "_" -> None | _ -> Some n))
  | Node (_, Symbol (_, "_:=_"), [Symbol f; Symbol (_l,n)])
    -> (Some f, (s, Some n))
  | _
    -> let loc = Sexp.location s in
       pexp_error loc "Unknown pattern arg";
       (None, (s, None))

let pexp_u_pat_arg ((okn, (l, oname)) : symbol option * vname) : sexp =
  let pname = Symbol (Sexp.location l, match oname with None -> "_" | Some n -> n) in
  match okn with
  | None -> pname
  | Some ((l, _) as n) -> Node (l, Symbol (l, "_:=_"), [Symbol n; pname])

let pexp_p_pat (s : sexp) : ppat = match s with
  | Symbol (_l, n) -> Ppatsym (s, match n with "_" -> None | _ -> Some n)
  | Node (l, c, args) -> Ppatcons (l, c, List.map pexp_p_pat_arg args)
  | _
    -> let l = Sexp.location s in
       pexp_error l "Unknown pattern"; Ppatsym (s, None)

let pexp_u_pat (p : ppat) : sexp = match p with
  | Ppatsym (l, None) -> Symbol (Sexp.location l, "_")
  | Ppatsym (l, Some n) -> Symbol (Sexp.location l, n)
  | Ppatcons (l, c, args) -> Node (l, c, List.map pexp_u_pat_arg args)
