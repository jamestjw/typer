(*
 *      Typer Compiler
 *
 * ---------------------------------------------------------------------------
 *
 *      Copyright (C) 2011-2020  Free Software Foundation, Inc.
 *
 *   Author: Pierre Delaunay <pierre.delaunay@hec.ca>
 *   Keywords: languages, lisp, dependent types.
 *
 *   This file is part of Typer.
 *
 *   Typer is free software; you can redistribute it and/or modify it under the
 *   terms of the GNU General Public License as published by the Free Software
 *   Foundation, either version 3 of the License, or (at your option) any
 *   later version.
 *
 *   Typer is distributed in the hope that it will be useful, but WITHOUT ANY
 *   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *   FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 *   more details.
 *
 *   You should have received a copy of the GNU General Public License along
 *   with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 *      Description:
 *          Declare new print function which align printed values.
 *
 * ---------------------------------------------------------------------------*)

(* print n char 'c' *)
let make_line c n = String.make n c

(* Table Printing helper *)
let make_title title =
    let title_n = String.length title in
    let p = title_n mod 2 in
    let sep_n = (80 - title_n - 4) / 2 in
    let lsep = (make_line '=' sep_n) in
    let rsep = (make_line '=' (sep_n + p)) in
        ("    " ^ lsep ^ title ^ rsep ^ "\n")

let make_rheader (head: ((char * int) option * string) list) =
  print_string "    | ";
  let print_header (o, name) =
    (match o with
     | Some ('r', size) -> Printf.printf "%*s" size name
     | Some ('l', size) -> Printf.printf "%-*s" size name
     | _ -> print_string name);
    print_string " | "
  in
  List.iter print_header head;
  print_string "\n"

let make_sep c = "    " ^ (make_line c 76) ^ "\n"


(* used to help visualize the call trace *)
let print_ct_tree i =
    let rec loop j =
        if j = i then () else
        match j with
            | _ when (j mod 2) = 0 -> print_char '|'; loop (j + 1)
            | _ -> print_char ':'; loop (j + 1) in
    loop 0

(* Colors *)
let red_f : _ format6 = "\x1b[31m"
let green_f : _ format6 = "\x1b[32m"
let yellow_f : _ format6 = "\x1b[33m"
let blue_f : _ format6 = "\x1b[34m"
let magenta_f : _ format6 = "\x1b[35m"
let cyan_f : _ format6 = "\x1b[36m"
let reset_f : _ format6 = "\x1b[0m"

let red = string_of_format red_f
let green = string_of_format green_f
let blue = string_of_format blue_f
let yellow = string_of_format yellow_f
let magenta = string_of_format magenta_f
let cyan = string_of_format cyan_f
let reset = string_of_format reset_f

let color_string color str =
  color ^ str ^ reset

let formatter_of_out_channel (chan : out_channel) : Format.formatter =
  let open Format in
  let f = formatter_of_out_channel chan in
  let isatty = chan |> Unix.descr_of_out_channel |> Unix.isatty in
  let stag_fns : formatter_stag_functions =
    {
      mark_open_stag =
        if isatty
        then
          function
          | String_tag ("red") -> red
          | String_tag ("green") -> green
          | String_tag ("yellow") -> yellow
          | String_tag ("blue") -> blue
          | String_tag ("magenta") -> magenta
          | String_tag ("cyan") -> cyan
          | _ -> ""
        else
          Fun.const "";
      mark_close_stag =
        if isatty
        then
          function
          | String_tag ("red" | "green" | "yellow" | "blue" | "magenta" | "cyan")
            -> reset
          | _ -> ""
        else
          Fun.const "";
      print_open_stag = Fun.const ();
      print_close_stag = Fun.const ();
    }
  in
  Format.pp_set_formatter_stag_functions f stag_fns;
  Format.pp_set_tags f true;
  f
